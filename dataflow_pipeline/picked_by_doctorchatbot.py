
# coding: utf-8

# In[3]:


import apache_beam as beam
import datetime
from pymongo import MongoClient
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from bson.objectid import ObjectId


PROJECT='plenary-justice-151004'
BUCKET='staging-plenary-justice-151004'


def run():
   options = PipelineOptions()
   google_cloud_options = options.view_as(GoogleCloudOptions)
   google_cloud_options.project = 'plenary-justice-151004'
   google_cloud_options.job_name = 'sourcemongo'
   google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
   google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
   google_cloud_options.region = 'asia-east1'
   options.view_as(StandardOptions).runner = 'DirectRunner'
   options.view_as(SetupOptions).save_main_session = False
   requirements_file = "/home/grumpycat/flowarehouse/dataflow_pipeline/requirements.txt"
   options.view_as(SetupOptions).requirements_file = requirements_file

   p = beam.Pipeline(options=options)
   connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"
#    connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.200/alomobile"


#    start = datetime.datetime(2018, 10, 1)
#    end = datetime.datetime(2018, 10, 31)
   tdelta = datetime.timedelta(hours=7)

   def debug_function(pcollection_as_list):
    print(pcollection_as_list)

#    schema =
   que = (p
            | 'Readquestions' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={"picked_up_by_id":ObjectId('5b3eeee532f9062b81963307'), "is_paid":False, "is_closed":True , "created_at" : {"$gte":datetime.datetime(2019,1,30,10,36,0)-tdelta}}, fields=['title', 'in_question', 'status_question_user', 'status_question_doctor', '_type', 'meta_chatbot', 'picked_up_by_id', 'is_deleted', 'created_at'])
            | 'format' >> beam.Map(lambda x:{'id':str(x['_id']),
                                             'title':x['title'],
                                             'in_question':x.get('in_question'),
                                             'status_question_user':x.get('status_question_user'),
                                             'status_question_doctor':x.get('status_question_doctor'),
                                             '_type':x['_type'],
                                             'meta_chatbot':x.get('meta_chatbot'),
                                             'picked_up_by_id':str(x.get('picked_up_by_id')),
                                             'is_deleted':x['is_deleted'],
                                             'created_at':str(x['created_at']+tdelta)[0:19]})
#             | 'DebugQ' >> beam.Map(debug_function))
            | 'writeQToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','cek_metachatbot'),
                     schema = 'id:STRING, title:STRING, in_question:BOOLEAN, status_question_user:STRING, status_question_doctor:STRING, _type:STRING, meta_chatbot:BOOLEAN, picked_up_by_id:STRING, is_deleted:BOOLEAN, created_at:DATETIME',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )

   p.run().wait_until_finish()

if __name__ == '__main__':
   run()
