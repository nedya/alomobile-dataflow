
# coding: utf-8

# In[1]:

import apache_beam as beam
import re
import uuid, math
import itertools
from datetime import datetime, timedelta
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from google.oauth2.service_account import Credentials
from bson.objectid import ObjectId
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from google.cloud import bigquery
def debug_function(pcollection_as_list):
    print (pcollection_as_list)

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"
connection_alodokter = "mongodb://grumpycat:alo.1975.dokter@35.240.137.27/alodokter"

scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)

# table_names = {'fact_users-cr':None, 'fact_magazine-cr':None, 'fact_video_campaign-cr':None, 'fact_campaign-cr':None, 'factquestions-cr':None}
# from google.cloud import bigquery
# client = bigquery.Client()
# dataset_ref = client.dataset('alobrain_data_staging')
# for key,val in table_names.items():
#     data = key.split("-")
#     #field default : updated_at
#     field = 'updated_at'
#     if (data[1] == 'cr'):
#         field = 'created_at'
#     elif (data[1] == 'join'):
#         field = 'join_at'
#     sql = "SELECT MAX ("+ field +") FROM [plenary-justice-151004.alowarehouse_alodoktermobile."+data[0]+"]"
#     query = client.run_sync_query(sql)
#     query.run()
#     table_names[key] = query.rows[0][0]

#dimensi_doctors
class GetCitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        #add birthyear element
        birthyear = element.get('birthday', None)[-4:]
        if (birthyear):
            element['birthyear'] = int(birthyear)
        else: element['birthyear'] = None

        #add city name element
        if (element['city_id']):
            city_query = db.cities.find({'_id':element['city_id']})
            for city in city_query:
                element['city'] = city['name']
                data.append(element)
        else:
            element['city'] = None
            data.append(element)
        return data

class GetSpecialitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element['doctor_speciality_id'] != '':
            speciality_query = db.doctor_specialities.find({'_id':element['doctor_speciality_id']})
            for speciality in speciality_query:
                element['speciality']=speciality['name']
        else:
            element['speciality']=None
        data.append(element)
        return data

class GetSkuDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element.get('app_product_id'):
            sku_query = db.app_products.find({'_id':ObjectId(element['app_product_id'])})
            for sku in sku_query:
                element['sku']=sku['sku']
                data.append(element)
        else:
            element['sku']=None
            data.append(element)
        return data

#dimensi_review_doctors
def alphanumeric(x):
    x['review'] = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", x['review'])
    return x

#dimensi_journal
def paid_timezoneJournals(j):
    if j['paid_time'] is not None:
        j['paid_time'] = str(j['paid_time']+timedelta(hours=7))[0:19]
    return j

def gross_amountJournals(j):
    if j['gross_amount'] is not None:
        j['gross_amount'] = int(j['gross_amount'])
    return j

#dimensi questions
def join_listsQ((k, v)):
    return itertools.product(v['number_of_chatbox'], v['questions'])

def mergedictsQ(questions_dict_chatbox):
    (chatbox, questions_dict) = questions_dict_chatbox
    a_dict = questions_dict
    a_dict.update(chatbox)
    return a_dict

class GetPriceQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        prices = []
        if element.get('journal_id', None) is not None:
                price_query = db.journals.find({'_id':element['journal_id']})
                for price in price_query:
                    if price.get('gross_amount', None) is not None:
                        prices.append({'id':str(element['_id']),
                                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                                        'price':int(price.get('gross_amount', None)),
                                        'pickup_fee':4000, 'like_fee':1000,
                                        'title':element['title'],
                                        'content':element['content'],
                                        'is_said_thanks':element['is_said_thanks'],
                                        'is_paid':element.get('is_paid'),
                                        'is_closed':element['is_closed'],
                                        'type':element['_type'],
                                        'sub_intent_id':element.get('sub_intent_id', None),
                                        'intent_id':element.get('intent_id', None),
                                        'is_has_conclusion':element.get('is_has_conclusion', None),
                                        'is_shown':element.get('is_shown', None)})
                    elif price.get('gross_amount', None) is None:
                        prices.append({'id':str(element['_id']),
                                       'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                                       'price':None, 'pickup_fee':4000,
                                       'like_fee':1000,
                                       'title':element['title'],
                                       'content':element['content'],
                                       'is_said_thanks':element['is_said_thanks'],
                                       'is_paid':None,
                                       'is_closed':element['is_closed'],
                                       'type':element['_type'],
                                       'sub_intent_id':element.get('sub_intent_id', None),
                                       'intent_id':element.get('intent_id', None),
                                       'is_has_conclusion':element.get('is_has_conclusion', None),
                                       'is_shown':element.get('is_shown', None)})
        elif element.get('journal_id', None) is None:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        else:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        return prices


def is_chatbotQ(q):
       if q['intent_id'] or q['sub_intent_id'] is not None:
           is_chatbot = {'is_chatbot':True}
       elif q['intent_id'] and q['sub_intent_id'] is None:
           is_chatbot = {'is_chatbot':False}
       else:
           is_chatbot = {'is_chatbot':None}

       if q['type'] == "Core::PreQuestion":
           autochat_close = {'autochat_close':True}
       elif q['type'] == "Core::Question":
           autochat_close = {'autochat_close':False}
       else:
           autochat_close = {'autochat_close':None}

       q.update(is_chatbot)
       q.update(autochat_close)
       q.pop('intent_id')
       q.pop('sub_intent_id')
       return q

class GetChatBoxQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        chatboxes = []
        number_of_chatbox = db.answers.find({'question_id':ObjectId(element['id'])}, no_cursor_timeout=True).count()
        chatboxes.append({'id':str(element['id']),
                          'updated_at':element['updated_at'],
                          'number_of_chatbox':number_of_chatbox,
                          'price':element['price'],
                          'pickup_fee':element['pickup_fee'],
                          'like_fee':element['like_fee'],
                          'title':element['title'],
                          'content':element['content'],
                          'is_said_thanks':element['is_said_thanks'],
                          'is_paid':element['is_paid'],
                          'is_closed':element['is_closed'],
                          'type':element['type'],
                          'is_has_conclusion':element.get('is_has_conclusion', None),
                          'is_shown':element.get('is_shown', None)})
        return chatboxes

#dimensi_campaigns
class GetclientnameCampFn(beam.DoFn):
    def process (self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        clients = []
        client_query = db.clients.find({'_id':ObjectId(element['client_id'])})
        for client in client_query:
            clients.append({'id':str(element['_id']),
                            'name':str(client['name']+" (" + str(element['start_date']+timedelta(hours=7)) [0:19] + " - "+ str(element['end_date']+timedelta(hours=7))[0:19] + ")"),
                            'start_date':str(element['start_date']+timedelta(hours=7))[0:19],
                            'end_date':str(element['end_date']+timedelta(hours=7))[0:19],
                            'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19]})
        return clients


#dimensi video campaigns tag
class DenormalizeVCTFn(beam.DoFn):
    def process(self,element):
        data = []
        if element.get('tags'):
            for tag in element['tags']:
                data.append({'video_campaign_id':element['_id'], 'tag':tag})
        else: data.append({'video_campaign_id':element['_id'], 'tag':None})
        return data

class GetTagIDVCTFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        tag_list = []
        if element.get('tag'):
            tag_query = db.terms.find({'name':element['tag']})
            for tag in tag_query:
                tag_list.append({'video_campaign_id':str(element['video_campaign_id']),
                                'tag_id':str(tag['_id'])
                                })
        else:
            tag_list.append({'video_campaign_id':str(element['video_campaign_id']),
                                'tag_id':None
                                })

        return tag_list

#dimensi fact users
def join_infoUC((k,v)):
    return itertools.product(v['user_id'], v['channel_id'])

def merge_dictUC(user_channel):
      (user, channel) = user_channel
      a_dict = user
      a_dict.update(channel)
      return a_dict

#dimensi meta question
def maxrecMetaQ(m):
        if m.get('max_count_recommendation', None) is None:
                m['max_count_recommendation'] = 3
        return m

#dimensi fact_magazine
class DenormalizeFMFn(beam.DoFn):
    def process(self, element):
        magazines_relationships = element.get('magazine_relationships')
        magazines = []
        if magazines_relationships is not None:
            if (len(magazines_relationships) > 0):
                for term_taxonomy_ids in magazines_relationships:
                    for data in [term_taxonomy_ids]:
                        magazines.append({'id':str(uuid.uuid4()),
                                          'article_id':element['post_id'],
                                          'term_taxonomy_id':data['term_taxonomy_id'],
                                          'created_at':element['created_at']})
        else:
                    magazines.append({'id':str(uuid.uuid4()),
                              'article_id':element['post_id'],
                              'term_taxonomy_id':None,
                              'created_at':element['created_at']})
        return magazines

class GetTermMagFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        term_taxonomies = db.term_taxonomies
        term_list = []
        if element.get('term_taxonomy_id', None) is not None:
            for term in term_taxonomies.find({'_id':ObjectId(element['term_taxonomy_id'])}):
                term_list.append({'id':element['id'],
                                  'article_id':element['article_id'],
                                  'term_id':str(term['term_id']),
                                  'created_at':element['created_at']})
        else:
                term_list.append({'id':element['id'],
                      'article_id':element['article_id'],
                      'term_id':None,
                      'created_at':element['created_at']})
        return term_list


#dimensi fact video campaign
class UnlistfvcFn(beam.DoFn):
    def process(self, element):
        tags = element.get('tags', [])
        tag_list = []
        for data in tags:
                tag_list.append({'tags':data,
                                'campaign_id':element['video_ad_campaign_id'],
                                'user_id':element['user_id'],
                                'created_at':element['created_at']})
        return tag_list

class GetTagfvcFn(beam.DoFn):
    def process (self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        termID_list = []
        terms = db.terms
        for term in terms.find({'name':element['tags']}):
            campaign_query = db.video_ad_campaigns.find({'_id':element['campaign_id']})
            for campaign in campaign_query:
                termID_list.append({'id':str(uuid.uuid4()),
                                     'ads_unit_id':str(campaign['video_ad_id']),
                                     'user_id':str(element['user_id']),
                                     'campaign_id':str(element['campaign_id']),
                                     'tag_id':str(term['_id']),
                                     'created_at':str(element['created_at']+timedelta(hours = 7))[0:19],
                                     'year_id':element['created_at'].year,
                                     'month_id':element['created_at'].month,
                                     'date_id':element['created_at'].day,
                                     'hour_id':element['created_at'].hour,
                                     'minute_id':element['created_at'].minute
                                    })
        return termID_list

#fact_doctors
class DenormalizedFDFn(beam.DoFn):
    def process (self,element):
        data = []
        if element.get("educations"):
            for education in element['educations']:
                data.append({'doctor_id':str(element['_id']), 'year':int(education['year_of_graduation']), 'university':education['name']})
        else:
             data.append({'doctor_id':str(element['_id']), 'year':None, 'university':None})
        return data

class GetUniversityFn(beam.DoFn):
    def process (self,element):
        client = bigquery.Client()
        dataset_ref = client.dataset('alowarehouse_alodoktermobile')
        doctors = []
        if (element['university'] is not None):
            if "\"" in element['university']:
                element['university'] = element['university'].replace ("\"",'\\"')
            sql = ("SELECT id "
                       + "FROM [plenary-justice-151004:alowarehouse_alodoktermobile.universities] "
                       + "WHERE university = \"" + element['university'] + "\""
                  )
            query = client.run_sync_query(sql)
            query.run()
            universities = []
            university_id = query.rows[0][0]
        else:university_id = None
        doctors.append({'id': str(uuid.uuid4()),'doctor_id':str(element['doctor_id']),
                        'university_id':university_id,
                        'graduate_year_id':element['year']})
        return doctors

#dimensi fact campaigns
class GetTagFCFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        campaign_query = db.campaigns.find({'_id':element['campaign_id']},{'_id':1,'tags':1})
        for campaign in campaign_query:
            if (campaign.get('tags')): #if campaign tags is exsist
                for campaign_tag in campaign['tags']:
                    term_query = db.terms.find({'name':campaign_tag},{'_id':1})
                    for tag_id in term_query:
                        data.append({'id':element['_id'],
                                    'form_instance_values': element['form_instance_values'],
                                    'tag_id':tag_id['_id'],
                                    'client_id':element['client_id'],
                                    'campaign_id':element['campaign_id'],
                                    'user_id':element['user_id'],
                                    'created_at':element['created_at']+timedelta(hours=7)}
                                   )
            else:
                data.append ({'id':element['_id'],
                                'form_instance_values': element['form_instance_values'],
                                'tag_id':None,
                                'client_id':element['client_id'],
                                'campaign_id':element['campaign_id'],
                                'user_id':element['user_id'],
                                'created_at':element['created_at']+timedelta(hours=7)}
                              )
        return data

class GetQnAFC(beam.DoFn):
    def process(self, element):
        data = []
        for question in element['form_instance_values']:
            if (question.get('form_value_ids')): #jika pertanyaan memiliki jawaban
                for answer in question['form_value_ids']:
                    if (answer == '5770ef13150cd4744f00000b'):
                        answer = questions['text_value']
                    data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': str(answer),
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
            else: data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': None,
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
        return data

#dimensi user interest
class GetUserInterestFn(beam.DoFn):
    def process(self,element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        if element.get('interest_ids'):
            for interest_id in element ['interest_ids']:
                data.append({'user_id':str(element['_id']),
                            'interest_id':str(interest_id),
                            'is_deleted':False})
        else: data.append({'user_id':str(element['_id']),
                           'interest_id':None,
                           'is_deleted':False})
        return data

#dimensi users
class GetCitiesUsersFn(beam.DoFn):
    def process(self, element):
        if element.get('birthday', None):
            birth_year = int(element['birthday'][-4:])
        else: birth_year = None

        if (element['provider']):
            registration_channel = element['provider']
        else: registration_channel = "email"

        if element.get('city_id', None) is not None:
            client = MongoClient(connection_string)
            db = client.alomobile
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])}, no_cursor_timeout=True)
            for city in city_query:
                city_name = city['name']
        else: city_name = None

        data = []
        data.append({'id':str(element['_id']),
                     'fullname':element['firstname'] + " " + element['lastname'],
                     'city':city_name,
                     'email':element.get('email', None),
                     'phone':element.get('phone', None),
                     'created_at':str(element['created_at'] +timedelta(hours=7))[0:19],
                     'gender':element.get('gender',""),
                     'birthdate':element.get('birthday', ""),
                     'birthyear': birth_year,
                     'registration_channel':registration_channel,
                     'version':element.get('version',""),
                     'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19]})
        return data

#fact_questions
class if_meta_question(beam.DoFn):
    def process(self, element):
        data = []
        if element.get('time_picked_up') is not None:
            time_picked_up = str(element['time_picked_up']+timedelta(hours=7))[0:19]
            date_picked_up = str(time_picked_up)[0:10]
        else:
            time_picked_up = None
            date_picked_up = None

        if element.get('_type') == "Core::Question" or element.get('_type') == "Core::PreQuestion":
            data.append({'topic':element.get('topic', None),
                        'question_id':str(element['_id']),
                        'meta_question_id':str(element.get('meta_question_id',"")),
                        'user_id':str(element['user_id']),
                        'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                        'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                        'intent_id':str(element.get('intent_id',"")),
                        'sub_intent_id':str(element.get('sub_intent_id',"")),
                        'time_picked_up':time_picked_up,
                        'journal_id':str(element.get('journal_id')),
                        'date_picked_up':date_picked_up,
                        'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
                        'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
                        'is_has_conclusion':element.get('is_has_conclusion', None),
                        'no_conclusion':element.get('no_conclusion', None)})

        elif element.get('_type') == "Core::MetaQuestion":
            data.append({'topic':element.get('topic', None),
                        'question_id':None,
                        'meta_question_id':str(element['_id']),
                        'user_id':str(element['user_id']),
                        'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                        'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                        'intent_id':str(element.get('intent_id',"")),
                        'sub_intent_id':str(element.get('sub_intent_id',"")),
                        'time_picked_up':time_picked_up,
                        'journal_id':str(element.get('journal_id')),
                        'date_picked_up':date_picked_up,
                        'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
                        'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
                        'is_has_conclusion':element.get('is_has_conclusion', None),
                        'no_conclusion':element.get('no_conclusion', None)})
        return data

class GetTermsFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_alodokter)
        db = client.alodokter
        terms = []
        term_query = db.core_terms.find({'name':re.compile(element['topic'], re.IGNORECASE)}, no_cursor_timeout=True).limit(1)

        for term in term_query:
            if element['topic'] == "":
                term['_id'] = ""
            else:
                term['_id'] = term['_id']

            terms.append({'topic_id':str(term['_id']),
                          'question_id':element['question_id'],
                          'meta_question_id':element['meta_question_id'],
                          'user_id':element['user_id'],
                          'picked_up_by_id':element['picked_up_by_id'],
                          'created_at':element['created_at'],
                          'updated_at':element['updated_at'],
                          'intent_id':element['intent_id'],
                          'sub_intent_id':element['sub_intent_id'],
                          'time_picked_up':element['time_picked_up'],
                          'journal_id':element['journal_id'],
                          'date_picked_up':element['date_picked_up'],
                          'first_user_answer_time':element['first_user_answer_time'],
                          'first_doctor_answer_time':element['first_doctor_answer_time'],
                          'is_has_conclusion':element['is_has_conclusion'],
                          'no_conclusion':element['no_conclusion']})
        return terms

class GetConclusionFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        conclusions = []
        if element.get('is_has_conclusion') is True:
            conclusion_query = db.question_conclusions.find({'question_id':ObjectId(element['question_id'])})
            for conclusion in conclusion_query:
                conclusion_id = str(conclusion['_id'])
        else: conclusion_id = None
        conclusions.append({'conclusion_id':conclusion_id,
                        'topic_id':element['topic_id'],
                        'question_id':element['question_id'],
                        'meta_question_id':element['meta_question_id'],
                        'user_id':element['user_id'],
                        'picked_up_by_id':element['picked_up_by_id'],
                        'created_at':element['created_at'],
                        'updated_at':element['updated_at'],
                        'intent_id':element['intent_id'],
                        'sub_intent_id':element['sub_intent_id'],
                        'time_picked_up':element['time_picked_up'],
                        'journal_id':element['journal_id'],
                        'date_picked_up':element['date_picked_up'],
                        'first_user_answer_time':element['first_user_answer_time'],
                        'first_doctor_answer_time':element['first_doctor_answer_time'],
                        'is_has_conclusion':element['is_has_conclusion'],
                        'no_conclusion':element['no_conclusion']})
        return conclusions

class GetPaymentFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        journals = []
        if element['journal_id'] != 'None' :
            journal_query = db.journals.find({'_id':ObjectId(element['journal_id'])})
            for journal in journal_query:
                journal_id = element['journal_id']
                payment_method_id = str(journal['payment_method_id'])
        else:
            journal_id = None
            payment_method_id = None
        journals.append({'id':str(uuid.uuid4()),
                         # 'conclusion_id':element['conclusion_id'],
                         'topic_id':element['topic_id'],
                         'question_id':element['question_id'],
                         'meta_question_id':element['meta_question_id'],
                         'user_id':element['user_id'],
                         'picked_up_by_id':element['picked_up_by_id'],
                         'created_at':element['created_at'],
                         'updated_at':element['updated_at'],
                         'intent_id':element['intent_id'],
                         'sub_intent_id':element['sub_intent_id'],
                         'time_picked_up':element['time_picked_up'],
                         'journal_id':journal_id,
                         'payment_method_id':payment_method_id,
                         'date_picked_up':element['date_picked_up'],
                         'first_user_answer_time':element['first_user_answer_time'],
                         'first_doctor_answer_time':element['first_doctor_answer_time'],
                         'is_has_conclusion':element['is_has_conclusion'],
                         'no_conclusion':element['no_conclusion']})
        return journals

#campaign_answer_freetext
class GetTextValueFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        text_values = []
        if element['form_type_id'] == "5770ef13150cd4744f00000b":
            form_instances = db.form_instances.find({"form_instance_values":{"$elemMatch":{"form_value_ids":[element['id']]}}})
            for answers in form_instances:
                for answer in answers['form_instance_values']:
                    if (answer['text_value']):
                        text_values.append({'instance_id': str(answers['_id']),
                                            'value_id':str(element['id']),
                                            'user_id':str(answers['user_id']),
                                            'text_value':answer['text_value'],
                                            'created_at':str(answers['created_at']+tdelta)[0:19]})
        return text_values

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]

    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'

    # table_names = {'clients-upd':None}
              #  'ads_unit-upd':None,
              #  'app_products-upd':None,
              #  'article_android-upd':None,
              #  'campaign_video-upd':None,
              #  'campaign_questions-cr':None,
              #  'review_doctors-cr':None,
              #  'journals-upd':None,
              #  'conclusions-upd':None,
              #  'metaquestions-cr':None,
              #  'questions-upd':None,
              #  'doctor-join':None,
              #  'users-upd':None
              # }

    # client = bigquery.Client()
    # dataset_ref = client.dataset('alowarehouse_alodoktermobile')
    # for key,val in table_names.items():
    #     data = key.split("-")
    #     #field default : updated_at
    #     field = 'updated_at'
    #     if (data[1] == 'cr'):
    #         field = 'created_at'
    #     elif (data[1] == 'join'):
    #         field = 'join_at'
    #     sql = "SELECT MAX ("+ field +") FROM [plenary-justice-151004.alowarehouse_alodoktermobile."+ data[0] +"]"
    #     query = client.run_sync_query(sql)
    #     query.run()
    #     table_names[key] = query.rows[0][0]

#    pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])

# #1
#     client = MongoClient(connection_string)
#     db = client.alomobile
#     users = db.users
#     universities=[]
#     for element in users.distinct('educations.name',{'_type':'Core::Doctor','educations':{'$exists':True}}):
#         universities.append({'id': str(uuid.uuid4()),'university':element})
#
#     with beam.Pipeline(options = options) as p_universities:
#         (p_universities
#             | 'unique_universities' >> beam.Create(universities)
#             # | 'Debuguniversities' >> beam.Map(debug_function)
#             | 'writeuniversitiesToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','universities'),
#                           schema='id:STRING, university:STRING',
#                           create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                           write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )
# # #2
#     with beam.Pipeline(options = options) as p_client:
#         (p_client
#              | 'ReadClientsMob' >> ReadFromMongo(connection_string, 'alomobile', 'clients',
#                             # query={'updated_at':{'$gt':table_names['clients-upd'] - timedelta(hours = 7)}},
#                             query={},
#                             fields=['name', 'updated_at'])
#              | 'strClient' >> beam.Map (lambda x:{ 'id':str(x['_id']), 'name':x['name'], 'updated_at':str(x['updated_at']+timedelta(hours=7))[0:19]})
#              # | 'DebugClient' >> beam.Map(debug_function)
#              | 'writeClientToBQ' >> beam.io.Write(
#                          beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','clients'),
#                          schema='id:STRING, name:STRING, updated_at:DATETIME',
#                          create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                          write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )
# #3
    # with beam.Pipeline(options = options) as p_intents:
    #    (p_intents
    #       | 'ReadIntentsMob' >> ReadFromMongo(connection_string, 'alomobile', 'intents',
    #                                   query={'is_deleted':False}, fields=['name'])
    #       |'strintents' >> beam.Map (lambda x:{'id':str(x['_id']), 'intent_name':x['name']})
    #       # |'Debugintents' >> beam.Map(debug_function)
    #       | 'writeintentsToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','intents'),
    #                               schema='id:STRING, intent_name:STRING',
    #                               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                               write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #    )
# # #4
#     with beam.Pipeline(options = options) as p_subintents:
#       (p_subintents
#           |'ReadSubintentsMob' >> ReadFromMongo(connection_string, 'alomobile', 'sub_intents',
#                                     query={'is_active':True, 'is_disabled':False, 'is_deleted':False}, fields=['topic'])
#           |'strsubintents' >> beam.Map (lambda x:{'id':str(x['_id']), 'subintent_name':x['topic']})
#           # |'Debugsubintents' >> beam.Map(debug_function)
#           | 'writesubintentsToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','subintents'),
#                                   schema='id:STRING, subintent_name:STRING',
#                                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#       )
# #5
#     with beam.Pipeline(options = options) as p_payment_methods:
#       (p_payment_methods
#           | 'ReadPaymentMethodMob'>> ReadFromMongo(connection_string, 'alomobile', 'payment_methods', query={},
#                                         fields=['payment_type', 'payment_section', 'name', 'code'])
#           | 'strpayment_methods' >> beam.Map (lambda x:{'id':str(x['_id']),
#                                                         'name':x['name'],
#                                                         'payment_section':x['payment_section'],
#                                                         'code':x['code']})
#           # | 'Debugpayment_methods' >> beam.Map(debug_function)
#           | 'writepayment_methodsToBQ' >> beam.io.Write(
#                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','payment_methods'),
#                             schema='id:STRING, name:STRING, payment_section:STRING, code:STRING',
#                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#       )
# # #6
#     with beam.Pipeline(options = options) as p_payment_gateways:
#       (p_payment_gateways
#           | 'ReadPaymentGatewaysMob'>> ReadFromMongo(connection_string, 'alomobile', 'payment_gateways',
#                                         query={},
#                                         fields=['channel_name', 'code'])
#           | 'strpayment_gateways' >> beam.Map (lambda x:{'id':str(x['_id']), 'channel_name':x['channel_name'], 'code':x['code']})
#           # | 'Debugpayment_gateways' >> beam.Map(debug_function)
#           | 'writepayment_gatewaysToBQ' >> beam.io.Write(
#                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','payment_gateways'),
#                             schema='id:STRING, channel_name:STRING, code:STRING',
#                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#       )
# #7
#     with beam.Pipeline(options = options) as p_payment_provider:
#        (p_payment_provider
#              | 'ReadPaymentPoviderMob'>> ReadFromMongo(connection_string, 'alomobile', 'payment_providers',
#                                         query={},
#                                         fields=['name'])
#              | 'strpayment_provider' >> beam.Map (lambda x:{'id':str(x['_id']), 'name':x['name']})
#              # | 'Debugpayment_provider' >> beam.Map(debug_function)
#              | 'writepayment_providerToBQ' >> beam.io.Write(
#                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','payment_providers'),
#                             schema='id:STRING, name:STRING',
#                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#        )
# # 8
#     with beam.Pipeline(options = options) as p_specialities:
#         (p_specialities
#             |'ReadSpecialitiesMob' >> ReadFromMongo(connection_string, 'alomobile', 'specialities', query={}, fields=['_id', 'name'])
#             |'strspecialities' >> beam.Map (lambda x:{'id':str(x['_id']), 'interest':x['name']})
#             # |'Debugspecialities' >> beam.Map(debug_function)
#             | 'writespecialitiesToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','specialities'),
#                       schema='id:STRING, interest:STRING',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )
# #9
#     with beam.Pipeline(options = options) as p_popularities:
#         (p_popularities
#                  | 'ReadMagPopMob' >> ReadFromMongo(connection_string, 'alomobile', 'magazine_popularities', query={}, fields=['magazine_id', 'total_view'])
#                  | 'strpopularities' >> beam.Map (lambda x:{ 'article_id':str(x['magazine_id']), 'total_view':int(x['total_view'])})
#                  # | 'Debugpopularities' >> beam.Map(debug_function)
#                  | 'writepopularitiesToBQ' >> beam.io.Write(
#                              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','magazine_popularities'),
#                              schema='article_id:STRING, total_view:INTEGER',
#                              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )
# #10
#     with beam.Pipeline(options = options) as p_ads_unit:
#        (p_ads_unit
#            | 'ReadAdsUnitMob'>> ReadFromMongo(connection_string, 'alomobile', 'video_ads',
#                                          # query={'updated_at':{'$gt':table_names['ads_unit-upd'] - timedelta(hours = 7)},
#                                          query={},
#                                          fields=['_id', 'ad_unit_name', 'ad_unit_url', 'updated_at'])
#            |'strads_unit' >> beam.Map (lambda x:{'id':str(x['_id']), 'name':x['ad_unit_name'], 'url':x['ad_unit_url'], 'updated_at':str(x['updated_at']+timedelta(hours=7))[0:19]})
#            # |'Debugads_unit' >> beam.Map(debug_function)
#            | 'writeads_unitToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','ads_unit'),
#                       schema='id:STRING, name:STRING, url:STRING, updated_at:DATETIME',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#          )
# # #11
#     with beam.Pipeline(options = options) as p_terms:
#       (p_terms
#            | 'ReadTermMob' >> ReadFromMongo(connection_string, 'alomobile', 'terms', query={}, fields=['name'])
#            | 'strTerm' >> beam.Map (lambda x:{ 'id':str(x['_id']), 'topic':x['name']})
#            # | 'DebugTerm' >> beam.Map(debug_function)
#            | 'writeTermToBQ' >> beam.io.Write(
#                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','terms'),
#                             schema='id:STRING, topic:STRING',
#                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#      )

# #16
#     with beam.Pipeline(options = options) as p_campaign_answers:
#        (p_campaign_answers
#            |'ReadCampAnsMob' >> ReadFromMongo(connection_string, 'alomobile', 'form_values',
#                                       query={'is_deleted':False, 'form_type_id' : {'$ne':ObjectId('5770ef13150cd4744f00000b')}},
#                                       fields=['value', 'form_type_id'])
#            |'strCA'>>beam.Map (lambda x:{'id':str(x['_id']),
#                                  'answer':x['value']})
#            # |'DebugCA' >> beam.Map(debug_function)
#            | 'writeCAToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','campaign_answers'),
#                       schema='id:STRING, answer:STRING',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#       )
# # #17
#     with beam.Pipeline(options = options) as p_interests:
#       (p_interests
#           |'ReadInterestsMob' >> ReadFromMongo(connection_string, 'alomobile', 'interests', query={}, fields=['_id', 'name'])
#           |'strinterests' >> beam.Map (lambda x:{'id':str(x['_id']), 'interest':x['name']})
#           # |'Debuginterests' >> beam.Map(debug_function)
#           | 'writeinterestsToBQ' >> beam.io.Write(
#                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','interests'),
#                      schema='id:STRING, interest:STRING',
#                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#       )


# #20
#     with beam.Pipeline(options = options) as p_video_campaign_tag:
#         (p_video_campaign_tag
#                 |'ReadVCTMob'>> ReadFromMongo(connection_string, 'alomobile', 'video_ad_campaigns',
#                                          query={}, fields=['_id', 'tags'])
#                 |'DenormalizeVCT' >> beam.ParDo(DenormalizeVCTFn())
#                 |'GetTagIDVCT'>> beam.ParDo(GetTagIDVCTFn())
#                 # |'DebugVCT' >> beam.Map(debug_function)
#                 | 'writeVCTToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','video_campaigns_tag'),
#                       schema='video_campaign_id:STRING, tag_id:STRING',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#        )
# #21
#     with beam.Pipeline(options = options) as p_popperday:
#         (p_popperday
#                  | 'ReadPopperDayMob' >> ReadFromMongo(connection_string, 'alomobile', 'magazine_popularity_per_days', query={}, fields=['magazine_id', 'total_view'])
#                  | 'strpopperday' >> beam.Map (lambda x:{ 'article_id':str(x['magazine_id']), 'total_view_per_day':x['total_view']})
#                  # | 'Debugpopperday' >> beam.Map(debug_function)
#                  | 'writepopperdayToBQ' >> beam.io.Write(
#                              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','pageviews'),
#                              schema='article_id:STRING, total_view_per_day:INTEGER',
#                              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )



#29
    # with beam.Pipeline(options = options) as pipeline_fact_doctors:
    #     (pipeline_fact_doctors
    #         |'ReadDoctor' >> ReadFromMongo(connection_string, 'alomobile', 'users',
    #                                              query={'_type':'Core::Doctor'},
    #                                              fields=['educations.name','educations.year_of_graduation'])
    #         |'Denormalized' >> beam.ParDo (DenormalizedFDFn())
    #         |'GetUniversity' >> beam.ParDo (GetUniversityFn())
    #         #|'Debug' >> beam.Map (debug_function)
    #         | 'writeToBQ' >> beam.io.Write(
    #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_doctors'),
    #                       schema='id:STRING, doctor_id:STRING,university_id:STRING, graduate_year_id:INTEGER',
    #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )

###############################################################################################################################################################

# #30
    # with beam.Pipeline(options = options) as p_user_interests:
    #     (p_user_interests
    #         |'ReadUserInterestMob' >> ReadFromMongo(connection_string, 'alomobile', 'users',
    #                                     query={"_type":"Core::User", 'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}}, fields=['_id', 'interest_ids'])
    #         |'GetUserInterest' >> beam.ParDo(GetUserInterestFn())
    #         # |'DebugUserInterest' >> beam.Map(debug_function)
    #         | 'writeUserInterestToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','user_interests'),
    #                   schema='user_id:STRING, interest_id:STRING, is_deleted:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #     )


##################################################################################################################################################################

# # #32
#     with beam.Pipeline(options = options) as p_doctor_specialities:
#         (p_doctor_specialities
#             |'ReadDoctSpecMob' >> ReadFromMongo(connection_string, 'alomobile', 'users',
#                                     query={'_type':'Core::Doctor'}, fields=['_id', 'speciality_ids'])
#             |'UnlistSpecialitiesID'>>beam.ParDo(UnlistDoctorSpecialitiesIDFn())
#             |'GetSpecialityID'>>beam.ParDo(GetDoctorSpecialityIDFn())
#             # |'Debugdoctorspecialities' >> beam.Map(debug_function)
#             | 'writedoctorspecialitiesToBQ' >> beam.io.Write(
#                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','doctor_specialities'),
#                   schema='id:STRING, doctor_id:STRING, speciality_id:STRING, is_deleted:BOOLEAN',
#                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )




## # 36
    # with beam.Pipeline(options = options) as p_review_chatbots:
    #     (p_review_chatbots
    #         | 'Readreviewchatbots' >> ReadFromMongo(connection_string, 'alomobile', 'review_chatbots',
    #                                    query={'updated_at':{'$gt':datetime(2019,2,22,22,49,25) - timedelta(hours = 7)}},
    #                                   fields=["question_id",
    #                                           "satisfaction",
    #                                           "accuration_score",
    #                                           "quality_qna_session",
    #                                           "comment",
    #                                           "updated_at", "created_at"])
    #         |'strrevchatbot' >> beam.Map (lambda x:{'id':str(x['_id']), 'question_id':str(x['question_id']),
    #                                         'satisfaction':x['satisfaction'], 'accuration_score':x['accuration_score'],
    #                                         'quality_qna_session':x['quality_qna_session'], 'comment':str(x['comment']),
    #                                         'created_at':str(x['created_at']+timedelta(hours = 7))[0:19], 'updated_at':str(x['updated_at']+timedelta(hours = 7))[0:19]})
    #         | 'writerevchatbotToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','review_chatbot'),
    #                                         schema='id:STRING, question_id:STRING, satisfaction:INTEGER, accuration_score:FLOAT64, quality_qna_session:FLOAT64, comment:STRING, created_at:DATETIME, updated_at:DATETIME',
    #                                         create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                                         write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #     )

    with beam.Pipeline(options = options) as p_freetext:
        (p_freetext
            |'Read_formvalues' >> ReadFromMongo(connection_string, 'alomobile', 'form_values',
                                      query={'is_deleted':False, 'form_type_id':ObjectId("5770ef13150cd4744f00000b"), 'created_at':{'$gt':datetime(2019,2,24,22,42,11)}},
                                      fields=['value', 'form_type_id'])
            |'strform'>>beam.Map(lambda x:{'id':x['_id'],
                                      'answer':x['value'], 'form_type_id':str(x['form_type_id'])})
            |'text_value' >> beam.ParDo(GetTextValueFn())
#           |'Debug' >> beam.Map(debug_function)
            | 'writeToBQ_freetext' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','campaign_answer_freetexts'),
                      schema='instance_id:STRING, value_id:STRING, user_id:STRING, text_value:STRING, created_at:DATETIME',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))

        )

#
# if __name__ == '__main__':
#     run()
