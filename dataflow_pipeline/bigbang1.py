import apache_beam as beam
import datetime
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math
import re

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

tdelta = datetime.timedelta(hours=7)
connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.200:27017/alomobile"

#dimensi_doctors
class GetCitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        #add birthyear element
        birthyear = element.get('birthday', None)[-4:]
        if (birthyear):
            element['birthyear'] = int(birthyear)
        else: element['birthyear'] = None

        #add city name element
        if (element['city_id']):
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])})
            for city in city_query:
                element['city'] = city['name']
                data.append(element)
        else:
            element['city'] = None
            data.append(element)
        return data

class GetSpecialitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element['doctor_speciality_id'] != '':
            speciality_query = db.doctor_specialities.find({'_id':ObjectId(element['doctor_speciality_id'])})
            for speciality in speciality_query:
                element['speciality']=speciality['name']
        else:
            element['speciality']=None
        data.append(element)
        return data

class GetSkuDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element.get('app_product_id'):
            sku_query = db.app_products.find({'_id':ObjectId(element['app_product_id'])})
            for sku in sku_query:
                element['sku']=sku['sku']
                data.append(element)
        else:
            element['sku']=None
            data.append(element)
        return data

#dimensi_review_doctors
def alphanumeric(x):
    x['review'] = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", x['review'])
    return x

#dimensi_campaigns
class GetclientnameCampFn(beam.DoFn):
    def process (self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        clients = []
        client_query = db.clients.find({'_id':ObjectId(element['client_id'])})
        for client in client_query:
            clients.append({'id':str(element['_id']),
                            'name':str(client['name']+" (" + str(element['start_date']+tdelta) [0:19] + " - "+ str(element['end_date']+tdelta)[0:19] + ")"),
                            'start_date':str(element['start_date']+tdelta)[0:19],
                            'end_date':str(element['end_date']+tdelta)[0:19],
                            'updated_at':str(element['updated_at']+tdelta)[0:19]})
        return clients

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'

##    with beam.Pipeline(options = options) as pipeline_doctors:
##       (pipeline_doctors
##         | 'read_doctors' >> ReadFromMongo(connection_string, 'alomobile', 'users', query={'_type':'Core::Doctor'}, fields=['_id', 'firstname', 'lastname', 'gender', 'email', 'city_id', 'birthday', 'status', 'str_number', 'version', 'bank_account_name', 'number_account_bank', 'name_bank', 'npwp', 'domicile', 'doctor_speciality_id', 'job_type', 'app_product_id', 'phone', 'created_at'])
##         | 'get_city_doctor' >> beam.ParDo(GetCitiesDoctorFn())
##         | 'get_specialities_doctor' >> beam.ParDo(GetSpecialitiesDoctorFn())
##         | 'get_sku_doctor' >> beam.ParDo(GetSkuDoctorFn())
##         | 'format_data_doctors' >> beam.Map (lambda element:{'doctor_id':str(element['_id']),
##                                                'city':element['city'],
##                                                'speciality':element['speciality'],
##                                                'status':element['status'],
##                                                'str_number':element['str_number'],
##                                                'gender':element['gender'],
##                                                'fullname':element['firstname'] + ' ' + element['lastname'],
##                                                'bank_account_name':element.get('bank_account_name'," "),
##                                                'sku':element['sku'],
##                                                'phone':element['phone'],
##                                                'job_type':element.get('job_type'," "),
##                                                'version':element.get('version'," "),
##                                                'npwp':element.get('npwp'," "),
##                                                'email':element.get('email'," "),
##                                                'name_bank':element.get('name_bank'," "),
##                                                'number_account_bank':element.get('number_account_bank'," "),
##                                                'birthdate':element['birthday'],
##                                                'birthyear':element['birthyear'],
##                                                'domicile':element.get('domicile'," "),
##                                                'join_at':str(element['created_at'])[0:19]
##                                                })
##         # | 'Debugdoctors' >> beam.Map(debug_function)
##         | 'write_doctorsToBQ' >> beam.io.Write(
##                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','doctors'),
##                             schema='doctor_id:STRING,city:STRING,speciality:STRING,status:STRING,str_number:STRING,gender:STRING,fullname:STRING,bank_account_name:STRING,sku:STRING,phone:STRING,job_type:STRING,version:STRING,npwp:STRING,email:STRING,name_bank:STRING,number_account_bank:STRING,birthdate:STRING,birthyear:INTEGER,domicile:STRING,join_at:STRING',
##                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##     )

##    with beam.Pipeline(options = options) as pipeline_terms:
##       (pipeline_terms
##            | 'Readterm' >> ReadFromMongo(connection_string, 'alomobile', 'terms', query={}, fields=['name'])
##            | 'strTerm' >> beam.Map (lambda x:{ 'id':str(x['_id']), 'topic':x['name']})
##            # | 'DebugTerm' >> beam.Map(debug_function)
##            | 'writeTermToBQ' >> beam.io.Write(
##                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','terms'),
##                             schema='id:STRING, topic:STRING',
##                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##      )
##
##    with beam.Pipeline(options = options) as pipeline_interests:
##       (pipeline_interests
##           |'Readinterests' >> ReadFromMongo(connection_string, 'alomobile', 'interests', query={}, fields=['_id', 'name'])
##           |'strinterests' >> beam.Map (lambda x:{'id':str(x['_id']), 'interest':x['name']})
##           # |'Debuginterests' >> beam.Map(debug_function)
##           | 'writeinterestsToBQ' >> beam.io.Write(
##                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','interests'),
##                      schema='id:STRING, interest:STRING',
##                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##       )
##
    # with beam.Pipeline(options = options) as pipeline_intents:
    #    (pipeline_intents
    #       | 'Readintents' >> ReadFromMongo(connection_string, 'alomobile', 'intents',
    #                                   query={'is_deleted':False}, fields=['name'])
    #       |'strintents' >> beam.Map (lambda x:{'id':str(x['_id']), 'intent_name':x['name']})
    #       # |'Debugintents' >> beam.Map(debug_function)
    #       | 'writeintentsToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','intents'),
    #                               schema='id:STRING, intent_name:STRING',
    #                               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                               write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #    )
##
##    with beam.Pipeline(options = options) as pipeline_subintents:
##       (pipeline_subintents
##           |'Readsubintents' >> ReadFromMongo(connection_string, 'alomobile', 'sub_intents', query={'is_active':True, 'is_disabled':False, 'is_deleted':False}, fields=['topic'])
##           |'strsubintents' >> beam.Map (lambda x:{'id':str(x['_id']), 'subintent_name':x['topic']})
##           # |'Debugsubintents' >> beam.Map(debug_function)
##           | 'writesubintentsToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','subintents'),
##                                   schema='id:STRING, subintent_name:STRING',
##                                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##       )
##
##
    with beam.Pipeline(options = options) as pipeline_payment_methods:
      (pipeline_payment_methods
          | 'Readpaymentmethod'>> ReadFromMongo(connection_string, 'alomobile', 'payment_methods', query={},
                                        fields=['payment_type', 'payment_section', 'name', 'code'])
          | 'strpayment_methods' >> beam.Map (lambda x:{'id':str(x['_id']),
                                                        'name':x['name'],
                                                        'payment_section':x['payment_section'],
                                                        'code':x['code']})
          # | 'Debugpayment_methods' >> beam.Map(debug_function)
          | 'writepayment_methodsToBQ' >> beam.io.Write(
                            beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','payment_methods'),
                            schema='id:STRING, name:STRING, payment_section:STRING, code:STRING',
                            create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                            write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
      )
##
##    with beam.Pipeline(options = options) as pipeline_payment_gateways:
##       (pipeline_payment_gateways
##           | 'Readpayment_gateways'>> ReadFromMongo(connection_string, 'alomobile', 'payment_gateways',
##                                         query={},
##                                         fields=['channel_name', 'code'])
##           | 'strpayment_gateways' >> beam.Map (lambda x:{'id':str(x['_id']), 'channel_name':x['channel_name'], 'code':x['code']})
##           # | 'Debugpayment_gateways' >> beam.Map(debug_function)
##           | 'writepayment_gatewaysToBQ' >> beam.io.Write(
##                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','payment_gateways'),
##                             schema='id:STRING, channel_name:STRING, code:STRING',
##                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##       )
##
##    with beam.Pipeline(options = options) as pipeline_app_products:
##       (pipeline_app_products
##           | 'ReadappProducts'>> ReadFromMongo(connection_string, 'alomobile', 'app_products',
##                                         query={},
##                                         fields=['package_name', 'sku', 'status', 'default_price', 'updated_at'])
##           | 'strapp_products' >> beam.Map (lambda x:{'id':str(x['_id']),
##                                       'package_name':x['package_name'],
##                                       'status':x['status'],
##                                       'sku':x['sku'],
##                                       'default_price':x['default_price'],
##                                       'updated_at':str(x['updated_at']+tdelta)[0:19]})
##           # | 'Debugapp_products' >> beam.Map(debug_function)
##           | 'writeapp_productsToBQ' >> beam.io.Write(
##                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','app_products'),
##                             schema='id:STRING, package_name:STRING, sku:STRING, status:STRING, default_price:INTEGER, updated_at:DATETIME',
##                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##         )
##
##    with beam.Pipeline(options = options) as pipeline_payment_provider:
##        (pipeline_payment_provider
##              | 'Readpayment_provider'>> ReadFromMongo(connection_string, 'alomobile', 'payment_providers',
##                                         query={},
##                                         fields=['name'])
##              | 'strpayment_provider' >> beam.Map (lambda x:{'id':str(x['_id']), 'name':x['name']})
##              # | 'Debugpayment_provider' >> beam.Map(debug_function)
##              | 'writepayment_providerToBQ' >> beam.io.Write(
##                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','payment_providers'),
##                             schema='id:STRING, name:STRING',
##                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##        )
##
##    with beam.Pipeline(options = options) as pipeline_conclusions:
##       (pipeline_conclusions
##           |'Readconclusions' >> ReadFromMongo(connection_string, 'alomobile', 'question_conclusions',
##                                      query={},
##                                      fields=(['status', 'created_at', 'conclusion', 'updated_at']))
##           |'strconclusions'>>beam.Map (lambda x:{'id':str(x['_id']),
##                                 'status':x['status'],
##                                 'created_at':str(x['created_at']+tdelta)[0:19],
##                                 'updated_at':str(x['updated_at']+tdelta)[0:19],
##                                 'conclusion':x['conclusion']})
##           # |'Debugconclusions' >> beam.Map(debug_function)
##           | 'writeconclusionsToBQ' >> beam.io.Write(
##                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','conclusions'),
##                      schema='id:STRING, status:STRING, created_at:DATETIME, updated_at:DATETIME, conclusion:STRING',
##                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##
##       )
##
##    with beam.Pipeline(options = options) as pipeline_review_doctors:
##        (pipeline_review_doctors
##           | 'Readreviewsdoctors' >> ReadFromMongo(connection_string, 'alomobile', 'review_doctors',
##                                       query={},
##                                      fields=['_id', 'status', 'review',
##                                              'is_like', 'doctor_id',
##                                              'user_id', 'created_at'])
##           |'alphanumericreviewsdoctors' >> beam.Map(alphanumeric)
##           |'strdoctors' >> beam.Map (lambda x:{'id':str(x['_id']), 'status':x['status'],
##                                   'review':x['review'], 'is_like':x['is_like'],
##                                   'doctor_id':str(x.get('doctor_id', None)), 'user_id':str(x['user_id']),
##                                   'created_at':str(x['created_at']+tdelta)[0:19]})
##           # | 'Debugdoctors' >> beam.Map(debug_function)
##           | 'writereviewsdoctorsToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','review_doctors'),
##                                   schema='id:STRING, status:STRING, review:STRING, is_like:BOOLEAN, doctor_id:STRING, user_id:STRING, created_at:DATETIME',
##                                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##        )
##
##    with beam.Pipeline(options = options) as pipeline_campaigns:
##       (pipeline_campaigns
##           |'Readcampaign' >> ReadFromMongo(connection_string, 'alomobile', 'campaigns',
##                                   query={}, fields=['start_date', 'end_date', 'client_id', 'updated_at'])
##           |'getclientnameCamp'>>beam.ParDo(GetclientnameCampFn())
##           # |'Debugcampaign' >> beam.Map(debug_function)
##           | 'writecampaignToBQ' >> beam.io.Write(
##                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','campaigns'),
##                      schema='id:STRING, name:STRING,start_date:DATETIME, end_date:DATETIME, updated_at:DATETIME',
##                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##      )
##
##    with beam.Pipeline(options = options) as pipeline_campaign_video:
##       (pipeline_campaign_video
##           |'ReadusersCV'>> ReadFromMongo(connection_string, 'alomobile', 'video_ad_campaigns',
##                                         query={},
##                                         fields=['_id', 'start_date', 'end_date', 'updated_at'])
##           |'strCV' >> beam.Map(lambda x:{'video_campaign_id':str(x['_id']),
##                                     'start_date':str(x['start_date']+tdelta)[0:19],
##                                     'end_date':str(x['end_date']+tdelta)[0:19],
##                                     'updated_at':str(x['updated_at']+tdelta)[0:19]})
##           # |'DebugCV' >> beam.Map(debug_function)
##           | 'writeCVToBQ' >> beam.io.Write(
##                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','campaign_video'),
##                      schema='video_campaign_id:STRING, start_date:DATETIME, end_date:DATETIME, updated_at:DATETIME',
##                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
##                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
##       )
    #
    # with beam.Pipeline(options = options) as pipeline_ads_unit:
    #    (pipeline_ads_unit
    #        | 'Readads_unit'>> ReadFromMongo(connection_string, 'alomobile', 'video_ads',
    #                                      query={},
    #                                      fields=['_id', 'ad_unit_name', 'ad_unit_url', 'updated_at'])
    #        |'strads_unit' >> beam.Map (lambda x:{'id':str(x['_id']), 'name':x['ad_unit_name'], 'url':x['ad_unit_url'], 'updated_at':str(x['updated_at']+tdelta)[0:19]})
    #        # |'Debugads_unit' >> beam.Map(debug_function)
    #        | 'writeads_unitToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','ads_unit'),
    #                   schema='id:STRING, name:STRING, url:STRING, updated_at:DATETIME',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #      )

    # with beam.Pipeline(options = options) as pipeline_campaign_questions:
    #     (pipeline_campaign_questions
    #         |'ReadCQ' >> ReadFromMongo(connection_string, 'alomobile', 'forms',
    #                                   query={},
    #                                   fields=(['_id', 'question', 'created_at']))
    #         |'strCQ'>>beam.Map (lambda x:{'id':str(x['_id']),
    #                              'question':x['question'],
    #                              'created_at':str(x['created_at']+tdelta)[0:19]})
    #         # |'DebugCQ' >> beam.Map(debug_function)
    #         | 'writeCQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','campaign_questions'),
    #                   schema='id:STRING, question:STRING, created_at:DATETIME',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #
    #     )
    #
    # with beam.Pipeline(options = options) as pipeline_campaign_answers:
    #    (pipeline_campaign_answers
    #        |'ReadCA' >> ReadFromMongo(connection_string, 'alomobile', 'form_values',
    #                                   query={'is_deleted':False, 'form_type_id' : {'$ne':ObjectId('5770ef13150cd4744f00000b')}},
    #                                   fields=['value', 'form_type_id'])
    #        |'strCA'>>beam.Map (lambda x:{'id':str(x['form_type_id']),
    #                              'answer':x['value']})
    #        # |'DebugCA' >> beam.Map(debug_function)
    #        | 'writeCAToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','campaign_answers'),
    #                   schema='id:STRING, answer:STRING',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #
    #   )
