
# coding: utf-8

# In[1]:

import apache_beam as beam
import re
import uuid, math
import itertools
from datetime import datetime, timedelta
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from google.oauth2.service_account import Credentials
from bson.objectid import ObjectId
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from google.cloud import bigquery
def debug_function(pcollection_as_list):
    print (pcollection_as_list)

#connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"
connection_alodokter = "mongodb://grumpycat:alo.1975.dokter@35.240.137.27/alodokter"
# start_datetime = datetime.utcnow()-timedelta(hours=4)

scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)

# table_names = {'fact_users-cr':None, 'fact_magazine-cr':None, 'fact_video_campaign-cr':None, 'fact_campaign-cr':None, 'factquestions-cr':None}
# from google.cloud import bigquery
# client = bigquery.Client()
# dataset_ref = client.dataset('alobrain_data_staging')
# for key,val in table_names.items():
#     data = key.split("-")
#     #field default : updated_at
#     field = 'updated_at'
#     if (data[1] == 'cr'):
#         field = 'created_at'
#     elif (data[1] == 'join'):
#         field = 'join_at'
#     sql = "SELECT MAX ("+ field +") FROM [plenary-justice-151004.alowarehouse_alodoktermobile."+data[0]+"]"
#     query = client.run_sync_query(sql)
#     query.run()
#     table_names[key] = query.rows[0][0]

#dimensi_referrals
class GetReferralFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        referrals = []
        name = []
        referral_query = db.answers.find({'question_id':ObjectId(element['_id']), "$or":[ {"answer_type":'50'}, {"answer_type":'51'}, {'answer_type':'52'}, {'answer_type':'53'}]})
        for referral in referral_query:
            if referral['answer_type'] == '50':
                referral['answer_type'] = 'specialist chat'
            elif referral['answer_type'] == '51':
                referral['answer_type'] = 'specialist booking'
            elif referral['answer_type'] == '52':
                referral['answer_type'] = 'procedure medical treatment'
            elif referral['answer_type'] == '53':
                referral['answer_type'] = 'emergency'

            referrals.append({'question_id':str(element['_id']),
                             'referral_id':referral['referral_id'],
                             'referral_name':referral['referral_name'],
                             'answer_type':referral['answer_type'],
                             'created_at':str(element['created_at'])[0:19] })
            return referrals

#dimensi_doctors
class GetCitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        #add birthyear element
        birthyear = element.get('birthday', None)[-4:]
        if (birthyear):
            element['birthyear'] = int(birthyear)
        else: element['birthyear'] = None

        #add city name element
        if (element['city_id']):
            city_query = db.cities.find({'_id':element['city_id']})
            for city in city_query:
                element['city'] = city['name']
                data.append(element)
        else:
            element['city'] = None
            data.append(element)
        return data

class GetSpecialitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element['doctor_speciality_id'] != '':
            speciality_query = db.doctor_specialities.find({'_id':element['doctor_speciality_id']})
            for speciality in speciality_query:
                element['speciality']=speciality['name']
        else:
            element['speciality']=None
        data.append(element)
        return data

class GetSkuDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element.get('app_product_id'):
            sku_query = db.app_products.find({'_id':ObjectId(element['app_product_id'])})
            for sku in sku_query:
                element['sku']=sku['sku']
                data.append(element)
        else:
            element['sku']=None
            data.append(element)
        return data

#dimensi_review_doctors
def alphanumeric(x):
    x['review'] = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", x['review'])
    return x

#dimensi_journal
def paid_timezoneJournals(j):
    if j['paid_time'] is not None:
        j['paid_time'] = str(j['paid_time']+timedelta(hours=7))[0:19]
    return j

def gross_amountJournals(j):
    if j['gross_amount'] is not None:
        j['gross_amount'] = int(j['gross_amount'])
    return j

#dimensi questions
def join_listsQ((k, v)):
    return itertools.product(v['number_of_chatbox'], v['questions'])

def mergedictsQ(questions_dict_chatbox):
    (chatbox, questions_dict) = questions_dict_chatbox
    a_dict = questions_dict
    a_dict.update(chatbox)
    return a_dict

class GetPriceQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        prices = []
        if element.get('journal_id', None) is not None:
                price_query = db.journals.find({'_id':element['journal_id']})
                for price in price_query:
                    if price.get('gross_amount', None) is not None:
                        prices.append({'id':str(element['_id']),
                                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                                        'price':int(price.get('gross_amount', None)),
                                        'pickup_fee':4000, 'like_fee':1000,
                                        'title':element['title'],
                                        'content':element['content'],
                                        'is_said_thanks':element['is_said_thanks'],
                                        'is_paid':element.get('is_paid'),
                                        'is_closed':element['is_closed'],
                                        'type':element['_type'],
                                        'sub_intent_id':element.get('sub_intent_id', None),
                                        'intent_id':element.get('intent_id', None),
                                        'is_has_conclusion':element.get('is_has_conclusion', None),
                                        'is_shown':element.get('is_shown', None)})
                    elif price.get('gross_amount', None) is None:
                        prices.append({'id':str(element['_id']),
                                       'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                                       'price':None, 'pickup_fee':4000,
                                       'like_fee':1000,
                                       'title':element['title'],
                                       'content':element['content'],
                                       'is_said_thanks':element['is_said_thanks'],
                                       'is_paid':None,
                                       'is_closed':element['is_closed'],
                                       'type':element['_type'],
                                       'sub_intent_id':element.get('sub_intent_id', None),
                                       'intent_id':element.get('intent_id', None),
                                       'is_has_conclusion':element.get('is_has_conclusion', None),
                                       'is_shown':element.get('is_shown', None)})
        elif element.get('journal_id', None) is None:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        else:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        return prices


def is_chatbotQ(q):
       if q['intent_id'] or q['sub_intent_id'] is not None:
           is_chatbot = {'is_chatbot':True}
       elif q['intent_id'] and q['sub_intent_id'] is None:
           is_chatbot = {'is_chatbot':False}
       else:
           is_chatbot = {'is_chatbot':None}

       if q['type'] == "Core::PreQuestion":
           autochat_close = {'autochat_close':True}
       elif q['type'] == "Core::Question":
           autochat_close = {'autochat_close':False}
       else:
           autochat_close = {'autochat_close':None}

       q.update(is_chatbot)
       q.update(autochat_close)
       q.pop('intent_id')
       q.pop('sub_intent_id')
       return q

class GetChatBoxQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        chatboxes = []
        number_of_chatbox = db.answers.find({'question_id':ObjectId(element['id'])}, no_cursor_timeout=True).count()
        chatboxes.append({'id':str(element['id']),
                          'updated_at':element['updated_at'],
                          'number_of_chatbox':number_of_chatbox,
                          'price':element['price'],
                          'pickup_fee':element['pickup_fee'],
                          'like_fee':element['like_fee'],
                          'title':element['title'],
                          'content':element['content'],
                          'is_said_thanks':element['is_said_thanks'],
                          'is_paid':element['is_paid'],
                          'is_closed':element['is_closed'],
                          'type':element['type'],
                          'is_has_conclusion':element.get('is_has_conclusion', None),
                          'is_shown':element.get('is_shown', None)})
        return chatboxes

#dimensi_campaigns
class GetclientnameCampFn(beam.DoFn):
    def process (self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        clients = []
        client_query = db.clients.find({'_id':ObjectId(element['client_id'])})
        for client in client_query:
            clients.append({'id':str(element['_id']),
                            'name':str(client['name']+" (" + str(element['start_date']+timedelta(hours=7)) [0:19] + " - "+ str(element['end_date']+timedelta(hours=7))[0:19] + ")"),
                            'start_date':str(element['start_date']+timedelta(hours=7))[0:19],
                            'end_date':str(element['end_date']+timedelta(hours=7))[0:19],
                            'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19]})
        return clients


#dimensi video campaigns tag
class DenormalizeVCTFn(beam.DoFn):
    def process(self,element):
        data = []
        if element.get('tags'):
            for tag in element['tags']:
                data.append({'video_campaign_id':element['_id'], 'tag':tag})
        else: data.append({'video_campaign_id':element['_id'], 'tag':None})
        return data

class GetTagIDVCTFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        tag_list = []
        if element.get('tag'):
            tag_query = db.terms.find({'name':element['tag']})
            for tag in tag_query:
                tag_list.append({'video_campaign_id':str(element['video_campaign_id']),
                                'tag_id':str(tag['_id'])
                                })
        else:
            tag_list.append({'video_campaign_id':str(element['video_campaign_id']),
                                'tag_id':None
                                })

        return tag_list

#dimensi fact users
def join_infoUC((k,v)):
    return itertools.product(v['user_id'], v['channel_id'])

def merge_dictUC(user_channel):
      (user, channel) = user_channel
      a_dict = user
      a_dict.update(channel)
      return a_dict

#dimensi meta question
def maxrecMetaQ(m):
        if m.get('max_count_recommendation', None) is None:
                m['max_count_recommendation'] = 3
        return m

#dimensi fact_magazine
class DenormalizeFMFn(beam.DoFn):
    def process(self, element):
        magazines_relationships = element.get('magazine_relationships')
        magazines = []
        if magazines_relationships is not None:
            if (len(magazines_relationships) > 0):
                for term_taxonomy_ids in magazines_relationships:
                    for data in [term_taxonomy_ids]:
                        magazines.append({'id':str(uuid.uuid4()),
                                          'article_id':element['post_id'],
                                          'term_taxonomy_id':data['term_taxonomy_id'],
                                          'created_at':element['created_at']})
        else:
                    magazines.append({'id':str(uuid.uuid4()),
                              'article_id':element['post_id'],
                              'term_taxonomy_id':None,
                              'created_at':element['created_at']})
        return magazines

class GetTermMagFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        term_taxonomies = db.term_taxonomies
        term_list = []
        if element.get('term_taxonomy_id', None) is not None:
            for term in term_taxonomies.find({'_id':ObjectId(element['term_taxonomy_id'])}):
                term_list.append({'id':element['id'],
                                  'article_id':element['article_id'],
                                  'term_id':str(term['term_id']),
                                  'created_at':element['created_at']})
        else:
                term_list.append({'id':element['id'],
                      'article_id':element['article_id'],
                      'term_id':None,
                      'created_at':element['created_at']})
        return term_list


#dimensi fact video campaign
class UnlistfvcFn(beam.DoFn):
    def process(self, element):
        tags = element.get('tags', [])
        tag_list = []
        for data in tags:
                tag_list.append({'tags':data,
                                'campaign_id':element['video_ad_campaign_id'],
                                'user_id':element['user_id'],
                                'created_at':element['created_at']})
        return tag_list

class GetTagfvcFn(beam.DoFn):
    def process (self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        termID_list = []
        terms = db.terms
        for term in terms.find({'name':element['tags']}):
            campaign_query = db.video_ad_campaigns.find({'_id':element['campaign_id']})
            for campaign in campaign_query:
                termID_list.append({'id':str(uuid.uuid4()),
                                     'ads_unit_id':str(campaign['video_ad_id']),
                                     'user_id':str(element['user_id']),
                                     'campaign_id':str(element['campaign_id']),
                                     'tag_id':str(term['_id']),
                                     'created_at':str(element['created_at']+timedelta(hours = 7))[0:19],
                                     'year_id':element['created_at'].year,
                                     'month_id':element['created_at'].month,
                                     'date_id':element['created_at'].day,
                                     'hour_id':element['created_at'].hour,
                                     'minute_id':element['created_at'].minute
                                    })
        return termID_list

#fact_doctors
class DenormalizedFDFn(beam.DoFn):
    def process (self,element):
        data = []
        if element.get("educations"):
            for education in element['educations']:
                data.append({'doctor_id':str(element['_id']), 'year':int(education['year_of_graduation']), 'university':education['name']})
        else:
             data.append({'doctor_id':str(element['_id']), 'year':None, 'university':None})
        return data

class GetUniversityFn(beam.DoFn):
    def process (self,element):
        client = bigquery.Client()
        dataset_ref = client.dataset('alowarehouse_alodoktermobile')
        doctors = []
        if (element['university'] is not None):
            if "\"" in element['university']:
                element['university'] = element['university'].replace ("\"",'\\"')
            sql = ("SELECT id "
                       + "FROM [plenary-justice-151004:alowarehouse_alodoktermobile.universities] "
                       + "WHERE university = \"" + element['university'] + "\""
                  )
            query = client.run_sync_query(sql)
            query.run()
            universities = []
            university_id = query.rows[0][0]
        else:university_id = None
        doctors.append({'id': str(uuid.uuid4()),'doctor_id':str(element['doctor_id']),
                        'university_id':university_id,
                        'graduate_year_id':element['year']})
        return doctors

#dimensi fact campaigns
class GetTagFCFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        campaign_query = db.campaigns.find({'_id':element['campaign_id']},{'_id':1,'tags':1})
        for campaign in campaign_query:
            if (campaign.get('tags')): #if campaign tags is exsist
                for campaign_tag in campaign['tags']:
                    term_query = db.terms.find({'name':campaign_tag},{'_id':1})
                    for tag_id in term_query:
                        data.append({'id':element['_id'],
                                    'form_instance_values': element['form_instance_values'],
                                    'tag_id':tag_id['_id'],
                                    'client_id':element['client_id'],
                                    'campaign_id':element['campaign_id'],
                                    'user_id':element['user_id'],
                                    'created_at':element['created_at']+timedelta(hours=7)}
                                   )
            else:
                data.append ({'id':element['_id'],
                                'form_instance_values': element['form_instance_values'],
                                'tag_id':None,
                                'client_id':element['client_id'],
                                'campaign_id':element['campaign_id'],
                                'user_id':element['user_id'],
                                'created_at':element['created_at']+timedelta(hours=7)}
                              )
        return data

class GetQnAFC(beam.DoFn):
    def process(self, element):
        data = []
        for question in element['form_instance_values']:
            if (question.get('form_value_ids')): #jika pertanyaan memiliki jawaban
                for answer in question['form_value_ids']:
                    if (answer == '5770ef13150cd4744f00000b'):
                        answer = questions['text_value']
                    data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': str(answer),
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
            else: data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': None,
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
        return data

#dimensi user interest
class GetUserInterestFn(beam.DoFn):
    def process(self,element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        if element.get('interest_ids'):
            for interest_id in element ['interest_ids']:
                data.append({'user_id':str(element['_id']),
                            'interest_id':str(interest_id),
                            'is_deleted':False})
        else: data.append({'user_id':str(element['_id']),
                           'interest_id':None,
                           'is_deleted':False})
        return data

#dimensi users
class GetCitiesUsersFn(beam.DoFn):
    def process(self, element):
        if element.get('birthday', None):
            birth_year = int(element['birthday'][-4:])
        else: birth_year = None

        if (element['provider']):
            registration_channel = element['provider']
        else: registration_channel = "email"

        if element.get('city_id', None) is not None:
            client = MongoClient(connection_string)
            db = client.alomobile
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])}, no_cursor_timeout=True)
            for city in city_query:
                city_name = city['name']
        else: city_name = None

        data = []
        data.append({'id':str(element['_id']),
                     'fullname':element['firstname'] + " " + element['lastname'],
                     'city':city_name,
                     'email':element.get('email', None),
                     'phone':element.get('phone', None),
                     'created_at':str(element['created_at'] +timedelta(hours=7))[0:19],
                     'gender':element.get('gender',""),
                     'birthdate':element.get('birthday', ""),
                     'birthyear': birth_year,
                     'registration_channel':registration_channel,
                     'version':element.get('version',""),
                     'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19]})
        return data

#fact_questions
class if_meta_question(beam.DoFn):
    def process(self, element):
        data = []
        if element.get('time_picked_up') is not None:
            time_picked_up = str(element['time_picked_up']+timedelta(hours=7))[0:19]
            date_picked_up = str(time_picked_up)[0:10]
        else:
            time_picked_up = None
            date_picked_up = None

        if element.get('_type') == "Core::Question" or element.get('_type') == "Core::PreQuestion":
            question_id = str(element['_id'])
            meta_question_id = str(element.get('meta_question_id',""))
        elif element.get('_type') == "Core::MetaQuestion":
            question_id = None
            meta_question_id = str(element['_id'])

        if element.get('first_user_answer_time') is not None:
            first_user_answer_time = str(element['first_user_answer_time']+timedelta(hours=7))[0:19]
        else:
            first_user_answer_time = None

        if element.get('first_doctor_answer_time') is not None:
            first_doctor_answer_time = str(element['first_doctor_answer_time']+timedelta(hours=7))[0:19]
        else:
            first_doctor_answer_time = None

        data.append({'topic':element.get('topic', None),
                    'question_id':question_id,
                    'meta_question_id':meta_question_id,
                    'user_id':str(element['user_id']),
                    'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                    'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                    'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                    'intent_id':str(element.get('intent_id',"")),
                    'sub_intent_id':str(element.get('sub_intent_id',"")),
                    'time_picked_up':time_picked_up,
                    'journal_id':str(element.get('journal_id')),
                    'date_picked_up':date_picked_up,
                    'first_user_answer_time':first_user_answer_time,
                    'first_doctor_answer_time':first_doctor_answer_time,
                    'is_has_conclusion':element.get('is_has_conclusion', None),
                    'no_conclusion':element.get('no_conclusion', None)})
        return data

class GetTermsFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_alodokter)
        db = client.alodokter
        terms = []
        term_query = db.core_terms.find({'name':re.compile(element['topic'], re.IGNORECASE)}, no_cursor_timeout=True).limit(1)

        for term in term_query:
            if element['topic'] == "":
                term['_id'] = ""
            else:
                term['_id'] = term['_id']

            terms.append({'topic_id':str(term['_id']),
                          'question_id':element['question_id'],
                          'meta_question_id':element['meta_question_id'],
                          'user_id':element['user_id'],
                          'picked_up_by_id':element['picked_up_by_id'],
                          'created_at':element['created_at'],
                          'updated_at':element['updated_at'],
                          'intent_id':element['intent_id'],
                          'sub_intent_id':element['sub_intent_id'],
                          'time_picked_up':element['time_picked_up'],
                          'journal_id':element['journal_id'],
                          'date_picked_up':element['date_picked_up'],
                          'first_user_answer_time':element['first_user_answer_time'],
                          'first_doctor_answer_time':element['first_doctor_answer_time'],
                          'is_has_conclusion':element['is_has_conclusion'],
                          'no_conclusion':element['no_conclusion']})
        return terms

class GetConclusionFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        conclusions = []
        if element.get('is_has_conclusion') is True:
            conclusion_query = db.question_conclusions.find({'question_id':ObjectId(element['question_id'])})
            for conclusion in conclusion_query:
                conclusion_id = str(conclusion['_id'])
        else: conclusion_id = None
        conclusions.append({'conclusion_id':conclusion_id,
                        'topic_id':element['topic_id'],
                        'question_id':element['question_id'],
                        'meta_question_id':element['meta_question_id'],
                        'user_id':element['user_id'],
                        'picked_up_by_id':element['picked_up_by_id'],
                        'created_at':element['created_at'],
                        'updated_at':element['updated_at'],
                        'intent_id':element['intent_id'],
                        'sub_intent_id':element['sub_intent_id'],
                        'time_picked_up':element['time_picked_up'],
                        'journal_id':element['journal_id'],
                        'date_picked_up':element['date_picked_up'],
                        'first_user_answer_time':element['first_user_answer_time'],
                        'first_doctor_answer_time':element['first_doctor_answer_time'],
                        'is_has_conclusion':element['is_has_conclusion'],
                        'no_conclusion':element['no_conclusion']})
        return conclusions

class GetPaymentFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        journals = []
        if element['journal_id'] != 'None' :
            journal_query = db.journals.find({'_id':ObjectId(element['journal_id'])})
            for journal in journal_query:
                journal_id = element['journal_id']
                payment_method_id = str(journal['payment_method_id'])
        else:
            journal_id = None
            payment_method_id = None
        journals.append({'id':str(uuid.uuid4()),
                         # 'conclusion_id':element['conclusion_id'],
                         'topic_id':element['topic_id'],
                         'question_id':element['question_id'],
                         'meta_question_id':element['meta_question_id'],
                         'user_id':element['user_id'],
                         'picked_up_by_id':element['picked_up_by_id'],
                         'created_at':element['created_at'],
                         'updated_at':element['updated_at'],
                         'intent_id':element['intent_id'],
                         'sub_intent_id':element['sub_intent_id'],
                         'time_picked_up':element['time_picked_up'],
                         'journal_id':journal_id,
                         'payment_method_id':payment_method_id,
                         'date_picked_up':element['date_picked_up'],
                         'first_user_answer_time':element['first_user_answer_time'],
                         'first_doctor_answer_time':element['first_doctor_answer_time'],
                         'is_has_conclusion':element['is_has_conclusion'],
                         'no_conclusion':element['no_conclusion']})
        return journals

#meta_slot
class GetDayFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        days = []
        day_query = db.meta_slot_days.find({'_id':element["meta_slot_day_id"]})
        for day in day_query:
            for time_slot in element['doctor_time_slots']:
                if time_slot['end_of_date'] is not None:
                    if time_slot['end_of_date'] != 'None':
                        if time_slot['end_of_date'] != '':
                            days.append({'day':day['name'],
                                         'meta_slot_id':str(element['_id']),
                                         'time_slot_id':element['time_slot_management_id'],
                                         'doctor_id':str(time_slot['doctor_id']),
                                         'start_of_date':str(time_slot['start_of_date'])[0:10],
                                         'end_of_date':str(time_slot['end_of_date'])[0:10],
                                         'end_day':str(time_slot['end_of_date'].isoweekday()),
                                         'booking_time':str(time_slot['created_at'])[0:19]})
                if time_slot['end_of_date'] is None or time_slot['end_of_date'] == "None" or time_slot['end_of_date'] == '':
                    days.append({'day':day['name'],
                                         'meta_slot_id':str(element['_id']),
                                         'time_slot_id':element['time_slot_management_id'],
                                         'doctor_id':str(time_slot['doctor_id']),
                                         'start_of_date':str(time_slot['start_of_date'])[0:10],
                                         'end_of_date':None,
                                         'end_day':None,
                                         'booking_time':str(time_slot['created_at'])[0:19]})
            return days

class GetTimeFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        times = []
        time_query = db.time_slot_managements.find({'_id':element['time_slot_id']})
        for time in time_query:
            times.append({'meta_slot_id':element['meta_slot_id'],
                          'day':element['day'],
                          'start_time':time['start_time'],
                          'end_time':time['end_time'],
                          'doctor_type':time['doctor_type'],
                          'doctor_id':element['doctor_id'],
                          'start_of_date':element['start_of_date'],
                          'end_of_date':element['end_of_date'],
                          'end_day':element['end_day'],
                          'booking_time':element['booking_time']})
        return times

class GetMetaFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        metaslots = []
        metaslot_query = db.meta_slots.find({'_id':ObjectId(element['meta_slot_id'])})
        for metaslot in metaslot_query:
            metaslots.append({'doctor_id':str(element['doctor_id']),
                               'time_slot_management_id':metaslot['time_slot_management_id'],
                               'slot_id':str(element['_id']),
                               'date_slot':str(element['date_slot'])[0:10],
                               'created_at':str(element['created_at'])[0:19]})
            return metaslots

class GetTimeSingleFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        times = []
        time_query = db.time_slot_managements.find({'_id':ObjectId(element['time_slot_management_id'])})
        for time in time_query:
            times.append({'doctor_id':str(element['doctor_id']),
                          'start_time':time['start_time'],
                          'end_time':time['end_time'],
                          'doctor_type':time['doctor_type'],
                          'slot_id':element['slot_id'],
                          'date_slot':element['date_slot'],
                          'created_at':element['created_at']})
        return times

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]

    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'


    with beam.Pipeline(options = options) as p_ratingdoctor:
        (p_ratingdoctor
         | 'read_doctors' >> ReadFromMongo(connection_string, 'alomobile', 'users', query={'_type':'Core::Doctor'}, fields=['_id', 'rate', 'score', 'is_specialist', 'total_positive_review', 'total_negative_review', 'total_review', 'total_question', 'thanks', 'updated_at'])
         | 'format_data' >> beam.Map (lambda element:{'doctor_id':str(element['_id']),
                                                      'rate':element.get('rate'),
                                                      'score':element.get('score'),
                                                      'is_specialist':element.get('is_specialist'),
                                                      'total_positive_review':element.get('total_positive_review'),
                                                      'total_negative_review':element.get('total_negative_review'),
                                                      'total_review':element.get('total_review'),
                                                      'total_question':element.get('total_question'),
                                                      'thanks':element.get('thanks'),
                                                      'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19]
                                                })
#          | 'Debug' >> beam.Map(debug_function)
         | 'writeToBQ' >> beam.io.Write(
                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','doctor_rating'),
                             schema='doctor_id:STRING, rate:INTEGER, score:FLOAT64, is_specialist:BOOLEAN, total_positive_review:INTEGER, total_negative_review:INTEGER, total_review:INTEGER, total_question:INTEGER, thanks:INTEGER, updated_at:DATETIME',
                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
       )

    with beam.Pipeline(options = options) as p_singleslot:
        (p_singleslot
            | 'read_slot' >> ReadFromMongo(connection_string, 'alomobile', 'single_meta_slots', query={'is_deleted':False, 'date_slot':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}}, fields=['_id', 'date_slot', 'doctor_id', 'meta_slot_id', 'created_at'])
            | 'get_meta' >> beam.ParDo(GetMetaFn())
            | 'get_time_single' >> beam.ParDo(GetTimeSingleFn())
#           | 'Debug' >> beam.Map(debug_function)
            | 'writeToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','single_meta_slots'),
                     schema= 'slot_id:STRING, doctor_id:STRING, date_slot:DATE, start_time:STRING,  end_time:STRING, created_at:DATETIME, doctor_type:STRING',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )

    with beam.Pipeline(options = options) as p_weeklyslot:
        (p_weeklyslot
            | 'read_slot' >> ReadFromMongo(connection_string, 'alomobile', 'meta_slots', query={}, fields=["meta_slot_day_id", "time_slot_management_id", "doctor_time_slots.doctor_id", "doctor_time_slots.start_of_date", "doctor_time_slots.end_of_date","doctor_time_slots.created_at"])
            | 'get_day' >> beam.ParDo(GetDayFn())
            | 'get_time' >> beam.ParDo(GetTimeFn())
        #   | 'get_date' >> beam.ParDo(GetDateFn())
    #       | 'Debug' >> beam.Map(debug_function)
            | 'writeToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','weekly_meta_slots'),
                     schema= 'booking_time:DATETIME, start_time:STRING, end_time:STRING, start_of_date:STRING, end_of_date:STRING, doctor_id:STRING, meta_slot_id:STRING, day:STRING, doctor_type:STRING, end_day:STRING',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options=options) as p_automatic_subintent:
       (p_automatic_subintent
            | 'Readquestions' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={"created_at":{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}, 'top_sub_intent_recommendation':{"$ne":{}}, '_type':'Core::MetaQuestion'},
                                                                                            fields=['choice', 'top_sub_intent_recommendation.score', 'top_sub_intent_recommendation.topic', 'created_at']
                                                                                            )
            | 'format' >> beam.Map(lambda x:{'id':str(x['_id']), 'topic':x['top_sub_intent_recommendation']['topic'], 'score':x['top_sub_intent_recommendation']['score'], 'choice':x['choice'], 'created_at':str(x['created_at']+timedelta(hours=7))[0:19]})
#             | 'DebugQ' >> beam.Map(debug_function)
            | 'writeQToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','automatic_subintent_date'),
                     schema = 'id:STRING, topic:STRING, score:FLOAT64, choice:BOOLEAN, created_at:DATETIME',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
       )

    # with beam.Pipeline(options = options) as p_referrals:
    #     (p_referrals
    #            | 'ReadreferralMob' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 # query={'is_referral':True , 'is_closed':True, 'created_at':{'$gt':start_datetime}},
    #                                 query={'is_referral':True, 'is_closed':True, 'created_at'{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}}
    #                                 fields=['_id', 'created_at'])
    #            | 'GetReferral' >> beam.ParDo(GetReferralFn())
    # #            | 'DebugReferral' >> beam.Map(debug_function)
    #            | 'writereferralToBQ' >> beam.io.Write(
    #                         beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','referrals'),
    #                         schema='question_id:STRING, answer_type:STRING, referral_name:STRING, created_at:DATETIME, referral_id:STRING',
    #                         create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                         write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #     )
#
# #12 plan weekly
#     # with beam.Pipeline(options = options) as p_app_products:
#     #   (p_app_products
#     #       | 'ReadAppProdMob'>> ReadFromMongo(connection_string, 'alomobile', 'app_products',
#     #                                     # query={'updated_at':{'$gt':table_names['app_products-upd'] - timedelta(hours = 7)},
#     #                                     # query={'updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}}, #every_night
#     #                                     query={'updated_at':{'$gt':datetime.utcnow()-timedelta(hours=3)}},
#     #                                     fields=['package_name', 'sku', 'status', 'default_price', 'updated_at'])
#     #       | 'strapp_products' >> beam.Map (lambda x:{'id':str(x['_id']),
#     #                                   'package_name':x['package_name'],
#     #                                   'status':x['status'],
#     #                                   'sku':x['sku'],
#     #                                   'default_price':x['default_price'],
#     #                                   'updated_at':str(x['updated_at']+timedelta(hours=7))[0:19]})
#     #       # | 'Debugapp_products' >> beam.Map(debug_function)
#     #       | 'writeapp_productsToBQ' >> beam.io.Write(
#     #                         beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','app_products'),
#     #                         schema='id:STRING, package_name:STRING, sku:STRING, status:STRING, default_price:INTEGER, updated_at:DATETIME',
#     #                         create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#     #                         write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#     #     )
#
#
# # # #13
    # with beam.Pipeline(options = options) as p_article:
    #     (p_article
    #             | 'ReadArticleMob' >> ReadFromMongo(connection_string, 'alomobile', 'magazines',
    #                                   # query={'updated_at':{'$gt':table_names['article_android-upd'] - timedelta(hours = 7)},
    #                                   query={'updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
    #                                   # query={'updated_at':{'$gt':start_datetime}},
    #                                   fields=['post_id', 'title', 'slug', 'is_deleted', 'created_at', 'updated_at'])
    #             | 'strarticle' >> beam.Map (lambda x:{ 'id':str(x['_id']),
    #                                 'post_id':x['post_id'],
    #                                 'title':x['title'],
    #                                 'slug':x.get('slug', None),
    #                                 'is_deleted':x['is_deleted'],
    #                                 'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
    #                                 'updated_at':str(x['updated_at']+timedelta(hours=7))[0:19]
    #                                })
    #             # | 'Debugarticle' >> beam.Map(debug_function)
    #             | 'writearticleToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','article_android'),
    #                   schema='id:STRING, post_id:STRING, title:STRING, slug:STRING, created_at:DATETIME, updated_at:DATETIME, is_deleted:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #     )
# # #14
#     with beam.Pipeline(options = options) as p_campaign_video:
#       (p_campaign_video
#           |'ReadCampVidMob'>> ReadFromMongo(connection_string, 'alomobile', 'video_ad_campaigns',
#                                         # query={'updated_at':{'$gt':table_names['campaign_video-upd'] - timedelta(hours = 7)},
#                                         # query={'updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
#                                         query={'updated_at':{'$gt':start_datetime}},
#                                         fields=['_id', 'start_date', 'end_date', 'updated_at'])
#           |'strCV' >> beam.Map(lambda x:{'video_campaign_id':str(x['_id']),
#                                     'start_date':str(x['start_date']+timedelta(hours=7))[0:19],
#                                     'end_date':str(x['end_date']+timedelta(hours=7))[0:19],
#                                     'updated_at':str(x['updated_at']+timedelta(hours=7))[0:19]})
#           # |'DebugCV' >> beam.Map(debug_function)
#           | 'writeCVToBQ' >> beam.io.Write(
#                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','campaign_video'),
#                      schema='video_campaign_id:STRING, start_date:DATETIME, end_date:DATETIME, updated_at:DATETIME',
#                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                      write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#       )
# # # # #15
#     with beam.Pipeline(options = options) as p_campaign_questions:
#         (p_campaign_questions
#             |'ReadCampQuesMob' >> ReadFromMongo(connection_string, 'alomobile', 'forms',
#                                       # query={'created_at':{'$gt':table_names['campaign_questions-cr'] - timedelta(hours = 7)},
#                                       # query={'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
#                                       query={'created_at':{'$gt':start_datetime}},
#                                       fields=(['_id', 'question', 'created_at']))
#             |'strCQ'>>beam.Map (lambda x:{'id':str(x['_id']),
#                                  'question':x['question'],
#                                  'created_at':str(x['created_at']+timedelta(hours=7))[0:19]})
#             # |'DebugCQ' >> beam.Map(debug_function)
#             | 'writeCQToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','campaign_questions'),
#                       schema='id:STRING, question:STRING, created_at:DATETIME',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#
#         )
#
# # #18
#     with beam.Pipeline(options = options) as p_review_doctors:
#        (p_review_doctors
#             | 'ReadRevDoctMob' >> ReadFromMongo(connection_string, 'alomobile', 'review_doctors',
#                                       # query={'updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},  #every_night
#                                       query={'updated_at':{ '$gt':start_datetime}},  #every_3hours
#                                       fields=['_id', 'status', 'review',
#                                               'is_like', 'doctor_id',
#                                               'user_id', 'moderated_by_id', 'created_at', 'updated_at'])
#             |'alphanumericreviewsdoctors' >> beam.Map(alphanumeric)
#             |'strdoctors' >> beam.Map (lambda x:{'id':str(x['_id']), 'status':x['status'],
#                                    'review':x['review'], 'is_like':x['is_like'],
#                                    'doctor_id':str(x.get('doctor_id', "")), 'user_id':str(x['user_id']),
#                                    'moderated_by_id':str(x.get('moderated_by_id',"")), 'updated_at':str(x['updated_at']+timedelta(hours=7))[0:19],
#                                    'created_at':str(x['created_at']+timedelta(hours=7))[0:19]})
# #           | 'Debugrevdoctor' >> beam.Map(debug_function)
#             | 'writereviewsdoctorsToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','review_doctors'),
#             schema='id:STRING, status:STRING, review:STRING, is_like:BOOLEAN, doctor_id:STRING, user_id:STRING, created_at:DATETIME, updated_at:DATETIME, moderated_by_id:STRING',
#             create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
#             write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#        )
#
# # #19
#     with beam.Pipeline(options = options) as p_journals:
#         (p_journals
#             | 'ReadJournalsMob'>> ReadFromMongo(connection_string, 'alomobile', 'journals',
#                                          # query={'status':{"$ne":"pending"}, 'updated_at':{'$gt':table_names['journals-upd'] - timedelta(hours = 7)}},
#                                          # query={'status':{"$ne":"pending"}, 'updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
#                                          query={'status':{"$ne":"pending"}, 'updated_at':{'$gt':start_datetime}},
#                                          fields=['status', 'transaction_id','paid_time', 'doctor_id', 'user_id', 'is_consumed',
#                                                  'created_at', 'updated_at','payment_method_id', 'payment_gateway_id',
#                                                  'payment_provider_id', 'app_product_id', 'gross_amount'])
#             | 'strJournals' >> beam.Map (lambda x:{'id':str(x['_id']),
#                                        'status':x['status'],
#                                        'transaction_id':x['transaction_id'],
#                                        'paid_time':x.get('paid_time', None),
#                                        'is_consumed':x['is_consumed'],
#                                        'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
#                                        'updated_at':str(x['updated_at']+timedelta(hours=7))[0:19],
#                                        'user_id':str(x['user_id']),
#                                        'payment_method_id':str(x['payment_method_id']),
#                                        'payment_gateway_id':str(x['payment_gateway_id']),
#                                        'payment_provider_id':str(x['payment_provider_id']),
#                                        'app_product_id':str(x['app_product_id']),
#                                        'gross_amount':x.get('gross_amount', None),
#                                        'doctor_id':str(x['doctor_id']),
#                                        'date_id':(x['updated_at']+timedelta(hours=7)).day,
#                                        'month_id':(x['updated_at']+timedelta(hours=7)).month,
#                                        'year_id':(x['updated_at']+timedelta(hours=7)).year,
#                                        'hour_id':(x['updated_at']+timedelta(hours=7)).hour,
#                                        'minute_id':(x['updated_at']+timedelta(hours=7)).minute})
#             | 'paid_timezoneJournals' >> beam.Map(paid_timezoneJournals)
#             | 'gross_amountJournals' >> beam.Map(gross_amountJournals)
#             # | 'DebugJournals' >> beam.Map(debug_function)
#             | 'writeJournalsToBQ' >> beam.io.Write(
#                              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','journal'),
#                              schema='id:STRING, user_id:STRING, status:STRING, created_at:DATETIME, updated_at:DATETIME, date_id:INTEGER, month_id:INTEGER, year_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER, app_product_id:STRING, transaction_id:STRING, is_consumed:BOOL, paid_time:DATETIME, payment_method_id:STRING, payment_gateway_id:STRING, payment_provider_id:STRING, gross_amount:INTEGER, doctor_id:STRING',
#                              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#         )
# #
# # ## 23
#     with beam.Pipeline(options = options) as p_conclusions:
#       (p_conclusions
#           |'ReadConclMob' >> ReadFromMongo(connection_string, 'alomobile', 'question_conclusions',
#                                      # query={'updated_at':{'$gt':table_names['conclusions-upd'] - timedelta(hours = 7)},
#                                      # query={'updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
#                                      query={'updated_at':{'$gt':start_datetime}},
#                                      fields=(['status', 'created_at', 'conclusion', 'updated_at', 'question_id']))
#           |'strconclusions'>>beam.Map (lambda x:{'id':str(x['_id']),
#                                 'status':x['status'],
#                                 'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
#                                 'updated_at':str(x['updated_at']+timedelta(hours=7))[0:19],
#                                 'conclusion':x['conclusion'],
#                                 'question_id':str(x['question_id'])})
#           # |'Debugconclusions' >> beam.Map(debug_function)
#           | 'writeconclusionsToBQ' >> beam.io.Write(
#                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','conclusions'),
#                      schema='id:STRING, question_id:STRING, status:STRING, created_at:DATETIME, updated_at:DATETIME, conclusion:STRING',
#                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                      write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#       )
# # #24
    with beam.Pipeline(options = options) as p_metaquestions:
        (p_metaquestions
            | 'ReadMetaQMob' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    # query={"_type":'Core::MetaQuestion', 'created_at':{'$gt':table_names['metaquestions-cr'] - timedelta(hours = 7)},
                                    # query={"_type":'Core::MetaQuestion', 'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
                                    query={'_type':'Core::MetaQuestion', 'is_closed':True, 'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
                                    fields=['title', 'choice', 'created_at', 'max_count_recommendation'])
            |'strMetaQ'>>beam.Map (lambda x:{'id':str(x['_id']),
                                 'title':x['title'],
                                 'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
                                 'answer_by_user':x['choice'],
                                 'max_count_recommendation':x['max_count_recommendation']
                                })
            | 'maxrecMetaQ' >> beam.Map(maxrecMetaQ)
            # | 'DebugMetaQ' >> beam.Map(debug_function)
            | 'writeMetaQToBQ' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','metaquestions'),
                      schema='id:STRING, title:STRING, created_at:DATETIME, answer_by_user:BOOLEAN, max_count_recommendation:INTEGER',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )
# #
# # #25
    with beam.Pipeline(options = options) as p_doctors:
      (p_doctors
        | 'ReadDoctorsMob' >> ReadFromMongo(connection_string, 'alomobile', 'users',
                                # query={'_type':'Core::Doctor', 'created_at':{'$gt':table_names['doctors-join']}},
                                query={'_type':'Core::Doctor', 'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
                                # query={'_type':'Core::Doctor', 'created_at':{'$gt':start_datetime}},
                                fields=['_id', 'firstname', 'lastname', 'gender', 'email', 'city_id', 'birthday',
                                'status', 'str_number', 'version', 'bank_account_name', 'number_account_bank', 'name_bank',
                                'npwp', 'domicile', 'doctor_speciality_id', 'job_type', 'app_product_id', 'phone', 'created_at', 'is_specialist'])
        | 'get_city_doctor' >> beam.ParDo(GetCitiesDoctorFn())
        | 'get_specialities_doctor' >> beam.ParDo(GetSpecialitiesDoctorFn())
        | 'get_sku_doctor' >> beam.ParDo(GetSkuDoctorFn())
        | 'format_data_doctors' >> beam.Map (lambda element:{'doctor_id':str(element['_id']),
                                               'city':element['city'],
                                               'speciality':element['speciality'],
                                               'status':element['status'],
                                               'str_number':element['str_number'],
                                               'gender':element['gender'],
                                               'fullname':element['firstname'] + ' ' + element['lastname'],
                                               'bank_account_name':element.get('bank_account_name'," "),
                                               'sku':element['sku'],
                                               'phone':element['phone'],
                                               'job_type':element.get('job_type'," "),
                                               'version':element.get('version'," "),
                                               'npwp':element.get('npwp'," "),
                                               'email':element.get('email'," "),
                                               'name_bank':element.get('name_bank'," "),
                                               'number_account_bank':element.get('number_account_bank'," "),
                                               'birthdate':element['birthday'],
                                               'birthyear':element['birthyear'],
                                               'domicile':element.get('domicile'," "),
                                               'join_at':str(element['created_at'])[0:19],
                                               'is_specialist':element.get('is_specialist')})
        # | 'Debugdoctors' >> beam.Map(debug_function)
        | 'write_doctorsToBQ' >> beam.io.Write(
                            beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','doctors'),
                            schema='doctor_id:STRING,city:STRING,speciality:STRING,status:STRING,str_number:STRING,gender:STRING,fullname:STRING,bank_account_name:STRING,sku:STRING,phone:STRING,job_type:STRING,version:STRING,npwp:STRING,email:STRING,name_bank:STRING,number_account_bank:STRING,birthdate:STRING,birthyear:INTEGER,domicile:STRING,join_at:STRING,is_specialist:BOOLEAN',
                            create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                            write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )
#
# #26
#     with beam.Pipeline(options = options) as p_fact_users:
#       registration_channel = (p_fact_users
#         |'ReadRegChanMob' >> beam.io.Read(beam.io.BigQuerySource(query='SELECT id, provider FROM alowarehouse_alodoktermobile.registration_channel', use_standard_sql=True))
#         |'ExtractTextColumnRC' >> beam.Map(lambda row: (row['provider'], {'channel_id':row['id']}))
#                         )
#       user = (p_fact_users
#         |'ReadUsersMob' >> ReadFromMongo(connection_string, 'alomobile', 'users',
#                                          # query={'created_at':{'$gt':table_names['fact_users-cr'] - timedelta(hours = 7), '_type':'Core::User'},
#                                          # query={'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}, '_type':'Core::User'},
#                                          query={'created_at':{'$gt':start_datetime}, '_type':'Core::User'},
#                                          fields=['_id', 'provider', 'created_at'])
#         | beam.Map(lambda x:(x['provider'], {'user_id':x['_id'], 'created_at':x['created_at']}))
#             )
#       result = ({'user_id':user, 'channel_id':registration_channel} | 'result:UC' >> beam.CoGroupByKey()
#                 |'joinUC'>> beam.FlatMap(join_infoUC)
#                 |'merge_dictUC' >> beam.Map(merge_dictUC)
#                 |'formatdataFU' >> beam.Map (lambda x:{'id':str(uuid.uuid4()),
#                                                     'user_id':str(x['user_id']),
#                                                     'channel_id':x['channel_id'],
#                                                     'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
#                                                     'year_id':x['created_at'].year,
#                                                     'month_id':x['created_at'].month,
#                                                     'date_id':x['created_at'].day,
#                                                     'hour_id':x['created_at'].hour,
#                                                     'minute_id':x['created_at'].minute})
# #               |'Debug_fact_users' >> beam.Map(debug_function)
#                 | 'writefactusersToBQ' >> beam.io.Write(
#                               beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_users'),
#                               schema='id:STRING, user_id:STRING, channel_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                               write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#          )
# # #27
#     with beam.Pipeline(options = options) as p_fact_magazine:
#       (p_fact_magazine
#         |'ReadFMagMob' >> ReadFromMongo(connection_string, 'alomobile', 'magazines',
#                                                             # query={'created_at':{'$gt':table_names['fact_magazine-cr']}},
#                                                             # query={'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
#                                                             query={'created_at':{'$gt':start_datetime}},
#                                                             fields=['post_id', 'created_at', 'magazine_relationships.term_taxonomy_id'])
#         | 'Denormalize' >> beam.ParDo(DenormalizeFMFn())
#         | 'GetTerm' >> beam.ParDo(GetTermMagFn())
#         | 'FormatData' >> beam.Map(lambda x:{'id':x['id'],
#                                            'article_id':str(x['article_id']),
#                                            'term_id': str(x['term_id']),
#                                            'created_at':str(x['created_at']),
#                                            'day_id': x['created_at'].isoweekday(),
#                                            'date_id':x['created_at'].day,
#                                            'month_id':x['created_at'].month,
#                                            'year_id':x['created_at'].year,
#                                            'hour_id':x['created_at'].hour,
#                                            'minute_id':x['created_at'].minute,
#                                            'quarter_id':int(math.ceil(x['created_at'].month/3.))})
#         # | 'debug' >> beam.ParDo (debug_function)
#         |'WriteFactMagazineToBQ' >> beam.io.Write(
#                  beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_magazine'),
#                  schema='id:STRING, term_id:STRING, article_id:STRING, created_at:DATETIME, day_id:INTEGER, date_id:INTEGER, month_id:INTEGER, quarter_id:INTEGER, year_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                  create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                  write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#     )
# #
# # # #28
#     with beam.Pipeline(options = options) as p_fact_video_campaign:
#         (p_fact_video_campaign
#             |'ReadFVidCampMob' >> ReadFromMongo(connection_string, 'alomobile', 'view_video_campaigns',
#                                       # query={'created_at':{'$gt':table_names['fact_video_campaign-cr'] - timedelta(hours = 7)}},
#                                       # query={'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
#                                       query={'created_at':{'$gt':start_datetime}},
#                                       fields=(['user_id', 'tags', 'video_ad_campaign_id', 'created_at']))
#             |'Unlistfvc' >> beam.ParDo(UnlistfvcFn())
#             |'GetTagfvc' >> beam.ParDo(GetTagfvcFn())
#             # |'Debugfvc' >> beam.Map(debug_function)
#             | 'writeTfvcoBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_video_campaign'),
#                       schema='id:STRING, user_id:STRING, tag_id:STRING, campaign_id:STRING, ads_unit_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#         )
# #
# #
# # ###############################################################################################################################################################
# #
# # # ##31
#     with beam.Pipeline(options = options) as p_fact_campaign:
#        (p_fact_campaign
#             |'ReadFCampMob'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          # query={'created_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
#                                          query={'created_at':{'$gt':start_datetime}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#             |'GetTagFC' >> beam.ParDo(GetTagFCFn())
#             |'GetQnAFC' >> beam.ParDo(GetQnAFC())
#             # |'DebugFC' >> beam.Map(debug_function)
#             | 'writeFCToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaigns'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#        )
# #
# # ##################################################################################################################################################################
# #
# #
# # #33
    with beam.Pipeline(options = options) as p_questions:
        (p_questions
            | 'ReadQuestionsMob' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                # query={'updated_at':{'$gt':table_names['questions-upd'] - timedelta(hours = 7)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
                query={'updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
                # query={'updated_at':{'$gt':start_datetime}, 'is_closed':True, "$or": [{'_type':'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
                fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
                'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
            | 'get_price' >> beam.ParDo(GetPriceQFn())
            | 'is_chatbot' >> beam.Map(is_chatbotQ)
            | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
            # | 'DebugQ' >> beam.Map(debug_function)
            | 'writeQToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions'),
                     schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )
# #
# # #34
    with beam.Pipeline(options = options) as p_users:
        (p_users
             |'ReadUsersMob'>> ReadFromMongo(connection_string, 'alomobile', 'users',
                                              #query={"_type" : "Core::User",'updated_at':{'$gt':table_names['users-upd'] - timedelta(hours = 7)}},
                                              query={'_type' : 'Core::User','updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}},
                                              fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
                                              'city_id', 'provider', 'version', 'updated_at', 'phone'])
             |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
             # |'Debug' >> beam.Map(debug_function)
             | 'writeUserToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users'),
                        schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING, phone:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )
# #
# # # #35
    with beam.Pipeline(options = options) as p_factquestions:
        (p_factquestions
           | 'ReadFQMob' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                # query={'is_closed':True, 'updated_at':{'$gt':table_names['factquestions-cr'] - timedelta(hours = 7)}} ,
                                query={'is_closed':True, 'updated_at':{'$gt':datetime.today()-timedelta(days = 1)-timedelta(hours=7)}} ,
                                # query={'is_closed':True, 'updated_at':{'$gt':start_datetime}},
                                fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic',
                                'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
           | 'type' >> beam.ParDo(if_meta_question())
           | 'get_terms' >> beam.ParDo(GetTermsFn())
           # | 'get_conclusionsQ5' >> beam.ParDo(GetConclusionFn())
           | 'get_paymethod' >> beam.ParDo(GetPaymentFn())
           # | 'DebugQ5' >> beam.Map(debug_function)
           | 'writeFQToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_questions'),
                        schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:DATETIME, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_doctor_answer_time:DATETIME, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
           )


#
# # if __name__ == '__main__':
# #     run()
