
# coding: utf-8

# In[ ]:
import apache_beam as beam
from pymongo import MongoClient
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from bson.objectid import ObjectId

PROJECT='plenary-justice-151004'
BUCKET='staging-plenary-justice-151004'

connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109/alomobile"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

#dimensi_doctors
class GetCitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        #add birthyear element
        birthyear = element.get('birthday', None)[-4:]
        if (birthyear):
            element['birthyear'] = int(birthyear)
        else: element['birthyear'] = None

        #add city name element
        if (element['city_id']):
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])})
            for city in city_query:
                element['city'] = city['name']
                data.append(element)
        else:
            element['city'] = None
            data.append(element)
        return data

class GetSpecialitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element['doctor_speciality_id'] != '':
            speciality_query = db.doctor_specialities.find({'_id':ObjectId(element['doctor_speciality_id'])})
            for speciality in speciality_query:
                element['speciality']=speciality['name']
        else:
            element['speciality']=None
        data.append(element)
        return data

class GetSkuDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element.get('app_product_id'):
            sku_query = db.app_products.find({'_id':ObjectId(element['app_product_id'])})
            for sku in sku_query:
                element['sku']=sku['sku']
                data.append(element)
        else:
            element['sku']=None
            data.append(element)
        return data

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
#     pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])

# def run():
#     options = PipelineOptions()
#     google_cloud_options = options.view_as(GoogleCloudOptions)
#     google_cloud_options.project = 'plenary-justice-151004'
#     google_cloud_options.job_name = 'sourcemongo'
#     google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
#     google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
#     google_cloud_options.region = 'asia-east1'
#     options.view_as(StandardOptions).runner = 'DirectRunner'
#     options.view_as(SetupOptions).save_main_session = True
#
#     pipeline = beam.Pipeline(options=options)
    with beam.Pipeline(options = options) as pipeline:
        (pipeline
             | 'read_doctors' >> ReadFromMongo(connection_string, 'alomobile', 'users', query={'_type':'Core::Doctor'},
                                fields=['_id', 'firstname', 'lastname', 'gender', 'email', 'city_id', 'birthday', 'status', 'str_number', 'version', 'bank_account_name', 'number_account_bank', 'name_bank', 'npwp', 'domicile', 'doctor_speciality_id', 'job_type', 'app_product_id', 'phone', 'created_at', 'updated_at'])
             | 'get_city_doctor' >> beam.ParDo(GetCitiesDoctorFn())
             | 'get_specialities_doctor' >> beam.ParDo(GetSpecialitiesDoctorFn())
             | 'get_sku_doctor' >> beam.ParDo(GetSkuDoctorFn())
             | 'format_data' >> beam.Map (lambda element:{'doctor_id':str(element['_id']),
                                                    'city':element['city'],
                                                    'speciality':element['speciality'],
                                                    'status':element['status'],
                                                    'str_number':element['str_number'],
                                                    'gender':element['gender'],
                                                    'fullname':element['firstname'] + ' ' + element['lastname'],
                                                    'bank_account_name':element.get('bank_account_name'," "),
                                                    'sku':element['sku'],
                                                    'phone':element['phone'],
                                                    'job_type':element.get('job_type'," "),
                                                    'version':element.get('version'," "),
                                                    'npwp':element.get('npwp'," "),
                                                    'email':element.get('email'," "),
                                                    'name_bank':element.get('name_bank'," "),
                                                    'number_account_bank':element.get('number_account_bank'," "),
                                                    'birthdate':element['birthday'],
                                                    'birthyear':element['birthyear'],
                                                    'domicile':element.get('domicile'," "),
                                                    'join_at':str(element['created_at'])[0:19],
                                                    'updated_at':str(element['updated_at'])[0:19]
                                                    })
    #          | 'Debug' >> beam.Map(debug_function)
             | 'writeToBQ' >> beam.io.Write(
                                 beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','doctor'),
                                 schema='updated_at:DATETIME,doctor_id:STRING,city:STRING,speciality:STRING,status:STRING,str_number:STRING,gender:STRING,fullname:STRING,bank_account_name:STRING,sku:STRING,phone:STRING,job_type:STRING,version:STRING,npwp:STRING,email:STRING,name_bank:STRING,number_account_bank:STRING,birthdate:STRING,birthyear:INTEGER,domicile:STRING,join_at:STRING',
                                 create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                                 write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
            )
#     pipeline.run().wait_until_finish()
#
# if __name__ == '__main__':
#     run()
