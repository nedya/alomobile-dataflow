import apache_beam as beam
import datetime
import itertools
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math
import re


def debug_function(pcollection_as_list):
    print (pcollection_as_list)

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.109:27017/alomobile"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@192.168.1.133:27017/alomobile"
connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"

tdelta = datetime.timedelta(hours=7)
scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)
#
# #fact questions
# class if_meta_question(beam.DoFn):
#     def process(self, element):
#         data = []
#         if element.get('time_picked_up') is not None:
#             if element['_type'] == "Core::Question" or element['_type'] == "Core::PreQuestion":
#                 data.append({'topic':element['topic'],
#                                   'question_id':str(element['_id']),
#                                   'meta_question_id':str(element.get('meta_question_id',"")),
#                                   'user_id':str(element['user_id']),
#                                   'picked_up_by_id':str(element.get('picked_up_by_id',"")),
#                                   'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
#                                   'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
#                                   'intent_id':str(element.get('intent_id',"")),
#                                   'sub_intent_id':str(element.get('sub_intent_id',"")),
#                                   'time_picked_up':str(element['time_picked_up']+timedelta(hours=7))[0:19],
#                                   'journal_id':str(element.get('journal_id')),
#                                   'date_picked_up':str(element['time_picked_up'])[0:10],
#                                   'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
#                                   'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
#                                   'is_has_conclusion':element.get('is_has_conclusion', None),
#                                   'no_conclusion':element.get('no_conclusion', None)})
#
#             elif element['_type'] == "Core::MetaQuestion":
#                 data.append({'topic':element['topic'],
#                                   'question_id':None,
#                                   'meta_question_id':str(element['_id']),
#                                   'user_id':str(element['user_id']),
#                                   'picked_up_by_id':str(element.get('picked_up_by_id',"")),
#                                   'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
#                                   'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
#                                   'intent_id':str(element.get('intent_id',"")),
#                                   'sub_intent_id':str(element.get('sub_intent_id',"")),
#                                   'time_picked_up':str(element['time_picked_up']+timedelta(hours=7))[0:19],
#                                   'journal_id':str(element.get('journal_id')),
#                                   'date_picked_up':str(element['time_picked_up'])[0:10],
#                                   'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
#                                   'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
#                                   'is_has_conclusion':element.get('is_has_conclusion', None),
#                                   'no_conclusion':element.get('no_conclusion', None)})
#         return data
#
# class GetTermsFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         terms = []
#         term_query = db.terms.find({'name':element['topic']})
#         if element.get('time_picked_up') is not None:
#             for term in term_query:
#                 if element.get('journal_id', None) is not None:
#                     terms.append({'topic_id':str(term['_id']),
#                                   'question_id':element['question_id'],
#                                   'meta_question_id':element['meta_question_id'],
#                                   'user_id':element['user_id'],
#                                   'picked_up_by_id':element['picked_up_by_id'],
#                                   'created_at':element['created_at'],
#                                   'updated_at':element['updated_at'],
#                                   'intent_id':element['intent_id'],
#                                   'sub_intent_id':element['sub_intent_id'],
#                                   'time_picked_up':element['time_picked_up'],
#                                   'journal_id':element['journal_id'],
#                                   'date_picked_up':element['date_picked_up'],
#                                   'first_user_answer_time':element['first_user_answer_time'],
#                                   'first_doctor_answer_time':element['first_doctor_answer_time'],
#                                   'is_has_conclusion':element['is_has_conclusion'],
#                                   'no_conclusion':element['no_conclusion']})
#                 elif element.get('journal_id', None) is None:
#                     terms.append({'topic_id':str(term['_id']),
#                                   'question_id':element['question_id'],
#                                   'meta_question_id':element['meta_question_id'],
#                                   'user_id':element['user_id'],
#                                   'picked_up_by_id':element['picked_up_by_id'],
#                                   'created_at':element['created_at'],
#                                   'updated_at':element['updated_at'],
#                                   'intent_id':element['intent_id'],
#                                   'sub_intent_id':element['sub_intent_id'],
#                                   'time_picked_up':element['time_picked_up'],
#                                   'journal_id':None,
#                                   'date_picked_up':element['date_picked_up'],
#                                   'first_doctor_answer_time':element['first_doctor_answer_time'],
#                                   'is_has_conclusion':element['is_has_conclusion'],
#                                   'no_conclusion':element['no_conclusion']})
#         return terms
#
# class GetConclusionFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         conclusions = []
#         if element.get('is_has_conclusion') is True:
#             conclusion_query = db.question_conclusions.find({'question_id':ObjectId(element['question_id'])})
#             for conclusion in conclusion_query:
#                 conclusion_id = str(conclusion['_id'])
#         else: conclusion_id = None
#         conclusions.append({'conclusion_id':conclusion_id,
#                         'topic_id':element['topic_id'],
#                         'question_id':element['question_id'],
#                         'meta_question_id':element['meta_question_id'],
#                         'user_id':element['user_id'],
#                         'picked_up_by_id':element['picked_up_by_id'],
#                         'created_at':element['created_at'],
#                         'updated_at':element['updated_at'],
#                         'intent_id':element['intent_id'],
#                         'sub_intent_id':element['sub_intent_id'],
#                         'time_picked_up':element['time_picked_up'],
#                         'journal_id':element['journal_id'],
#                         'date_picked_up':element['date_picked_up'],
#                         'first_user_answer_time':element['first_user_answer_time'],
#                         'first_doctor_answer_time':element['first_doctor_answer_time'],
#                         'is_has_conclusion':element['is_has_conclusion'],
#                         'no_conclusion':element['no_conclusion']})
#
#         return conclusions
#
# class GetPaymentFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         journals = []
#         if element['journal_id'] != 'None' :
#             journal_query = db.journals.find({'_id':ObjectId(element['journal_id'])})
#             for journal in journal_query:
#                 journal_id = element['journal_id']
#                 payment_method_id = str(journal['payment_method_id'])
#         else:
#             journal_id = None
#             payment_method_id = None
#         journals.append({'id':str(uuid.uuid4()),
#                          'conclusion_id':element['conclusion_id'],
#                          'topic_id':element['topic_id'],
#                          'question_id':element['question_id'],
#                          'meta_question_id':element['meta_question_id'],
#                          'user_id':element['user_id'],
#                          'picked_up_by_id':element['picked_up_by_id'],
#                          'created_at':element['created_at'],
#                          'updated_at':element['updated_at'],
#                          'intent_id':element['intent_id'],
#                          'sub_intent_id':element['sub_intent_id'],
#                          'time_picked_up':element['time_picked_up'],
#                          'journal_id':journal_id,
#                          'payment_method_id':payment_method_id,
#                          'date_picked_up':element['date_picked_up'],
#                          'first_user_answer_time':element['first_user_answer_time'],
#                          'first_doctor_answer_time':element['first_doctor_answer_time'],
#                          'is_has_conclusion':element['is_has_conclusion'],
#                          'no_conclusion':element['no_conclusion']})
#         return journals
#
# def run():
#     gcs_path = "gs://staging-plenary-justice-151004"
#     dataflow_options = [
#         "--project", "plenary-justice-151004",
#         "--staging_location", ("%s/staging/" %gcs_path),
#         "--temp_location", ("%s/temp" % gcs_path),
#         "--region", "asia-east1",
#         "--setup_file", "./setup.py"
#     ]
#     options = PipelineOptions(dataflow_options)
#     gcloud_options = options.view_as(GoogleCloudOptions)
#     options.view_as(StandardOptions).runner = 'dataflow'
#
#     with beam.Pipeline(options = options) as pipeline_fact_questions1:
#         (pipeline_fact_questions1
#            | 'read_questionsQ1' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 12, 15),'$lte':datetime(2017, 12, 31)}},
#                                 fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
#            | 'format' >> beam.Map(lambda x:{'id':str(x['_id']),
#                                    'is_has_conclusion':x.get('is_has_conclusion',None),
#                                    'no_conclusion':x.get('no_conclusion',None),
#                                    'first_user_answer_time':str(x.get('first_user_answer_time', None)),
#                                    'first_doctor_answer_time':str(x.get('first_doctor_answer_time', None)),
#                                    'meta_question_id':x.get('meta_question_id', None),
#                                    'user_id':str(x['user_id']),
#                                    'topic':x['topic'],
#                                    'picked_up_by_id':str(x.get('picked_up_by_id',None)),
#                                    'created_at':str(x['created_at']+tdelta)[0:19]}),
#                                    'updated_at':str(x['updated_at']+tdelta)[0:19]}),
#                                    'intent_id':str(x.get('intent_id',None)),
#                                    'sub_intent_id':str(x.get('sub_intent_id')),
#                                    'time_picked_up':str(x['time_picked_up']+tdelta)[0:19],
#                                    'journal_id':str(x.get('journal_id',None)),
#                                    'pick_hour':x.get('pick_hour',None),
#                                    '_type':str(x['_type'])
#                                    })
#            # | 'typeQ1' >> beam.ParDo(if_meta_question())
#            # | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
#            # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
#            # | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
#            # | 'DebugQ1' >> beam.Map(debug_function)
#            | 'writeFQ1ToBQ' >> beam.io.Write(
#                         beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','samplefactquestions'),
#                         schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_doctor_answer_time:STRING, updated_at:DATETIME, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, picked_up_by_id:STRING, journal_id:STRING',
#                         create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                         write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#            # | 'writeFQ1ToBQ' >> beam.io.Write(
#            #              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','samplefactquestions'),
#            #              schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_doctor_answer_time:STRING, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, conclusion_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, pick_hour:STRING, picked_up_by_id:STRING, journal_id:STRING',
#            #              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#            #              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#            )
