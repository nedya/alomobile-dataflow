
# coding: utf-8

# In[1]:


import apache_beam as beam
import datetime
from pymongo import MongoClient
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from bson.objectid import ObjectId


PROJECT='plenary-justice-151004'
BUCKET='staging-plenary-justice-151004'


def run():
   options = PipelineOptions()
   google_cloud_options = options.view_as(GoogleCloudOptions)
   google_cloud_options.project = 'plenary-justice-151004'
   google_cloud_options.job_name = 'sourcemongo'
   google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
   google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
   google_cloud_options.region = 'asia-east1'
   options.view_as(StandardOptions).runner = 'DirectRunner'
   options.view_as(SetupOptions).save_main_session = False
   requirements_file = "/home/grumpycat/flowarehouse/dataflow_pipeline/requirements.txt"
   options.view_as(SetupOptions).requirements_file = requirements_file

   p = beam.Pipeline(options=options)
   connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"
#   connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.225.73/alomobile"

#    connection_string = "mongodb://grumpycat:alo.1975.dokter@112.78.168.133/alomobile"
   start = datetime.datetime(2019,1,16,13,56,29)  
   #end = datetime.datetime(2018, 7, 15)
   tdelta = datetime.timedelta(hours=7)

   def debug_function(pcollection_as_list):
    print(pcollection_as_list)

   class GetMetaFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        metaslots = []
        metaslot_query = db.meta_slots.find({'_id':ObjectId(element['meta_slot_id'])})
        for metaslot in metaslot_query:
            metaslots.append({'doctor_id':str(element['doctor_id']),
                               'time_slot_management_id':metaslot['time_slot_management_id'],
                               'slot_id':str(element['_id']),
                               'date_slot':str(element['date_slot'])[0:10],
                               'created_at':str(element['created_at'])[0:19]})
            return metaslots

   class GetTimeFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        times = []
        time_query = db.time_slot_managements.find({'_id':ObjectId(element['time_slot_management_id'])})
        for time in time_query:
            times.append({'doctor_id':str(element['doctor_id']),
                          'start_time':time['start_time'],
                          'end_time':time['end_time'],
                          'doctor_type':time['doctor_type'],
                          'slot_id':element['slot_id'],
                          'date_slot':element['date_slot'],
                          'created_at':element['created_at']})
        return times


   (p
      | 'read_slot' >> ReadFromMongo(connection_string, 'alomobile', 'single_meta_slots', query={'is_deleted':False, 'date_slot':{'$gte':start}}, fields=['_id', 'date_slot', 'doctor_id', 'meta_slot_id', 'created_at'])
      | 'get_meta' >> beam.ParDo(GetMetaFn())
      | 'get_time' >> beam.ParDo(GetTimeFn())
#       | 'Debug' >> beam.Map(debug_function)
      | 'writeToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','single_meta_slots'),
                     schema= 'slot_id:STRING, doctor_id:STRING, date_slot:DATE, start_time:STRING,  end_time:STRING, created_at:DATETIME, doctor_type:STRING',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))

   )

   p.run().wait_until_finish()

if __name__ == '__main__':
   run()
