
# coding: utf-8

# In[ ]:


import apache_beam as beam
import datetime
from pymongo import MongoClient
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from bson.objectid import ObjectId


PROJECT='plenary-justice-151004'
BUCKET='staging-plenary-justice-151004'


def run():
   options = PipelineOptions()
   google_cloud_options = options.view_as(GoogleCloudOptions)
   google_cloud_options.project = 'plenary-justice-151004'
   google_cloud_options.job_name = 'sourcemongo'
   google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
   google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
   google_cloud_options.region = 'asia-east1'
   options.view_as(StandardOptions).runner = 'DirectRunner'
   options.view_as(SetupOptions).save_main_session = False
   requirements_file = "/home/grumpycat/flowarehouse/dataflow_pipeline/requirements.txt"
   options.view_as(SetupOptions).requirements_file = requirements_file

   p = beam.Pipeline(options=options)
   connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"
#    connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.200/alomobile"


   start = datetime.datetime(2018, 10, 1)
   end = datetime.datetime(2018, 10, 31)
   tdelta = datetime.timedelta(hours=7)

   def debug_function(pcollection_as_list):
    print(pcollection_as_list)

   schema = 'id:STRING, topic:STRING, score:FLOAT64, choice:BOOLEAN, created_at:DATETIME'

   que = (p
            | 'Readquestions' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={"created_at":{"$gte":datetime.datetime(2019,1,23,23,25,58)-tdelta}, 'top_sub_intent_recommendation':{"$ne":{}}, '_type':'Core::MetaQuestion'}, fields=['choice', 'top_sub_intent_recommendation.score', 'top_sub_intent_recommendation.topic', 'created_at'])
            | 'format' >> beam.Map(lambda x:{'id':str(x['_id']), 'topic':x['top_sub_intent_recommendation']['topic'], 'score':x['top_sub_intent_recommendation']['score'], 'choice':x['choice'], 'created_at':str(x['created_at']+tdelta)[0:19]})
#             | 'DebugQ' >> beam.Map(debug_function)
            | 'writeQToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','automatic_subintent_date'),
                     schema = schema,
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
          )

   p.run().wait_until_finish()

if __name__ == '__main__':
   run()
