
# coding: utf-8

# In[1]:

import apache_beam as beam
import re
import uuid, math
import itertools
from datetime import datetime, timedelta
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from google.oauth2.service_account import Credentials
from bson.objectid import ObjectId
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from google.cloud import bigquery
def debug_function(pcollection_as_list):
    print (pcollection_as_list)

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"

scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)

# table_names = {'fact_users-cr':None, 'fact_magazine-cr':None, 'fact_video_campaign-cr':None, 'fact_campaign-cr':None, 'factquestions-cr':None}
# from google.cloud import bigquery
# client = bigquery.Client()
# dataset_ref = client.dataset('alobrain_data_staging')
# for key,val in table_names.items():
#     data = key.split("-")
#     #field default : updated_at
#     field = 'updated_at'
#     if (data[1] == 'cr'):
#         field = 'created_at'
#     elif (data[1] == 'join'):
#         field = 'join_at'
#     sql = "SELECT MAX ("+ field +") FROM [plenary-justice-151004.alowarehouse_alodoktermobile."+data[0]+"]"
#     query = client.run_sync_query(sql)
#     query.run()
#     table_names[key] = query.rows[0][0]


#dimensi fact campaigns
class GetTagFCFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        campaign_query = db.campaigns.find({'_id':element['campaign_id']},{'_id':1,'tags':1})
        for campaign in campaign_query:
            if (campaign.get('tags')): #if campaign tags is exsist
                for campaign_tag in campaign['tags']:
                    term_query = db.terms.find({'name':campaign_tag},{'_id':1})
                    for tag_id in term_query:
                        data.append({'id':element['_id'],
                                    'form_instance_values': element['form_instance_values'],
                                    'tag_id':tag_id['_id'],
                                    'client_id':element['client_id'],
                                    'campaign_id':element['campaign_id'],
                                    'user_id':element['user_id'],
                                    'created_at':element['created_at']+timedelta(hours=7)}
                                   )
            else:
                data.append ({'id':element['_id'],
                                'form_instance_values': element['form_instance_values'],
                                'tag_id':None,
                                'client_id':element['client_id'],
                                'campaign_id':element['campaign_id'],
                                'user_id':element['user_id'],
                                'created_at':element['created_at']+timedelta(hours=7)}
                              )
        return data

class GetQnAFC(beam.DoFn):
    def process(self, element):
        data = []
        for question in element['form_instance_values']:
            if (question.get('form_value_ids')): #jika pertanyaan memiliki jawaban
                for answer in question['form_value_ids']:
                    if (answer == '5770ef13150cd4744f00000b'):
                        answer = questions['text_value']
                    data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': str(answer),
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
            else: data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': None,
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
        return data

#dimensi user interest
class GetUserInterestFn(beam.DoFn):
    def process(self,element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        if element.get('interest_ids'):
            for interest_id in element ['interest_ids']:
                data.append({'user_id':str(element['_id']),
                            'interest_id':str(interest_id),
                            'is_deleted':False})
        else: data.append({'user_id':str(element['_id']),
                           'interest_id':None,
                           'is_deleted':False})
        return data




def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]

    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'

#    pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])


# #30
    # with beam.Pipeline(options = options) as p_user_interests:
    #     (p_user_interests
    #         |'ReadUserInterestMob' >> ReadFromMongo(connection_string, 'alomobile', 'users',
    #                                     query={"_type":"Core::User"}, fields=['_id', 'interest_ids'])
    #         |'GetUserInterest' >> beam.ParDo(GetUserInterestFn())
    #         # |'DebugUserInterest' >> beam.Map(debug_function)
    #         | 'writeUserInterestToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','user_interests'),
    #                   schema='user_id:STRING, interest_id:STRING, is_deleted:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )
# ##31
    with beam.Pipeline(options = options) as p_fact_campaign:
       (p_fact_campaign
            |'ReadFCampMob'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
                                         query={'created_at':{'$gt':datetime.now() - timedelta(hours = 2)}},
                                         fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
            |'GetTagFC' >> beam.ParDo(GetTagFCFn())
            |'GetQnAFC' >> beam.ParDo(GetQnAFC())
            # |'DebugFC' >> beam.Map(debug_function)
            | 'writeFCToBQ' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaigns'),
                      schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
       )


# if __name__ == '__main__':
#     run()
