import apache_beam as beam
import datetime, itertools
import itertools
from bson.objectid import ObjectId
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math
import re

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.200:27017/alomobile"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.198.223.103:27017/alomobile"
#connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.225.73/alomobile"
tdelta = datetime.timedelta(hours=7)

##batching_dim_questions
def join_listsQ((k, v)):
    return itertools.product(v['number_of_chatbox'], v['questions'])

def mergedictsQ(questions_dict_chatbox):
    (chatbox, questions_dict) = questions_dict_chatbox
    a_dict = questions_dict
    a_dict.update(chatbox)
    return a_dict

class GetPriceQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        prices = []
        if element.get('journal_id', None) is not None:
                price_query = db.journals.find({'_id':element['journal_id']})
                for price in price_query:
                    if price.get('gross_amount', None) is not None:
                        prices.append({'id':str(element['_id']),
                                        'updated_at':str(element['updated_at']+tdelta)[0:19],
                                        'price':int(price.get('gross_amount', None)),
                                        'pickup_fee':4000, 'like_fee':1000,
                                        'title':element['title'],
                                        'content':element['content'],
                                        'is_said_thanks':element['is_said_thanks'],
                                        'is_paid':element.get('is_paid'),
                                        'is_closed':element['is_closed'],
                                        'type':element['_type'],
                                        'sub_intent_id':element.get('sub_intent_id', None),
                                        'intent_id':element.get('intent_id', None),
                                        'is_has_conclusion':element.get('is_has_conclusion', None),
                                        'is_shown':element.get('is_shown', None)})
                    elif price.get('gross_amount', None) is None:
                        prices.append({'id':str(element['_id']),
                                       'updated_at':str(element['updated_at']+tdelta)[0:19],
                                       'price':None, 'pickup_fee':4000,
                                       'like_fee':1000,
                                       'title':element['title'],
                                       'content':element['content'],
                                       'is_said_thanks':element['is_said_thanks'],
                                       'is_paid':None,
                                       'is_closed':element['is_closed'],
                                       'type':element['_type'],
                                       'sub_intent_id':element.get('sub_intent_id', None),
                                       'intent_id':element.get('intent_id', None),
                                       'is_has_conclusion':element.get('is_has_conclusion', None),
                                       'is_shown':element.get('is_shown', None)})
        elif element.get('journal_id', None) is None:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+tdelta)[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        else:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+tdelta)[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        return prices


def is_chatbotQ(q):
       if q['intent_id'] or q['sub_intent_id'] is not None:
           is_chatbot = {'is_chatbot':True}
       elif q['intent_id'] and q['sub_intent_id'] is None:
           is_chatbot = {'is_chatbot':False}
       else:
           is_chatbot = {'is_chatbot':None}

       if q['type'] == "Core::PreQuestion":
           autochat_close = {'autochat_close':True}
       elif q['type'] == "Core::Question":
           autochat_close = {'autochat_close':False}
       else:
           autochat_close = {'autochat_close':None}

       q.update(is_chatbot)
       q.update(autochat_close)
       q.pop('intent_id')
       q.pop('sub_intent_id')
       return q

class GetChatBoxQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        chatboxes = []
        number_of_chatbox = db.answers.find({'question_id':ObjectId(element['id'])}, no_cursor_timeout=True).count()
        chatboxes.append({'id':str(element['id']),
                          'updated_at':element['updated_at'],
                          'number_of_chatbox':number_of_chatbox,
                          'price':element['price'],
                          'pickup_fee':element['pickup_fee'],
                          'like_fee':element['like_fee'],
                          'title':element['title'],
                          'content':element['content'],
                          'is_said_thanks':element['is_said_thanks'],
                          'is_paid':element['is_paid'],
                          'is_closed':element['is_closed'],
                          'type':element['type'],
                          'is_has_conclusion':element.get('is_has_conclusion', None),
                          'is_shown':element.get('is_shown', None)})
        return chatboxes

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])

# with beam.Pipeline(options=options) as pipeline_question6:
    # with beam.Pipeline(options = options) as pipeline_question6:
    #     (pipeline_question6
    #     | 'Readquestions6' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #         query={'created_at':{'$gte':datetime.datetime(2018,7,1)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #         fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #         'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #     | 'get_price' >> beam.ParDo(GetPriceQFn())
    #     | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #     | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #     # | 'DebugQ' >> beam.Map(debug_function)
    #     | 'writeQToBQ' >> beam.io.Write(
    #              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions6'),
    #              schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )
# with beam.Pipeline(runner='DirectRunner') as pipeline_question5:
    # with beam.Pipeline(options = options) as pipeline_question5:
    #     (pipeline_question5
    #     | 'Readquestions5b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #         query={'created_at':{'$gte':datetime.datetime(2018,4,1), '$lte':datetime.datetime(2018, 6, 30)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #         fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #         'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #     | 'get_price' >> beam.ParDo(GetPriceQFn())
    #     | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #     | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #     # | 'DebugQ' >> beam.Map(debug_function)
    #     | 'writeQToBQ' >> beam.io.Write(
    #              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions5b'),
    #              schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )

    # with beam.Pipeline(options = options) as pipeline_question5:
    #     (pipeline_question5
    #     | 'Readquestions5a3' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #         query={'created_at':{'$gte':datetime.datetime(2018,3,1), '$lte':datetime.datetime(2018, 3, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #         fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #         'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #     | 'get_price' >> beam.ParDo(GetPriceQFn())
    #     | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #     | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #     # | 'DebugQ' >> beam.Map(debug_function)
    #     | 'writeQToBQ' >> beam.io.Write(
    #              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions5a'),
    #              schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #     )

    # with beam.Pipeline(options = options) as pipeline_question5:
    #     (pipeline_question5
    #     | 'Readquestions5a2' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #         query={'created_at':{'$gte':datetime.datetime(2018,2,1), '$lte':datetime.datetime(2018, 2, 28)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #         fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #         'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #     | 'get_price' >> beam.ParDo(GetPriceQFn())
    #     | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #     | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #     # | 'DebugQ' >> beam.Map(debug_function)
    #     | 'writeQToBQ' >> beam.io.Write(
    #              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions5a'),
    #              schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #     )

    # with beam.Pipeline(options = options) as pipeline_question5:
    #     (pipeline_question5
    #     | 'Readquestions5a1' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #         query={'created_at':{'$gte':datetime.datetime(2018,1,1), '$lte':datetime.datetime(2018, 1, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #         fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #         'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #     | 'get_price' >> beam.ParDo(GetPriceQFn())
    #     | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #     | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #     # | 'DebugQ' >> beam.Map(debug_function)
    #     | 'writeQToBQ' >> beam.io.Write(
    #              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions5a'),
    #              schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #     )

#bulan 12-2017
# with beam.Pipeline(runner='DirectRunner') as pipeline_question4:
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_12_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,12,1), '$lte':datetime.datetime(2017, 12, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #      )

#bulan 11-2017 append to table questions4
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_11_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,11,1), '$lte':datetime.datetime(2017, 11, 30)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )

# #bulan 10-2017 append to table questions4
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_10B2_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,10,22), '$lte':datetime.datetime(2017, 10, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_10B1_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,10,16), '$lte':datetime.datetime(2017, 10, 21)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )

# #bulan 10-2017 append to table questions4
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_10A_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,10,1), '$lte':datetime.datetime(2017, 10, 15)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )

#
# #bulan 9-2017 append to table questions4
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_9c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,9,21), '$lte':datetime.datetime(2017, 9, 30)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )

    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_9b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,9,11), '$lte':datetime.datetime(2017, 9, 20)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_9a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,9,1), '$lte':datetime.datetime(2017, 9, 10)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
# #bulan 8-2017 append to table questions4
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_8c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,8,21), '$lte':datetime.datetime(2017, 8, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_8b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,8,11), '$lte':datetime.datetime(2017, 8, 20)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_8a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,8,1), '$lte':datetime.datetime(2017, 8, 10)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )

#bulan 7-2017 append to table questions4
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_7c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,7,21), '$lte':datetime.datetime(2017, 7, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_7b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,7,11), '$lte':datetime.datetime(2017, 7, 20)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question4:
    #      (pipeline_question4
    #          | 'Readquestions4_7a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,7,1), '$lte':datetime.datetime(2017, 7, 10)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions4'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
# with beam.Pipeline(runner='DirectRunner') as pipeline_question3:
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_6c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,6,21), '$lte':datetime.datetime(2017, 6, 30)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_6b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,6,11), '$lte':datetime.datetime(2017, 6, 20)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_6a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,6,1), '$lte':datetime.datetime(2017, 6, 10)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_5c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                 query={'created_at':{'$gte':datetime.datetime(2017,5,21), '$lte':datetime.datetime(2017, 5, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #                 fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id', 'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )

    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_5b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,5,11), '$lte':datetime.datetime(2017, 5, 20)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )

    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_5a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,5,1), '$lte':datetime.datetime(2017, 5, 10)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_4c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,4,21), '$lte':datetime.datetime(2017, 4, 30)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_4b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,4,11), '$lte':datetime.datetime(2017, 4, 20)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_4a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,4,1), '$lte':datetime.datetime(2017, 4, 10)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_3c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,3,21), '$lte':datetime.datetime(2017, 3, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_3b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,3,11), '$lte':datetime.datetime(2017, 3, 20)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_3a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,3,1), '$lte':datetime.datetime(2017, 3, 10)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_2c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,2,20), '$lte':datetime.datetime(2017, 2, 28)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_2b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,2,10), '$lte':datetime.datetime(2017, 2, 19)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_2a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,2,1), '$lte':datetime.datetime(2017, 2, 9)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_1c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,1,21), '$lte':datetime.datetime(2017, 1, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_1b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,1,11), '$lte':datetime.datetime(2017, 1, 20)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question3:
    #      (pipeline_question3
    #          | 'Readquestions3_1a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2017,1,1), '$lte':datetime.datetime(2017, 1, 10)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions3'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
# with beam.Pipeline(runner='DirectRunner') as pipeline_question2:
    # with beam.Pipeline(options = options) as pipeline_question2:
    #      (pipeline_question2
    #          | 'Readquestions2_12b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2016,12,16), '$lte':datetime.datetime(2016, 12, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions2'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question2:
    #      (pipeline_question2
    #          | 'Readquestions2_12a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2016,12,1), '$lte':datetime.datetime(2016, 12, 15)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions2'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question2:
    #      (pipeline_question2
    #          | 'Readquestions2_11' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2016,11,1), '$lte':datetime.datetime(2016, 11, 30)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions2'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question2:
    #      (pipeline_question2
    #          | 'Readquestions2_10' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2016,10,1), '$lte':datetime.datetime(2016, 10, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions2'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question2:
    #      (pipeline_question2
    #          | 'Readquestions2_9' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2016,9,1), '$lte':datetime.datetime(2016, 9, 30)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #         # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions2'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question2:
    #      (pipeline_question2
    #          | 'Readquestions2_8' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$gte':datetime.datetime(2016,8,1), '$lte':datetime.datetime(2016, 8, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions2'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    # with beam.Pipeline(options = options) as pipeline_question1:
    #      (pipeline_question1
    #          | 'Readquestions1_lte7_2016' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #              query={'created_at':{'$lte':datetime.datetime(2016, 7, 31)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #              fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #              'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #          | 'get_price' >> beam.ParDo(GetPriceQFn())
    #          | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #          | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #          # | 'DebugQ' >> beam.Map(debug_function)
    #          | 'writeQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions1'),
    #                   schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
