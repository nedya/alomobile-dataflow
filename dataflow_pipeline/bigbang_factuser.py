import apache_beam as beam
from datetime import datetime, timedelta
import itertools
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from google.oauth2.service_account import Credentials
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math
import re


connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"
scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)
PROJECT='plenary-justice-151004'
BUCKET='staging-plenary-justice-151004'

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

#fact_users
def join_infoUC((k,v)):
    return itertools.product(v['user_id'], v['channel_id'])

def merge_dictUC(user_channel):
      (user, channel) = user_channel
      a_dict = user
      a_dict.update(channel)
      return a_dict

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
#    pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])
# def run():
#     options = PipelineOptions()
#     google_cloud_options = options.view_as(GoogleCloudOptions)
#     google_cloud_options.project = 'plenary-justice-151004'
#     google_cloud_options.job_name = 'sourcemongo'
#     google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
#     google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
#     google_cloud_options.region = 'asia-east1'
#     options.view_as(StandardOptions).runner = 'DirectRunner'
#     options.view_as(SetupOptions).save_main_session = True
#
    # pipeline_options = PipelineOptions()
    # google_cloud_options = pipeline_options.view_as(GoogleCloudOptions)
    # google_cloud_options.project = 'plenary-justice-151004'
    # pipeline_options.view_as(StandardOptions).runner = 'DirectRunner'


    # with beam.Pipeline(runner = 'DirectRunner', options = pipeline_options) as pipeline_fact_users5:
    # with beam.Pipeline(options = options) as pipeline_fact_users5:
    #     registration_channel = (pipeline_fact_users5
    #         |'Readdataregchan' >> beam.io.Read(beam.io.BigQuerySource(query='SELECT id, provider FROM alowarehouse_alodoktermobile.registration_channel', use_standard_sql=True))
    #         |'ExtractTextColumnRC' >> beam.Map(lambda row: (row['provider'], {'channel_id':row['id']}))
    #                         )
    #     user = (pipeline_fact_users5
    #         |'Readfactusers5' >> ReadFromMongo(connection_string, 'alomobile', 'users',
    #                                          query={'created_at':{'$gte':datetime(2018,6,1)},
    #                                          '_type':'Core::User'},
    #                                          fields=['_id', 'provider', 'created_at'])
    #         | beam.Map(lambda x:(x['provider'], {'user_id':x['_id'], 'created_at':x['created_at']}))
    #             )
    #     result = ({'user_id':user, 'channel_id':registration_channel} | 'result:UC' >> beam.CoGroupByKey()
    #         |'joinUC'>> beam.FlatMap(join_infoUC)
    #         |'merge_dictUC' >> beam.Map(merge_dictUC)
    #         |'formatdataFU' >> beam.Map (lambda x:{'id':str(uuid.uuid4()),
    #                                             'user_id':str(x['user_id']),
    #                                             'channel_id':x['channel_id'],
    #                                             'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
    #                                             'year_id':x['created_at'].year,
    #                                             'month_id':x['created_at'].month,
    #                                             'date_id':x['created_at'].day,
    #                                             'hour_id':x['created_at'].hour,
    #                                             'minute_id':x['created_at'].minute})
    #         |'Debug_fact_users' >> beam.Map(debug_function)
#             | 'writefactusersToBQ' >> beam.io.Write(
#                           beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_users05'),
#                           schema='id:STRING, user_id:STRING, channel_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                           create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                           write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#             )

# with beam.Pipeline(runner = 'DirectRunner', options = pipeline_options) as pipeline_fact_users4:
    # with beam.Pipeline(options = options) as pipeline_fact_users4:
    #     registration_channel = (pipeline_fact_users4
    #         |'Readdataregchan' >> beam.io.Read(beam.io.BigQuerySource(query='SELECT id, provider FROM alowarehouse_alodoktermobile.registration_channel', use_standard_sql=True))
    #         |'ExtractTextColumnRC' >> beam.Map(lambda row: (row['provider'], {'channel_id':row['id']}))
    #                         )
    #     user = (pipeline_fact_users4
    #         |'Readfactusers4' >> ReadFromMongo(connection_string, 'alomobile', 'users',
    #                                          query={'created_at':{'$gte':datetime(2018,1,1), '$lte':datetime(2018,5,31)},
    #                                          '_type':'Core::User'},
    #                                          fields=['_id', 'provider', 'created_at'])
    #         | beam.Map(lambda x:(x['provider'], {'user_id':x['_id'], 'created_at':x['created_at']}))
    #             )
    #     result = ({'user_id':user, 'channel_id':registration_channel} | 'result:UC' >> beam.CoGroupByKey()
    #         |'joinUC'>> beam.FlatMap(join_infoUC)
    #         |'merge_dictUC' >> beam.Map(merge_dictUC)
    #         |'formatdataFU' >> beam.Map (lambda x:{'id':str(uuid.uuid4()),
    #                                             'user_id':str(x['user_id']),
    #                                             'channel_id':x['channel_id'],
    #                                             'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
    #                                             'year_id':x['created_at'].year,
    #                                             'month_id':x['created_at'].month,
    #                                             'date_id':x['created_at'].day,
    #                                             'hour_id':x['created_at'].hour,
    #                                             'minute_id':x['created_at'].minute})
    #         # |'Debug_fact_users' >> beam.Map(debug_function)
    #         | 'writefactusersToBQ' >> beam.io.Write(
    #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_users04'),
    #                       schema='id:STRING, user_id:STRING, channel_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #         )

# with beam.Pipeline(runner = 'DirectRunner', options = pipeline_options) as pipeline_fact_users3:
    # with beam.Pipeline(options = options) as pipeline_fact_users3:
    #     registration_channel = (pipeline_fact_users3
    #         |'Readdataregchan' >> beam.io.Read(beam.io.BigQuerySource(query='SELECT id, provider FROM alowarehouse_alodoktermobile.registration_channel', use_standard_sql=True))
    #         |'ExtractTextColumnRC' >> beam.Map(lambda row: (row['provider'], {'channel_id':row['id']}))
    #                         )
    #     user = (pipeline_fact_users3
    #         |'Readfactusers3' >> ReadFromMongo(connection_string, 'alomobile', 'users',
    #                                          query={'created_at':{'$gte':datetime(2017,6,1), '$lte':datetime(2017,12,31)},
    #                                          '_type':'Core::User'},
    #                                          fields=['_id', 'provider', 'created_at'])
    #         | beam.Map(lambda x:(x['provider'], {'user_id':x['_id'], 'created_at':x['created_at']}))
    #             )
    #     result = ({'user_id':user, 'channel_id':registration_channel} | 'result:UC' >> beam.CoGroupByKey()
    #         |'joinUC'>> beam.FlatMap(join_infoUC)
    #         |'merge_dictUC' >> beam.Map(merge_dictUC)
    #         |'formatdataFU' >> beam.Map (lambda x:{'id':str(uuid.uuid4()),
    #                                             'user_id':str(x['user_id']),
    #                                             'channel_id':x['channel_id'],
    #                                             'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
    #                                             'year_id':x['created_at'].year,
    #                                             'month_id':x['created_at'].month,
    #                                             'date_id':x['created_at'].day,
    #                                             'hour_id':x['created_at'].hour,
    #                                             'minute_id':x['created_at'].minute})
    #         # |'Debug_fact_users' >> beam.Map(debug_function)
    #         | 'writefactusersToBQ' >> beam.io.Write(
    #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_users03'),
    #                       schema='id:STRING, user_id:STRING, channel_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #  )

# with beam.Pipeline(runner = 'DirectRunner', options = pipeline_options) as pipeline_fact_users2:
    # with beam.Pipeline(options = options) as pipeline_fact_users2:
    #     registration_channel = (pipeline_fact_users2
    #         |'Readdataregchan' >> beam.io.Read(beam.io.BigQuerySource(query='SELECT id, provider FROM alowarehouse_alodoktermobile.registration_channel', use_standard_sql=True))
    #         |'ExtractTextColumnRC' >> beam.Map(lambda row: (row['provider'], {'channel_id':row['id']}))
    #                         )
    #     user = (pipeline_fact_users2
    #         |'Readfactusers2' >> ReadFromMongo(connection_string, 'alomobile', 'users',
    #                                          query={'created_at':{'$gte':datetime(2017,1,1), '$lte':datetime(2017,5,31)},
    #                                          '_type':'Core::User'},
    #                                          fields=['_id', 'provider', 'created_at'])
    #         | beam.Map(lambda x:(x['provider'], {'user_id':x['_id'], 'created_at':x['created_at']}))
    #             )
    #     result = ({'user_id':user, 'channel_id':registration_channel} | 'result:UC' >> beam.CoGroupByKey()
    #         |'joinUC'>> beam.FlatMap(join_infoUC)
    #         |'merge_dictUC' >> beam.Map(merge_dictUC)
    #         |'formatdataFU' >> beam.Map (lambda x:{'id':str(uuid.uuid4()),
    #                                             'user_id':str(x['user_id']),
    #                                             'channel_id':x['channel_id'],
    #                                             'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
    #                                             'year_id':x['created_at'].year,
    #                                             'month_id':x['created_at'].month,
    #                                             'date_id':x['created_at'].day,
    #                                             'hour_id':x['created_at'].hour,
    #                                             'minute_id':x['created_at'].minute})
    #         # |'Debug_fact_users' >> beam.Map(debug_function)
    #         | 'writefactusersToBQ' >> beam.io.Write(
    #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_users02'),
    #                       schema='id:STRING, user_id:STRING, channel_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #  )

# with beam.Pipeline(runner = 'DirectRunner', options = pipeline_options) as pipeline_fact_users1:
    with beam.Pipeline(options = options) as pipeline_fact_users1:
        registration_channel = (pipeline_fact_users1
            |'Readdataregchan' >> beam.io.Read(beam.io.BigQuerySource(query='SELECT id, provider FROM alowarehouse_alodoktermobile.registration_channel', use_standard_sql=True))
            |'ExtractTextColumnRC' >> beam.Map(lambda row: (row['provider'], {'channel_id':row['id']}))
                            )
        user = (pipeline_fact_users1
            |'Readfactusers1' >> ReadFromMongo(connection_string, 'alomobile', 'users',
                                             query={'created_at':{'$lte':datetime(2016, 12, 31)},
                                             '_type':'Core::User'},
                                             fields=['_id', 'provider', 'created_at'])
            | beam.Map(lambda x:(x['provider'], {'user_id':x['_id'], 'created_at':x['created_at']}))
                )
        result = ({'user_id':user, 'channel_id':registration_channel} | 'result:UC' >> beam.CoGroupByKey()
            |'joinUC'>> beam.FlatMap(join_infoUC)
            |'merge_dictUC' >> beam.Map(merge_dictUC)
            |'formatdataFU' >> beam.Map (lambda x:{'id':str(uuid.uuid4()),
                                                'user_id':str(x['user_id']),
                                                'channel_id':x['channel_id'],
                                                'created_at':str(x['created_at']+timedelta(hours=7))[0:19],
                                                'year_id':x['created_at'].year,
                                                'month_id':x['created_at'].month,
                                                'date_id':x['created_at'].day,
                                                'hour_id':x['created_at'].hour,
                                                'minute_id':x['created_at'].minute})
            # |'Debug_fact_users' >> beam.Map(debug_function)
            | 'writefactusers1ToBQ' >> beam.io.Write(
                          beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_users010'),
                          schema='id:STRING, user_id:STRING, channel_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
                          create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                          write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
     )
