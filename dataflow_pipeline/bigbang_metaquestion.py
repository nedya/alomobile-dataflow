import apache_beam as beam
import datetime
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math
import re

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.200:27017/alomobile"
connection_string = "mongodb://grumpycat:alo.1975.dokter@35.198.223.103/alomobile"
tdelta = datetime.timedelta(hours=7)

#dimensi meta question
def maxrecMetaQ(m):
        if m.get('max_count_recommendation', None) is None:
                m['max_count_recommendation'] = 3
        return m

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]

    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])
   
# with beam.Pipeline(runner = "DirectRunner") as pipeline_meta_question:
    with beam.Pipeline(options = options) as pipeline_meta_question5:
        (pipeline_meta_question5
            | 'ReadMetaQmob5' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    query={'created_at':{'$gte':datetime.datetime(2018,7,1)}, "_type":'Core::MetaQuestion'},
                                    fields=['title', 'choice', 'created_at', 'max_count_recommendation'])
            |'strMetaQ'>>beam.Map (lambda x:{'id':str(x['_id']),
                                 'title':x['title'],
                                 'created_at':str(x['created_at']+datetime.timedelta(hours=7))[0:19],
                                 'answer_by_user':x['choice'],
                                 'max_count_recommendation':x.get('max_count_recommendation', None)
                                })
            | 'maxrecMetaQ' >> beam.Map(maxrecMetaQ)
        #     | 'DebugMetaQ' >> beam.Map(debug_function)
             | 'writeMetaQToBQ' >> beam.io.Write(
                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','metaquestion5'),
                       schema='id:STRING, title:STRING, created_at:DATETIME, answer_by_user:BOOLEAN, max_count_recommendation:INTEGER',
                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_meta_question4:
        (pipeline_meta_question4
            | 'ReadMetaQmob4' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    query={'created_at':{'$gte':datetime.datetime(2018,1,1), '$lte':datetime.datetime(2018,6,30)}, "_type":'Core::MetaQuestion'},
                                    fields=['title', 'choice', 'created_at', 'max_count_recommendation'])
            |'strMetaQ'>>beam.Map (lambda x:{'id':str(x['_id']),
                                 'title':x['title'],
                                 'created_at':str(x['created_at']+datetime.timedelta(hours=7))[0:19],
                                 'answer_by_user':x['choice'],
                                 'max_count_recommendation':x.get('max_count_recommendation', None)
                                })
            | 'maxrecMetaQ' >> beam.Map(maxrecMetaQ)
            # | 'DebugMetaQ' >> beam.Map(debug_function)
            | 'writeMetaQToBQ' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','metaquestion4'),
                      schema='id:STRING, title:STRING, created_at:DATETIME, answer_by_user:BOOLEAN, max_count_recommendation:INTEGER',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_meta_question3:
        (pipeline_meta_question3
            | 'ReadMetaQmob3' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    query={'created_at':{'$gte':datetime.datetime(2017,7,1), '$lte':datetime.datetime(2017, 12, 31)}, "_type":'Core::MetaQuestion'},
                                    fields=['title', 'choice', 'created_at', 'max_count_recommendation'])
            |'strMetaQ'>>beam.Map (lambda x:{'id':str(x['_id']),
                                 'title':x['title'],
                                 'created_at':str(x['created_at']+datetime.timedelta(hours=7))[0:19],
                                 'answer_by_user':x['choice'],
                                 'max_count_recommendation':x.get('max_count_recommendation', None)
                                })
            | 'maxrecMetaQ' >> beam.Map(maxrecMetaQ)
            # | 'DebugMetaQ' >> beam.Map(debug_function)
            | 'writeMetaQToBQ' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','metaquestion3'),
                      schema='id:STRING, title:STRING, created_at:DATETIME, answer_by_user:BOOLEAN, max_count_recommendation:INTEGER',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_meta_question2:
        (pipeline_meta_question2
            | 'ReadMetaQmob2' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    query={'created_at':{'$gte':datetime.datetime(2017,1, 1), '$lte':datetime.datetime(2017, 6, 30)}, "_type":'Core::MetaQuestion'},
                                    fields=['title', 'choice', 'created_at', 'max_count_recommendation'])
            |'strMetaQ'>>beam.Map (lambda x:{'id':str(x['_id']),
                                 'title':x['title'],
                                 'created_at':str(x['created_at']+datetime.timedelta(hours=7))[0:19],
                                 'answer_by_user':x['choice'],
                                 'max_count_recommendation':x.get('max_count_recommendation', None)
                                })
            | 'maxrecMetaQ' >> beam.Map(maxrecMetaQ)
        #     | 'DebugMetaQ' >> beam.Map(debug_function)
            | 'writeMetaQToBQ' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','metaquestion2'),
                      schema='id:STRING, title:STRING, created_at:DATETIME, answer_by_user:BOOLEAN, max_count_recommendation:INTEGER',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_meta_question1:
        (pipeline_meta_question1
            | 'ReadMetaQmob1' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    query={'created_at':{'$lte':datetime.datetime(2016,12,31)}, "_type":'Core::MetaQuestion'},
                                    fields=['title', 'choice', 'created_at', 'max_count_recommendation'])
            |'strMetaQ'>>beam.Map (lambda x:{'id':str(x['_id']),
                                 'title':x['title'],
                                 'created_at':str(x['created_at']+datetime.timedelta(hours=7))[0:19],
                                 'answer_by_user':x['choice'],
                                 'max_count_recommendation':x.get('max_count_recommendation', None)
                                })
            | 'maxrecMetaQ' >> beam.Map(maxrecMetaQ)
#             | 'DebugMetaQ' >> beam.Map(debug_function)
            | 'writeMetaQToBQ' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','metaquestion1'),
                      schema='id:STRING, title:STRING, created_at:DATETIME, answer_by_user:BOOLEAN, max_count_recommendation:INTEGER',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )
