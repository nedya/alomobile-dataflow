
# coding: utf-8

# In[ ]:


import apache_beam as beam
from datetime import datetime, timedelta
from pymongo import MongoClient
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from bson.objectid import ObjectId


PROJECT='plenary-justice-151004'
BUCKET='staging-plenary-justice-151004'

#   connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.225.73/alomobile"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"
#  connection_string = "mongodb://grumpycat:alo.1975.dokter@112.78.168.133/alomobile"
start = datetime.utcnow()-timedelta(hours=6)
#end = datetime.datetime(2018, 7, 15)
tdelta = timedelta(hours=7)

def debug_function(pcollection_as_list):
    print(pcollection_as_list)

class GetDayFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        days = []
        day_query = db.meta_slot_days.find({'_id':element["meta_slot_day_id"]})
        for day in day_query:
            for time_slot in element['doctor_time_slots']:
                if time_slot['end_of_date'] is not None:
                    if time_slot['end_of_date'] != 'None':
                        if time_slot['end_of_date'] != '':
                            days.append({'day':day['name'],
                                         'meta_slot_id':str(element['_id']),
                                         'time_slot_id':element['time_slot_management_id'],
                                         'doctor_id':str(time_slot['doctor_id']),
                                         'start_of_date':str(time_slot['start_of_date'])[0:10],
                                         'end_of_date':str(time_slot['end_of_date'])[0:10],
                                         'end_day':str(time_slot['end_of_date'].isoweekday()),
                                         'booking_time':str(time_slot['created_at'])[0:19]})
                if time_slot['end_of_date'] is None or time_slot['end_of_date'] == "None" or time_slot['end_of_date'] == '':
                    days.append({'day':day['name'],
                                         'meta_slot_id':str(element['_id']),
                                         'time_slot_id':element['time_slot_management_id'],
                                         'doctor_id':str(time_slot['doctor_id']),
                                         'start_of_date':str(time_slot['start_of_date'])[0:10],
                                         'end_of_date':None,
                                         'end_day':None,
                                         'booking_time':str(time_slot['created_at'])[0:19]})
            return days

class GetTimeFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        times = []
        time_query = db.time_slot_managements.find({'_id':element['time_slot_id']})
        for time in time_query:
            times.append({'meta_slot_id':element['meta_slot_id'],
                          'day':element['day'],
                          'start_time':time['start_time'],
                          'end_time':time['end_time'],
                          'doctor_type':time['doctor_type'],
                          'doctor_id':element['doctor_id'],
                          'start_of_date':element['start_of_date'],
                          'end_of_date':element['end_of_date'],
                          'end_day':element['end_day'],
                          'booking_time':element['booking_time']})
        return times

class GetMetaFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        metaslots = []
        metaslot_query = db.meta_slots.find({'_id':ObjectId(element['meta_slot_id'])})
        for metaslot in metaslot_query:
            metaslots.append({'doctor_id':str(element['doctor_id']),
                               'time_slot_management_id':metaslot['time_slot_management_id'],
                               'slot_id':str(element['_id']),
                               'date_slot':str(element['date_slot'])[0:10],
                               'created_at':str(element['created_at'])[0:19]})
            return metaslots

class GetTimeSingleFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        times = []
        time_query = db.time_slot_managements.find({'_id':ObjectId(element['time_slot_management_id'])})
        for time in time_query:
            times.append({'doctor_id':str(element['doctor_id']),
                          'start_time':time['start_time'],
                          'end_time':time['end_time'],
                          'doctor_type':time['doctor_type'],
                          'slot_id':element['slot_id'],
                          'date_slot':element['date_slot'],
                          'created_at':element['created_at']})
        return times

#    class GetDateFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         dates = []
#         date_query = db.meta_slot_bonuses.find({'meta_slot_id':ObjectId(element['meta_slot_id'])})
#         for date in date_query:
#             dates.append({'meta_slot_id':element['meta_slot_id'],
#                           'day':element['day'],
#                           'date_slot':str(date['date_slot'])[0:10],
#                           'start_time':element['start_time'],
#                           'end_time':element['end_time'],
#                           'doctor_type':element['doctor_type'],
#                           'doctor_id':element['doctor_id'],
#                           'start_of_date':element['start_of_date'],
#                           'end_of_date':element['end_of_date'],
#                           'end_day':element['end_day'],
#                           'booking_time':element['booking_time']})
#         return dates

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]

    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'

    with beam.Pipeline(options = options) as p_singleslot:
        (p_singleslot
            | 'read_slot' >> ReadFromMongo(connection_string, 'alomobile', 'single_meta_slots', query={'is_deleted':False, 'date_slot':{'$gte':start}}, fields=['_id', 'date_slot', 'doctor_id', 'meta_slot_id', 'created_at'])
            | 'get_meta' >> beam.ParDo(GetMetaFn())
            | 'get_time_single' >> beam.ParDo(GetTimeSingleFn())
#           | 'Debug' >> beam.Map(debug_function)
            | 'writeToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','single_meta_slots'),
                     schema= 'slot_id:STRING, doctor_id:STRING, date_slot:DATE, start_time:STRING,  end_time:STRING, created_at:DATETIME, doctor_type:STRING',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )

    with beam.Pipeline(options = options) as p_weeklyslot:
        (p_weeklyslot
            | 'read_slot' >> ReadFromMongo(connection_string, 'alomobile', 'meta_slots', query={}, fields=["meta_slot_day_id", "time_slot_management_id", "doctor_time_slots.doctor_id", "doctor_time_slots.start_of_date", "doctor_time_slots.end_of_date","doctor_time_slots.created_at"])
            | 'get_day' >> beam.ParDo(GetDayFn())
            | 'get_time' >> beam.ParDo(GetTimeFn())
        #   | 'get_date' >> beam.ParDo(GetDateFn())
    #       | 'Debug' >> beam.Map(debug_function)
            | 'writeToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','weekly_meta_slots'),
                     schema= 'booking_time:DATETIME, start_time:STRING, end_time:STRING, start_of_date:STRING, end_of_date:STRING, doctor_id:STRING, meta_slot_id:STRING, day:STRING, doctor_type:STRING, end_day:STRING',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

   # p.run().wait_until_finish()

# if __name__ == '__main__':
#    run()
