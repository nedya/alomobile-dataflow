import apache_beam as beam
from datetime import datetime, timedelta
import itertools
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math
import re


def debug_function(pcollection_as_list):
    print (pcollection_as_list)

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.109:27017/alomobile"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@192.168.1.133:27017/alomobile"
connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"


scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)

#fact questions (dari FQ1_10c_2017' keatas)>>'created_at': {'$gte':datetime(2017, 10, 21)
# class if_meta_question(beam.DoFn):
#     def process(self, element):
#         data = []
#         if element.get('time_picked_up') is not None:
#             if element['_type'] == "Core::Question" or element['_type'] == "Core::PreQuestion":
#                 data.append({'topic':element['topic'],
#                                   'question_id':str(element['_id']),
#                                   'meta_question_id':str(element.get('meta_question_id',"")),
#                                   'user_id':str(element['user_id']),
#                                   'picked_up_by_id':str(element.get('picked_up_by_id',"")),
#                                   'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
#                                   'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
#                                   'intent_id':str(element.get('intent_id',"")),
#                                   'sub_intent_id':str(element.get('sub_intent_id',"")),
#                                   'time_picked_up':str(element['time_picked_up']+timedelta(hours=7))[0:19],
#                                   'journal_id':str(element.get('journal_id')),
#                                   'date_picked_up':str(element['time_picked_up'])[0:10],
#                                   'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
#                                   'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
#                                   'is_has_conclusion':element.get('is_has_conclusion', None),
#                                   'no_conclusion':element.get('no_conclusion', None)})
#
#             elif element['_type'] == "Core::MetaQuestion":
#                 data.append({'topic':element['topic'],
#                                   'question_id':None,
#                                   'meta_question_id':str(element['_id']),
#                                   'user_id':str(element['user_id']),
#                                   'picked_up_by_id':str(element.get('picked_up_by_id',"")),
#                                   'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
#                                   'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
#                                   'intent_id':str(element.get('intent_id',"")),
#                                   'sub_intent_id':str(element.get('sub_intent_id',"")),
#                                   'time_picked_up':str(element['time_picked_up']+timedelta(hours=7))[0:19],
#                                   'journal_id':str(element.get('journal_id')),
#                                   'date_picked_up':str(element['time_picked_up'])[0:10],
#                                   'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
#                                   'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
#                                   'is_has_conclusion':element.get('is_has_conclusion', None),
#                                   'no_conclusion':element.get('no_conclusion', None)})
#         return data
#
# class GetTermsFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         terms = []
#         term_query = db.terms.find({'name':element['topic']}, no_cursor_timeout=True)
#         if element.get('time_picked_up') is not None:
#             for term in term_query:
#                 if element.get('journal_id', None) is not None:
#                     terms.append({'topic_id':str(term['_id']),
#                                   'question_id':element['question_id'],
#                                   'meta_question_id':element['meta_question_id'],
#                                   'user_id':element['user_id'],
#                                   'picked_up_by_id':element['picked_up_by_id'],
#                                   'created_at':element['created_at'],
#                                   'updated_at':element['updated_at'],
#                                   'intent_id':element['intent_id'],
#                                   'sub_intent_id':element['sub_intent_id'],
#                                   'time_picked_up':element['time_picked_up'],
#                                   'journal_id':element['journal_id'],
#                                   'date_picked_up':element['date_picked_up'],
#                                   'first_user_answer_time':element['first_user_answer_time'],
#                                   'first_doctor_answer_time':element['first_doctor_answer_time'],
#                                   'is_has_conclusion':element['is_has_conclusion'],
#                                   'no_conclusion':element['no_conclusion']})
#                 elif element.get('journal_id', None) is None:
#                     terms.append({'topic_id':str(term['_id']),
#                                   'question_id':element['question_id'],
#                                   'meta_question_id':element['meta_question_id'],
#                                   'user_id':element['user_id'],
#                                   'picked_up_by_id':element['picked_up_by_id'],
#                                   'created_at':element['created_at'],
#                                   'updated_at':element['updated_at'],
#                                   'intent_id':element['intent_id'],
#                                   'sub_intent_id':element['sub_intent_id'],
#                                   'time_picked_up':element['time_picked_up'],
#                                   'journal_id':None,
#                                   'date_picked_up':element['date_picked_up'],
#                                   'first_doctor_answer_time':element['first_doctor_answer_time'],
#                                   'is_has_conclusion':element['is_has_conclusion'],
#                                   'no_conclusion':element['no_conclusion']})
#         return terms
#
# class GetConclusionFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         conclusions = []
#         if element.get('is_has_conclusion') is True:
#             conclusion_query = db.question_conclusions.find({'question_id':ObjectId(element['question_id'])})
#             for conclusion in conclusion_query:
#                 conclusion_id = str(conclusion['_id'])
#         else: conclusion_id = None
#         conclusions.append({'conclusion_id':conclusion_id,
#                         'topic_id':element['topic_id'],
#                         'question_id':element['question_id'],
#                         'meta_question_id':element['meta_question_id'],
#                         'user_id':element['user_id'],
#                         'picked_up_by_id':element['picked_up_by_id'],
#                         'created_at':element['created_at'],
#                         'updated_at':element['updated_at'],
#                         'intent_id':element['intent_id'],
#                         'sub_intent_id':element['sub_intent_id'],
#                         'time_picked_up':element['time_picked_up'],
#                         'journal_id':element['journal_id'],
#                         'date_picked_up':element['date_picked_up'],
#                         'first_user_answer_time':element['first_user_answer_time'],
#                         'first_doctor_answer_time':element['first_doctor_answer_time'],
#                         'is_has_conclusion':element['is_has_conclusion'],
#                         'no_conclusion':element['no_conclusion']})
#         return conclusions
#
# class GetPaymentFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         journals = []
#         if element['journal_id'] != 'None' :
#             journal_query = db.journals.find({'_id':ObjectId(element['journal_id'])})
#             for journal in journal_query:
#                 journal_id = element['journal_id']
#                 payment_method_id = str(journal['payment_method_id'])
#         else:
#             journal_id = None
#             payment_method_id = None
#         journals.append({'id':str(uuid.uuid4()),
#                          # 'conclusion_id':element['conclusion_id'],
#                          'topic_id':element['topic_id'],
#                          'question_id':element['question_id'],
#                          'meta_question_id':element['meta_question_id'],
#                          'user_id':element['user_id'],
#                          'picked_up_by_id':element['picked_up_by_id'],
#                          'created_at':element['created_at'],
#                          'updated_at':element['updated_at'],
#                          'intent_id':element['intent_id'],
#                          'sub_intent_id':element['sub_intent_id'],
#                          'time_picked_up':element['time_picked_up'],
#                          'journal_id':journal_id,
#                          'payment_method_id':payment_method_id,
#                          'date_picked_up':element['date_picked_up'],
#                          'first_user_answer_time':element['first_user_answer_time'],
#                          'first_doctor_answer_time':element['first_doctor_answer_time'],
#                          'is_has_conclusion':element['is_has_conclusion'],
#                          'no_conclusion':element['no_conclusion']})
#         return journals

##fact_questionscK dari FQ1_10b_2017 kebawah >> 'created_at': '$lte':datetime(2017, 10, 20)
class if_meta_question(beam.DoFn):
    def process(self, element):
        data = []
        # if element.get('time_picked_up') is not None:
        #     time_picked_up = str(element['time_picked_up']+timedelta(hours=7))[0:19]
        #      date_picked_up = str(time_picked_up)[0:10]
        # else:
        time_picked_up = None
        date_picked_up = None

        if element.get('_type') == "Core::Question" or element.get('_type') == "Core::PreQuestion":
            data.append({'topic':element.get('topic', None),
                        'question_id':str(element['_id']),
                        'meta_question_id':str(element.get('meta_question_id',"")),
                        'user_id':str(element['user_id']),
                        'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                        'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                        'intent_id':str(element.get('intent_id',"")),
                        'sub_intent_id':str(element.get('sub_intent_id',"")),
                        'time_picked_up':time_picked_up,
                        'journal_id':str(element.get('journal_id')),
                        'date_picked_up':date_picked_up,
                        'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
                        'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
                        'is_has_conclusion':element.get('is_has_conclusion', None),
                        'no_conclusion':element.get('no_conclusion', None)})

        elif element.get('_type') == "Core::MetaQuestion":
            data.append({'topic':element.get('topic', None),
                        'question_id':None,
                        'meta_question_id':str(element['_id']),
                        'user_id':str(element['user_id']),
                        'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                        'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                        'intent_id':str(element.get('intent_id',"")),
                        'sub_intent_id':str(element.get('sub_intent_id',"")),
                        'time_picked_up':time_picked_up,
                        'journal_id':str(element.get('journal_id')),
                        'date_picked_up':date_picked_up,
                        'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
                        'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
                        'is_has_conclusion':element.get('is_has_conclusion', None),
                        'no_conclusion':element.get('no_conclusion', None)})
        else:
            data.append({'topic':element.get('topic', None),
                        'question_id':str(element['_id']),
                        'meta_question_id':str(element.get('meta_question_id',"")),
                        'user_id':str(element['user_id']),
                        'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                        'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                        'intent_id':str(element.get('intent_id',"")),
                        'sub_intent_id':str(element.get('sub_intent_id',"")),
                        'time_picked_up':time_picked_up,
                        'journal_id':str(element.get('journal_id')),
                        'date_picked_up':str(time_picked_up)[0:10],
                        'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
                        'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
                        'is_has_conclusion':element.get('is_has_conclusion', None),
                        'no_conclusion':element.get('no_conclusion', None)})
        return data

class GetTermsFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        terms = []
        term_query = db.terms.find({'name':element['topic']}, no_cursor_timeout=True)
        # if element.get('time_picked_up') is not None:
        #     time_picked_up =  element['time_picked_up']
        # else:
        time_picked_up = None

        for term in term_query:
            if element.get('journal_id', None) is not None:
                terms.append({'topic_id':str(term['_id']),
                                  'question_id':element['question_id'],
                                  'meta_question_id':element['meta_question_id'],
                                  'user_id':element['user_id'],
                                  'picked_up_by_id':element['picked_up_by_id'],
                                  'created_at':element['created_at'],
                                  'updated_at':element['updated_at'],
                                  'intent_id':element['intent_id'],
                                  'sub_intent_id':element['sub_intent_id'],
                                  'time_picked_up':time_picked_up,
                                  'journal_id':element['journal_id'],
                                  'date_picked_up':element['date_picked_up'],
                                  'first_user_answer_time':element['first_user_answer_time'],
                                  'first_doctor_answer_time':element['first_doctor_answer_time'],
                                  'is_has_conclusion':element['is_has_conclusion'],
                                  'no_conclusion':element['no_conclusion']})
            elif element.get('journal_id', None) is None:
                terms.append({'topic_id':str(term['_id']),
                                  'question_id':element['question_id'],
                                  'meta_question_id':element['meta_question_id'],
                                  'user_id':element['user_id'],
                                  'picked_up_by_id':element['picked_up_by_id'],
                                  'created_at':element['created_at'],
                                  'updated_at':element['updated_at'],
                                  'intent_id':element['intent_id'],
                                  'sub_intent_id':element['sub_intent_id'],
                                  'time_picked_up':time_picked_up,
                                  'journal_id':None,
                                  'date_picked_up':element['date_picked_up'],
                                  'first_doctor_answer_time':element['first_doctor_answer_time'],
                                  'is_has_conclusion':element['is_has_conclusion'],
                                  'no_conclusion':element['no_conclusion']})
        return terms

class GetConclusionFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        conclusions = []
        if element.get('is_has_conclusion') is True:
            conclusion_query = db.question_conclusions.find({'question_id':ObjectId(element['question_id'])})
            for conclusion in conclusion_query:
                conclusion_id = str(conclusion['_id'])
        else: conclusion_id = None
        conclusions.append({'conclusion_id':conclusion_id,
                        'topic_id':element['topic_id'],
                        'question_id':element['question_id'],
                        'meta_question_id':element['meta_question_id'],
                        'user_id':element['user_id'],
                        'picked_up_by_id':element['picked_up_by_id'],
                        'created_at':element['created_at'],
                        'updated_at':element['updated_at'],
                        'intent_id':element['intent_id'],
                        'sub_intent_id':element['sub_intent_id'],
                        'time_picked_up':element['time_picked_up'],
                        'journal_id':element['journal_id'],
                        'date_picked_up':element['date_picked_up'],
                        'first_user_answer_time':element['first_user_answer_time'],
                        'first_doctor_answer_time':element['first_doctor_answer_time'],
                        'is_has_conclusion':element['is_has_conclusion'],
                        'no_conclusion':element['no_conclusion']})
        return conclusions

class GetPaymentFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        journals = []
        if element['journal_id'] != 'None' :
            journal_query = db.journals.find({'_id':ObjectId(element['journal_id'])})
            for journal in journal_query:
                journal_id = element['journal_id']
                payment_method_id = str(journal['payment_method_id'])
        else:
            journal_id = None
            payment_method_id = None

        if element['date_picked_up'] == 'None':
            element['date_picked_up'] = None

        journals.append({'id':str(uuid.uuid4()),
                         'topic_id':element.get('topic_id'),
                         'question_id':element['question_id'],
                         'meta_question_id':element['meta_question_id'],
                         'user_id':element['user_id'],
                         'picked_up_by_id':element['picked_up_by_id'],
                         'created_at':element['created_at'],
                         'updated_at':element['updated_at'],
                         'intent_id':element['intent_id'],
                         'sub_intent_id':element['sub_intent_id'],
                         'time_picked_up':element['time_picked_up'],
                         'journal_id':journal_id,
                         'payment_method_id':payment_method_id,
                         'date_picked_up':element['date_picked_up'],
                         'first_user_answer_time':element['first_user_answer_time'],
                         'first_doctor_answer_time':element['first_doctor_answer_time'],
                         'is_has_conclusion':element['is_has_conclusion'],
                         'no_conclusion':element['no_conclusion']})
        return journals

#fact_questionscKT, code for FQ1_lte8_2016 (_type, keyError)
# class if_meta_question(beam.DoFn):
#     def process(self, element):
#         data = []
#         if element.get('time_picked_up') is not None:
#             if element.get('_type', None) == "Core::Question" or element.get('_type', None) == "Core::PreQuestion":
#                 data.append({'topic':element.get('topic', None),
#                                   'question_id':str(element['_id']),
#                                   'meta_question_id':str(element.get('meta_question_id',"")),
#                                   'user_id':str(element['user_id']),
#                                   'picked_up_by_id':str(element.get('picked_up_by_id',"")),
#                                   'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
#                                   'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
#                                   'intent_id':str(element.get('intent_id',"")),
#                                   'sub_intent_id':str(element.get('sub_intent_id',"")),
#                                   'time_picked_up':str(element['time_picked_up']+timedelta(hours=7))[0:19],
#                                   'journal_id':str(element.get('journal_id')),
#                                   'date_picked_up':str(element['time_picked_up'])[0:10],
#                                   'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
#                                   'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
#                                   'is_has_conclusion':element.get('is_has_conclusion', None),
#                                   'no_conclusion':element.get('no_conclusion', None)})
#
#             elif element.get('_type', None) == "Core::MetaQuestion":
#                 data.append({'topic':element.get('topic', None),
#                                   'question_id':None,
#                                   'meta_question_id':str(element['_id']),
#                                   'user_id':str(element['user_id']),
#                                   'picked_up_by_id':str(element.get('picked_up_by_id',"")),
#                                   'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
#                                   'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
#                                   'intent_id':str(element.get('intent_id',"")),
#                                   'sub_intent_id':str(element.get('sub_intent_id',"")),
#                                   'time_picked_up':str(element['time_picked_up']+timedelta(hours=7))[0:19],
#                                   'journal_id':str(element.get('journal_id')),
#                                   'date_picked_up':str(element['time_picked_up'])[0:10],
#                                   'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
#                                   'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
#                                   'is_has_conclusion':element.get('is_has_conclusion', None),
#                                   'no_conclusion':element.get('no_conclusion', None)})
#         return data
#
# class GetTermsFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         terms = []
#         term_query = db.terms.find({'name':element['topic']}, no_cursor_timeout=True)
#         if element.get('time_picked_up') is not None:
#             for term in term_query:
#                 if element.get('journal_id', None) is not None:
#                     terms.append({'topic_id':str(term['_id']),
#                                   'question_id':element['question_id'],
#                                   'meta_question_id':element['meta_question_id'],
#                                   'user_id':element['user_id'],
#                                   'picked_up_by_id':element['picked_up_by_id'],
#                                   'created_at':element['created_at'],
#                                   'updated_at':element['updated_at'],
#                                   'intent_id':element['intent_id'],
#                                   'sub_intent_id':element['sub_intent_id'],
#                                   'time_picked_up':element['time_picked_up'],
#                                   'journal_id':element['journal_id'],
#                                   'date_picked_up':element['date_picked_up'],
#                                   'first_user_answer_time':element['first_user_answer_time'],
#                                   'first_doctor_answer_time':element['first_doctor_answer_time'],
#                                   'is_has_conclusion':element['is_has_conclusion'],
#                                   'no_conclusion':element['no_conclusion']})
#                 elif element.get('journal_id', None) is None:
#                     terms.append({'topic_id':str(term['_id']),
#                                   'question_id':element['question_id'],
#                                   'meta_question_id':element['meta_question_id'],
#                                   'user_id':element['user_id'],
#                                   'picked_up_by_id':element['picked_up_by_id'],
#                                   'created_at':element['created_at'],
#                                   'updated_at':element['updated_at'],
#                                   'intent_id':element['intent_id'],
#                                   'sub_intent_id':element['sub_intent_id'],
#                                   'time_picked_up':element['time_picked_up'],
#                                   'journal_id':None,
#                                   'date_picked_up':element['date_picked_up'],
#                                   'first_doctor_answer_time':element['first_doctor_answer_time'],
#                                   'is_has_conclusion':element['is_has_conclusion'],
#                                   'no_conclusion':element['no_conclusion']})
#         return terms
#
# class GetConclusionFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         conclusions = []
#         if element.get('is_has_conclusion') is True:
#             conclusion_query = db.question_conclusions.find({'question_id':ObjectId(element['question_id'])})
#             for conclusion in conclusion_query:
#                 conclusion_id = str(conclusion['_id'])
#         else: conclusion_id = None
#         conclusions.append({'conclusion_id':conclusion_id,
#                         'topic_id':element['topic_id'],
#                         'question_id':element['question_id'],
#                         'meta_question_id':element['meta_question_id'],
#                         'user_id':element['user_id'],
#                         'picked_up_by_id':element['picked_up_by_id'],
#                         'created_at':element['created_at'],
#                         'updated_at':element['updated_at'],
#                         'intent_id':element['intent_id'],
#                         'sub_intent_id':element['sub_intent_id'],
#                         'time_picked_up':element['time_picked_up'],
#                         'journal_id':element['journal_id'],
#                         'date_picked_up':element['date_picked_up'],
#                         'first_user_answer_time':element['first_user_answer_time'],
#                         'first_doctor_answer_time':element['first_doctor_answer_time'],
#                         'is_has_conclusion':element['is_has_conclusion'],
#                         'no_conclusion':element['no_conclusion']})
#         return conclusions
#
# class GetPaymentFn(beam.DoFn):
#     def process(self, element):
#         client = MongoClient(connection_string)
#         db = client.alomobile
#         journals = []
#         if element['journal_id'] != 'None' :
#             journal_query = db.journals.find({'_id':ObjectId(element['journal_id'])})
#             for journal in journal_query:
#                 journal_id = element['journal_id']
#                 payment_method_id = str(journal['payment_method_id'])
#         else:
#             journal_id = None
#             payment_method_id = None
#         journals.append({'id':str(uuid.uuid4()),
#                          # 'conclusion_id':element['conclusion_id'],
#                          'topic_id':element['topic_id'],
#                          'question_id':element['question_id'],
#                          'meta_question_id':element['meta_question_id'],
#                          'user_id':element['user_id'],
#                          'picked_up_by_id':element['picked_up_by_id'],
#                          'created_at':element['created_at'],
#                          'updated_at':element['updated_at'],
#                          'intent_id':element['intent_id'],
#                          'sub_intent_id':element['sub_intent_id'],
#                          'time_picked_up':element['time_picked_up'],
#                          'journal_id':journal_id,
#                          'payment_method_id':payment_method_id,
#                          'date_picked_up':element['date_picked_up'],
#                          'first_user_answer_time':element['first_user_answer_time'],
#                          'first_doctor_answer_time':element['first_doctor_answer_time'],
#                          'is_has_conclusion':element['is_has_conclusion'],
#                          'no_conclusion':element['no_conclusion']})
#         return journals

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
#     pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])
# def run():
#     options = PipelineOptions()
#     google_cloud_options = options.view_as(GoogleCloudOptions)
#     google_cloud_options.project = 'plenary-justice-151004'
#     google_cloud_options.job_name = 'sourcemongo'
#     google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
#     google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
#     google_cloud_options.region = 'asia-east1'
#     options.view_as(StandardOptions).runner = 'DirectRunner'
#     options.view_as(SetupOptions).save_main_session = True

# with beam.Pipeline(runner = 'DirectRunner') as pipeline_fact_questionsc5:
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc5:
    #     (pipeline_fact_questionsc5
    #        | 'read_FQcC_05' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 11, 1)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typecQ5' >> beam.ParDo(if_meta_question())
    #        | 'get_termscQ5' >> beam.ParDo(GetTermsFn())
    #       # # | 'get_conclusionsQ5' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodcQ5' >> beam.ParDo(GetPaymentFn())
    #        # | 'DebugQ5' >> beam.Map(debug_function)
    #        | 'writeFQ5TocBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc5'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_doctor_answer_time:STRING, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )

    # with beam.Pipeline(options = options) as pipeline_fact_questionsc5:
    #     (pipeline_fact_questionsc5
    #       | 'read_FQcB_05' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 10, 16),'$lte':datetime(2018, 10, 31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ5' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ5' >> beam.ParDo(GetTermsFn())
    #        ## | 'get_conclusionsQ5' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ5' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ5' >> beam.Map(debug_function)
    #        | 'writeFQc5ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc5'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )

    # with beam.Pipeline(options = options) as pipeline_fact_questionsc5:
    #     (pipeline_fact_questionsc5
    #       | 'read_FQcA_05' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 10, 1),'$lte':datetime(2018, 10, 15)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ5' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ5' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ5' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ5' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ5' >> beam.Map(debug_function)
    #        | 'writeFQc5ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc5'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc4:
    #     (pipeline_fact_questionsc4
    #        | 'read_factquestionsc04' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 7, 1),'$lte':datetime(2018, 9, 30)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ4' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ4' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ4' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ4' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ4' >> beam.Map(debug_function)
    #        | 'writeFQc4ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc4'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #        )

    # with beam.Pipeline(options = options) as pipeline_fact_questions3:
    #     (pipeline_fact_questions3
    #        | 'read_fquestions03_3b' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 6, 16),'$lte':datetime(2018, 6, 30)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ3' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
    #       # | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ3' >> beam.Map(debug_function)
    #        | 'writeFQ3ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc3'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_user_answer_time:STRING, first_doctor_answer_time:STRING, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #       )

    # with beam.Pipeline(options = options) as pipeline_fact_questions3:
    #     (pipeline_fact_questions3
    #        | 'read_fquestions03_3a' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 6, 1),'$lte':datetime(2018, 6, 15)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ3' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
    #       # | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ3' >> beam.Map(debug_function)
    #        | 'writeFQ3ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc3'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_user_answer_time:STRING, first_doctor_answer_time:STRING, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #       )

    # with beam.Pipeline(options = options) as pipeline_fact_questions3:
    #     (pipeline_fact_questions3
    #        | 'read_fquestions03_2c' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 5, 21),'$lte':datetime(2018, 5, 31)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ3' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
    #       # | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ3' >> beam.Map(debug_function)
    #        | 'writeFQ3ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc3'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_user_answer_time:STRING, first_doctor_answer_time:STRING, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #       )
    # with beam.Pipeline(options = options) as pipeline_fact_questions3:
    #     (pipeline_fact_questions3
    #        | 'read_fquestions03_2b' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018,5,11),'$lte':datetime(2018, 5, 20)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ3' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
    #       # | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ3' >> beam.Map(debug_function)
    #        | 'writeFQ3ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc3'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_user_answer_time:STRING, first_doctor_answer_time:STRING, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #       )
    # with beam.Pipeline(options = options) as pipeline_fact_questions3:
    #     (pipeline_fact_questions3
    #        | 'read_fquestions03_2a' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 5, 1),'$lte':datetime(2018, 5, 10)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ3' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
    #       # | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ3' >> beam.Map(debug_function)
    #        | 'writeFQ3ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc3'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_user_answer_time:STRING, first_doctor_answer_time:STRING, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #       )

    # with beam.Pipeline(options = options) as pipeline_fact_questions3:
    #     (pipeline_fact_questions3
    #        | 'read_fquestions03_1c' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 4,20),'$lte':datetime(2018, 4, 30)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ3' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
    #       # | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ3' >> beam.Map(debug_function)
    #        | 'writeFQ3ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc3'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_user_answer_time:STRING, first_doctor_answer_time:STRING, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #       )
    # with beam.Pipeline(options = options) as pipeline_fact_questions3:
    #     (pipeline_fact_questions3
    #        | 'read_fquestions03_1b' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 4,11),'$lte':datetime(2018, 4, 19)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ3' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
    #       # | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ3' >> beam.Map(debug_function)
    #        | 'writeFQ3ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc3'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_user_answer_time:STRING, first_doctor_answer_time:STRING, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #       )
    # with beam.Pipeline(options = options) as pipeline_fact_questions3:
    #     (pipeline_fact_questions3
    #        | 'read_fquestions03_1a' >> ReadFromMongo(connection_string, 'alomobile', 'questions', query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 4, 1),'$lte':datetime(2018, 4, 9)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ3' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ3' >> beam.ParDo(GetTermsFn())
    #       # | 'get_conclusionsQ3' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ3' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ3' >> beam.Map(debug_function)
    #        | 'writeFQ3ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc3'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, date_picked_up:DATE, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_user_answer_time:STRING, first_doctor_answer_time:STRING, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #       )

    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_3c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 3, 21),'$lte':datetime(2018, 3, 30)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_3b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 3,11),'$lte':datetime(2018, 3, 20)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_3a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 3,1),'$lte':datetime(2018, 3, 10)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )

    #
    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_2c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 2, 20),'$lte':datetime(2018, 2, 28)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_2b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 2, 10),'$lte':datetime(2018, 2, 19)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_2a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 2, 1),'$lte':datetime(2018, 2, 9)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )

    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_1c' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 1, 21),'$lte':datetime(2018, 1, 31)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_1b' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 1, 11),'$lte':datetime(2018, 1, 20)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questions2:
    #     (pipeline_fact_questions2
    #        | 'read_FQc2_1a' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2018, 1, 1),'$lte':datetime(2018, 1, 10)} } ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ2' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ2' >> beam.ParDo(GetTermsFn())
    #      #  | 'get_conclusionsQ2' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ2' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ2' >> beam.Map(debug_function)
    #        | 'writeFQ2ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc2'),
    #                       schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )

    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_12c_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 12, 21),'$lte':datetime(2017, 12, 31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_12b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 12, 11),'$lte':datetime(2017, 12, 20)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_12a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017,12,1),'$lte':datetime(2017, 12, 10)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )

    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_11c_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 11, 21),'$lte':datetime(2017, 11, 30)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_11b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 11, 11),'$lte':datetime(2017, 11, 20)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_11a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 11, 1),'$lte':datetime(2017, 11, 10)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_10c_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 10, 21),'$lte':datetime(2017, 10, 31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionsc1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_10b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 10, 11),'$lte':datetime(2017, 10, 20)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time',
    #                                 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # #      | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_10a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 10, 1),'$lte':datetime(2017, 10, 10)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time',
    #                             'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_9c_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 9,21),'$lte':datetime(2017, 9, 30)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_9b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 9, 11),'$lte':datetime(2017, 9, 20)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_9a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 9, 1),'$lte':datetime(2017, 9, 10)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_8c_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 8,21),'$lte':datetime(2017, 8, 31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_8b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 8,11),'$lte':datetime(2017,8,20)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_8a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 8,1),'$lte':datetime(2017, 8,10)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_7c_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017,7,21),'$lte':datetime(2017, 7, 31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_7b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017,7,11),'$lte':datetime(2017,7,20)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_7a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 7, 1),'$lte':datetime(2017, 7,10)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_6b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 6, 16),'$lte':datetime(2017, 6, 30)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_6a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 6, 1),'$lte':datetime(2017, 6,15)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )

    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
    #        | 'read_FQ1_5b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                               query={'is_closed':True, 'created_at': {'$gte':datetime(2017,5,16),'$lte':datetime(2017, 5,31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
        (pipeline_fact_questionsc1
           | 'read_FQ1_5a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                  query={'is_closed':True, 'time_picked_up':None} ,
                                  fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
           | 'typeQ1' >> beam.ParDo(if_meta_question())
           # | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
           # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
           | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # #        | 'DebugQ1' >> beam.Map(debug_function)
           | 'writeFQc1ToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestions'),
                        schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
           )
######################################################################################################################
#run using batching_dim_questions IP200

    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_4b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                  # query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 4,17),'$lte':datetime(2017, 4, 30)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_4a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                  # query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 4, 1),'$lte':datetime(2017, 4,16)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_3b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                  # query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 3,16),'$lte':datetime(2017, 3, 31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_3a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                  # query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 3, 1),'$lte':datetime(2017, 3,15)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_2b_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    # query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 2,15),'$lte':datetime(2017, 2, 28)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_2a_2017' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    # query={'is_closed':True, 'created_at': {'$gte':datetime(2017, 2, 1),'$lte':datetime(2017, 2, 14)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_12_2016' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    # query={'is_closed':True, 'created_at': {'$gte':datetime(2016,12, 1), '$lte':datetime(2016 12,31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_11_2016' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    # query={'is_closed':True, 'created_at': {'$gte':datetime(2016,11, 1), '$lte':datetime(2016 11,30)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_9&10_2016' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    # query={'is_closed':True, 'created_at': {'$gte':datetime(2016,9, 1), '$lte':datetime(2016 10,31)}} ,
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscK1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
    # with beam.Pipeline(options = options) as pipeline_fact_questionsc1:
    #     (pipeline_fact_questionsc1
           # | 'read_FQ1_lte8_2016' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                    # query={'is_closed':True, 'created_at': {'$lte':datetime(2016,8,31)}},
    #                             fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic', 'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
    #        | 'typeQ1' >> beam.ParDo(if_meta_question())
    #        | 'get_termsQ1' >> beam.ParDo(GetTermsFn())
    #        # | 'get_conclusionsQ1' >> beam.ParDo(GetConclusionFn())
    #        | 'get_paymethodQ1' >> beam.ParDo(GetPaymentFn())
    # # # #        | 'DebugQ1' >> beam.Map(debug_function)
    #        | 'writeFQc1ToBQ' >> beam.io.Write(
    #                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestionscKT1'),
    #                     schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, first_doctor_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
    #                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #        )
