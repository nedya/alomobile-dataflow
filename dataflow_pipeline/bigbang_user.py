import apache_beam as beam
from datetime import datetime, timedelta
import itertools
from apache_beam.transforms import PTransform, ParDo, DoFn, Create
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime, timedelta
import uuid, math
import re


def debug_function(pcollection_as_list):
    print (pcollection_as_list)
connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"
scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)


#dimensi users
class GetCitiesUsersFn(beam.DoFn):
    def process(self, element):
        if element.get('birthday', None):
            birth_year = int(element['birthday'][-4:])
        else: birth_year = None

        if (element['provider']):
            registration_channel = element['provider']
        else: registration_channel = "email"

        if element.get('city_id', None) is not None:
            client = MongoClient(connection_string)
            db = client.alomobile
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])}, no_cursor_timeout=True)
            for city in city_query:
                city_name = city['name']
        else: city_name = None

        data = []
        data.append({'id':str(element['_id']),
                     'fullname':element['firstname'] + " " + element['lastname'],
                     'city':city_name,
                     'email':element.get('email', None),
                     'created_at':str(element['created_at'] +timedelta(hours=7))[0:19],
                     'gender':element.get('gender',""),
                     'birthdate':element.get('birthday', ""),
                     'birthyear': birth_year,
                     'registration_channel':registration_channel,
                     'version':element.get('version',""),
                     'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19]})
        return data

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])

#bulan 11/2018
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsersbulan11'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,11, 1)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','usersbln11'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #       )

 # bulan 10/2018
    # with beam.Pipeline(options = options) as pipeline_users:
    #     (pipeline_users
    #         | 'ReadUsersbulan10'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,10, 1),  '$lte':datetime(2018,10,31)}, "_type" : "Core::User"},
    #                                        fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
    #                                        'city_id', 'provider', 'version', 'updated_at'])
    #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
    #         #         |'DebugUsers' >> beam.Map(debug_function)
    #         | 'writeUserToBQ' >> beam.io.Write(
    #                 beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','usersbulan10'),
    #                 schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
    #                 create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                 write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )
#bulan 8 - 10/2018
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsersbulan11'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,08, 1),  '$lte':datetime(2018,10,31)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','usersbln10'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #       )
#bulan 7/2018
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsersBulan7'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,7,1), '$lte':datetime(2018, 7, 31)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','usersbln7'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #         )
#bulan 6/2018
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsersBulan6'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,6,1), '$lte':datetime(2018,6,30)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','usersbln6'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #         )

#bulan 4-5/2018
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsersBulan4_5'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,4,1), '$lte':datetime(2018,5,31)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'GetcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         |'WriteUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','usersbln4_5'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #        )
#bulan 3/2018
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsersBulan3'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,3,1), '$lte':datetime(2018,3,31)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'GetcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         |'WriteUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','usersbln3'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #         )
#bulan 1-2/2018
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsersBulan1_2'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2018,1,1), '$lte':datetime(2018,2,28)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'GetcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         |'WriteUserToBQ' >> beam.io.Write(
                       # beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','usersbln1_2'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #         )
#bulan 11-12/2017
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsersbln11sd12_2017'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2017,11,1), '$lte':datetime(2017,12,31)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users11sd12_17'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #       )

# bulan 9-10/2017
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsers9sd10_2017'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2017,9,1), '$lte':datetime(2017,10,31)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users9sd10_17'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #       )

# bulan 7-8/2017
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsers7sd8_2017'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2017,7,1), '$lte':datetime(2017,8,31)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users7sd8_2017'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
 #       )

# bulan 5-6/2017 append to table users7sd8_2017
 #    with beam.Pipeline(options = options) as pipeline_users:
 #        (pipeline_users
 #         | 'ReadUsers5sd6_2017'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2017,5,1), '$lte':datetime(2017,6,30)}, "_type" : "Core::User"},
 #                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
 #                                          'city_id', 'provider', 'version', 'updated_at'])
 #         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
 # #         |'DebugUsers' >> beam.Map(debug_function)
 #         | 'writeUserToBQ' >> beam.io.Write(
 #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users7sd8_2017'),
 #                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
 #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
 #                       write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
 #       )

# #bulan 3-4/2017 append to table users7sd8_2017
#     with beam.Pipeline(options = options) as pipeline_users:
#         (pipeline_users
#          | 'ReadUsers3sd4_2017'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2017,3,1), '$lte':datetime(2017,4,30)}, "_type" : "Core::User"},
#                                           fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
#                                           'city_id', 'provider', 'version', 'updated_at'])
#          |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
#     #         |'DebugUsers' >> beam.Map(debug_function)
#          | 'writeUserToBQ' >> beam.io.Write(
#                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users7sd8_2017'),
#                        schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
#                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
#        )

#bulan 1-2/2017 append to table users7sd8_2017
    # with beam.Pipeline(options = options) as pipeline_users:
    #     (pipeline_users
    #      | 'ReadUsers1sd2_2017'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2017,1,1), '$lte':datetime(2017,2,28)}, "_type" : "Core::User"},
    #                                       fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
    #                                       'city_id', 'provider', 'version', 'updated_at'])
    #      |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
    # #         |'DebugUsers' >> beam.Map(debug_function)
    #      | 'writeUserToBQ' >> beam.io.Write(
    #                    beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users7sd8_2017'),
    #                    schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
    #                    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #    )

# #bulan 11-12/2016 append to table users2016
    # with beam.Pipeline(options = options) as pipeline_users:
    #     (pipeline_users
    #      | 'ReadUsers11sd12_2016'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2016,11,1), '$lte':datetime(2016,12,31)}, "_type" : "Core::User"},
    #                                       fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
    #                                       'city_id', 'provider', 'version', 'updated_at'])
    #      |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
    # #         |'DebugUsers' >> beam.Map(debug_function)
    #      | 'writeUserToBQ' >> beam.io.Write(
    #                    beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users2016'),
    #                    schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
    #                    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #    )
# #bulan 9-10/2016 append to table users2016
    # with beam.Pipeline(options = options) as pipeline_users:
    #     (pipeline_users
    #      | 'ReadUsers9sd10_2016'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$gte':datetime(2016,9,1), '$lte':datetime(2016,10,31)}, "_type" : "Core::User"},
    #                                       fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
    #                                       'city_id', 'provider', 'version', 'updated_at'])
    #      |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
    # #         |'DebugUsers' >> beam.Map(debug_function)
    #      | 'writeUserToBQ' >> beam.io.Write(
    #                    beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users2016'),
    #                    schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
    #                    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #    )
# # lte bulan 8/2016 append to table users2016
    with beam.Pipeline(options = options) as pipeline_users:
        (pipeline_users
         | 'ReadUserslte8_2016'>> ReadFromMongo(connection_string, 'alomobile', 'users', query={'created_at':{'$lte':datetime(2016,8,31)}, "_type" : "Core::User"},
                                          fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
                                          'city_id', 'provider', 'version', 'updated_at'])
         |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
    #         |'DebugUsers' >> beam.Map(debug_function)
         | 'writeUserToBQ' >> beam.io.Write(
                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users2016'),
                       schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                       write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
       )
