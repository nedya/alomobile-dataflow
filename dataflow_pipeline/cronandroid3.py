
# coding: utf-8

# In[1]:

import apache_beam as beam
import re
import uuid, math
import itertools
from datetime import datetime, timedelta
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from google.oauth2.service_account import Credentials
from bson.objectid import ObjectId
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from google.cloud import bigquery
def debug_function(pcollection_as_list):
    print (pcollection_as_list)

#connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.14/alomobile"

scopes = (
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/drive'
)

# table_names = {'fact_users-cr':None, 'fact_magazine-cr':None, 'fact_video_campaign-cr':None, 'fact_campaign-cr':None, 'factquestions-cr':None}
# from google.cloud import bigquery
# client = bigquery.Client()
# dataset_ref = client.dataset('alobrain_data_staging')
# for key,val in table_names.items():
#     data = key.split("-")
#     #field default : updated_at
#     field = 'updated_at'
#     if (data[1] == 'cr'):
#         field = 'created_at'
#     elif (data[1] == 'join'):
#         field = 'join_at'
#     sql = "SELECT MAX ("+ field +") FROM [plenary-justice-151004.alowarehouse_alodoktermobile."+data[0]+"]"
#     query = client.run_sync_query(sql)
#     query.run()
#     table_names[key] = query.rows[0][0]

#dimensi_doctor_specialities
class UnlistDoctorSpecialitiesIDFn(beam.DoFn):
    def process(self, element):
        speciality_ids = element.get('speciality_ids', [])
        speciality_query = []
        for data in speciality_ids:
            speciality_query.append({'doctor_id':element['_id'],
                                     'speciality_ids':data})
        return speciality_query

class GetDoctorSpecialityIDFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        specialities = db.specialities
        speciality_list = []
        for data in specialities.find({'_id':element['speciality_ids']}):
                speciality_list.append({'id':str(uuid.uuid4()),
                                        'doctor_id':str(element['doctor_id']),
                                        'speciality_id':str(data['_id']),
                                        'is_deleted':False})
        return speciality_list

#fact_questions
class if_meta_question(beam.DoFn):
    def process(self, element):
        data = []
        if element.get('time_picked_up') is not None:
            time_picked_up = str(element['time_picked_up']+timedelta(hours=7))[0:19]
        else:
            time_picked_up = None

        if element.get('_type') == "Core::Question" or element.get('_type') == "Core::PreQuestion":
            data.append({'topic':element.get('topic', None),
                        'question_id':str(element['_id']),
                        'meta_question_id':str(element.get('meta_question_id',"")),
                        'user_id':str(element['user_id']),
                        'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                        'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                        'intent_id':str(element.get('intent_id',"")),
                        'sub_intent_id':str(element.get('sub_intent_id',"")),
                        'time_picked_up':time_picked_up,
                        'journal_id':str(element.get('journal_id')),
                        'date_picked_up':str(time_picked_up)[0:10],
                        'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
                        'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
                        'is_has_conclusion':element.get('is_has_conclusion', None),
                        'no_conclusion':element.get('no_conclusion', None)})

        elif element.get('_type') == "Core::MetaQuestion":
            data.append({'topic':element.get('topic', None),
                        'question_id':None,
                        'meta_question_id':str(element['_id']),
                        'user_id':str(element['user_id']),
                        'picked_up_by_id':str(element.get('picked_up_by_id',"")),
                        'created_at':str(element['created_at']+timedelta(hours=7))[0:19],
                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                        'intent_id':str(element.get('intent_id',"")),
                        'sub_intent_id':str(element.get('sub_intent_id',"")),
                        'time_picked_up':time_picked_up,
                        'journal_id':str(element.get('journal_id')),
                        'date_picked_up':str(time_picked_up)[0:10],
                        'first_user_answer_time':str(element.get('first_user_answer_time', ""))[0:19],
                        'first_doctor_answer_time':str(element.get('first_doctor_answer_time', ""))[0:19],
                        'is_has_conclusion':element.get('is_has_conclusion', None),
                        'no_conclusion':element.get('no_conclusion', None)})
        return data

class GetTermsFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        terms = []
        term_query = db.terms.find({'name':element['topic']}, no_cursor_timeout=True)
        if element.get('time_picked_up') is not None:
            time_picked_up = element['time_picked_up']
        else:
            time_picked_up = None


        for term in term_query:
                terms.append({'topic_id':str(term['_id']),
                                  'question_id':element['question_id'],
                                  'meta_question_id':element['meta_question_id'],
                                  'user_id':element['user_id'],
                                  'picked_up_by_id':element['picked_up_by_id'],
                                  'created_at':element['created_at'],
                                  'updated_at':element['updated_at'],
                                  'intent_id':element['intent_id'],
                                  'sub_intent_id':element['sub_intent_id'],
                                  'time_picked_up':time_picked_up,
                                  'journal_id':journal_id,
                                  'date_picked_up':element['date_picked_up'],
                                  'first_user_answer_time':element['first_user_answer_time'],
                                  'first_doctor_answer_time':element['first_doctor_answer_time'],
                                  'is_has_conclusion':element['is_has_conclusion'],
                                  'no_conclusion':element['no_conclusion']})
        return terms

class GetConclusionFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        conclusions = []
        if element.get('is_has_conclusion') is True:
            conclusion_query = db.question_conclusions.find({'question_id':ObjectId(element['question_id'])})
            for conclusion in conclusion_query:
                conclusion_id = str(conclusion['_id'])
        else: conclusion_id = None
        conclusions.append({'conclusion_id':conclusion_id,
                        'topic_id':element['topic_id'],
                        'question_id':element['question_id'],
                        'meta_question_id':element['meta_question_id'],
                        'user_id':element['user_id'],
                        'picked_up_by_id':element['picked_up_by_id'],
                        'created_at':element['created_at'],
                        'updated_at':element['updated_at'],
                        'intent_id':element['intent_id'],
                        'sub_intent_id':element['sub_intent_id'],
                        'time_picked_up':element['time_picked_up'],
                        'journal_id':element['journal_id'],
                        'date_picked_up':element['date_picked_up'],
                        'first_user_answer_time':element['first_user_answer_time'],
                        'first_doctor_answer_time':element['first_doctor_answer_time'],
                        'is_has_conclusion':element['is_has_conclusion'],
                        'no_conclusion':element['no_conclusion']})
        return conclusions

class GetPaymentFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        journals = []
        if element['journal_id'] != 'None' :
            journal_query = db.journals.find({'_id':ObjectId(element['journal_id'])})
            for journal in journal_query:
                journal_id = element['journal_id']
                payment_method_id = str(journal['payment_method_id'])
        else:
            journal_id = None
            payment_method_id = None
        journals.append({'id':str(uuid.uuid4()),
                         # 'conclusion_id':element['conclusion_id'],
                         'topic_id':element['topic_id'],
                         'question_id':element['question_id'],
                         'meta_question_id':element['meta_question_id'],
                         'user_id':element['user_id'],
                         'picked_up_by_id':element['picked_up_by_id'],
                         'created_at':element['created_at'],
                         'updated_at':element['updated_at'],
                         'intent_id':element['intent_id'],
                         'sub_intent_id':element['sub_intent_id'],
                         'time_picked_up':element['time_picked_up'],
                         'journal_id':journal_id,
                         'payment_method_id':payment_method_id,
                         'date_picked_up':element['date_picked_up'],
                         'first_user_answer_time':element['first_user_answer_time'],
                         'first_doctor_answer_time':element['first_doctor_answer_time'],
                         'is_has_conclusion':element['is_has_conclusion'],
                         'no_conclusion':element['no_conclusion']})
        return journals

#dimensi questions
def join_listsQ((k, v)):
    return itertools.product(v['number_of_chatbox'], v['questions'])

def mergedictsQ(questions_dict_chatbox):
    (chatbox, questions_dict) = questions_dict_chatbox
    a_dict = questions_dict
    a_dict.update(chatbox)
    return a_dict

class GetPriceQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        prices = []
        if element.get('journal_id', None) is not None:
                price_query = db.journals.find({'_id':element['journal_id']})
                for price in price_query:
                    if price.get('gross_amount', None) is not None:
                        prices.append({'id':str(element['_id']),
                                        'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                                        'price':int(price.get('gross_amount', None)),
                                        'pickup_fee':4000, 'like_fee':1000,
                                        'title':element['title'],
                                        'content':element['content'],
                                        'is_said_thanks':element['is_said_thanks'],
                                        'is_paid':element.get('is_paid'),
                                        'is_closed':element['is_closed'],
                                        'type':element['_type'],
                                        'sub_intent_id':element.get('sub_intent_id', None),
                                        'intent_id':element.get('intent_id', None),
                                        'is_has_conclusion':element.get('is_has_conclusion', None),
                                        'is_shown':element.get('is_shown', None)})
                    elif price.get('gross_amount', None) is None:
                        prices.append({'id':str(element['_id']),
                                       'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                                       'price':None, 'pickup_fee':4000,
                                       'like_fee':1000,
                                       'title':element['title'],
                                       'content':element['content'],
                                       'is_said_thanks':element['is_said_thanks'],
                                       'is_paid':None,
                                       'is_closed':element['is_closed'],
                                       'type':element['_type'],
                                       'sub_intent_id':element.get('sub_intent_id', None),
                                       'intent_id':element.get('intent_id', None),
                                       'is_has_conclusion':element.get('is_has_conclusion', None),
                                       'is_shown':element.get('is_shown', None)})
        elif element.get('journal_id', None) is None:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        else:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        return prices


def is_chatbotQ(q):
       if q['intent_id'] or q['sub_intent_id'] is not None:
           is_chatbot = {'is_chatbot':True}
       elif q['intent_id'] and q['sub_intent_id'] is None:
           is_chatbot = {'is_chatbot':False}
       else:
           is_chatbot = {'is_chatbot':None}

       if q['type'] == "Core::PreQuestion":
           autochat_close = {'autochat_close':True}
       elif q['type'] == "Core::Question":
           autochat_close = {'autochat_close':False}
       else:
           autochat_close = {'autochat_close':None}

       q.update(is_chatbot)
       q.update(autochat_close)
       q.pop('intent_id')
       q.pop('sub_intent_id')
       return q

class GetChatBoxQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        chatboxes = []
        number_of_chatbox = db.answers.find({'question_id':ObjectId(element['id'])}, no_cursor_timeout=True).count()
        chatboxes.append({'id':str(element['id']),
                          'updated_at':element['updated_at'],
                          'number_of_chatbox':number_of_chatbox,
                          'price':element['price'],
                          'pickup_fee':element['pickup_fee'],
                          'like_fee':element['like_fee'],
                          'title':element['title'],
                          'content':element['content'],
                          'is_said_thanks':element['is_said_thanks'],
                          'is_paid':element['is_paid'],
                          'is_closed':element['is_closed'],
                          'type':element['type'],
                          'is_has_conclusion':element.get('is_has_conclusion', None),
                          'is_shown':element.get('is_shown', None)})
        return chatboxes

#dimensi users
class GetCitiesUsersFn(beam.DoFn):
    def process(self, element):
        if element.get('birthday', None):
            birth_year = int(element['birthday'][-4:])
        else: birth_year = None

        if (element['provider']):
            registration_channel = element['provider']
        else: registration_channel = "email"

        if element.get('city_id', None) is not None:
            client = MongoClient(connection_string)
            db = client.alomobile
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])}, no_cursor_timeout=True)
            for city in city_query:
                city_name = city['name']
        else: city_name = None

        data = []
        data.append({'id':str(element['_id']),
                     'fullname':element['firstname'] + " " + element['lastname'],
                     'city':city_name,
                     'email':element.get('email', None),
                     'created_at':str(element['created_at'] +timedelta(hours=7))[0:19],
                     'gender':element.get('gender',""),
                     'birthdate':element.get('birthday', ""),
                     'birthyear': birth_year,
                     'registration_channel':registration_channel,
                     'version':element.get('version',""),
                     'updated_at':str(element['updated_at']+timedelta(hours=7))[0:19]})
        return data


def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]

    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'

#    pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])

# #32
    with beam.Pipeline(options = options) as p_doctor_specialities:
        (p_doctor_specialities
            |'ReadDoctSpecMob' >> ReadFromMongo(connection_string, 'alomobile', 'users',
                                    query={'_type':'Core::Doctor'}, fields=['_id', 'speciality_ids'])
            |'UnlistSpecialitiesID'>>beam.ParDo(UnlistDoctorSpecialitiesIDFn())
            |'GetSpecialityID'>>beam.ParDo(GetDoctorSpecialityIDFn())
            # |'Debugdoctorspecialities' >> beam.Map(debug_function)
            | 'writedoctorspecialitiesToBQ' >> beam.io.Write(
                  beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','doctor_specialities'),
                  schema='id:STRING, doctor_id:STRING, speciality_id:STRING, is_deleted:BOOLEAN',
                  create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                  write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

# #33
    with beam.Pipeline(options = options) as p_questions:
        (p_questions
            | 'ReadQuestionsMob' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                # query={'updated_at':{'$gt':table_names['questions-upd'] - timedelta(hours = 7)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
                query={'updated_at':{'$gt':datetime.now()-timedelta(hours = 7)}, 'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
                fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
                'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
            | 'get_price' >> beam.ParDo(GetPriceQFn())
            | 'is_chatbot' >> beam.Map(is_chatbotQ)
            | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
            # | 'DebugQ' >> beam.Map(debug_function)
            | 'writeQToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions'),
                     schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )
#
# #34
    with beam.Pipeline(options = options) as p_users:
        (p_users
             |'ReadUsersMob'>> ReadFromMongo(connection_string, 'alomobile', 'users',
                                              #query={"_type" : "Core::User",'updated_at':{'$gt':table_names['users-upd'] - timedelta(hours = 7)}},
                                              query={'_type' : 'Core::User','updated_at':{'$gt':datetime.now() - timedelta(hours = 7)}},
                                              fields=['_id', 'firstname', 'lastname', 'created_at', 'email', 'gender', 'birthday',
                                              'city_id', 'provider', 'version', 'updated_at'])
             |'getcityUsers' >> beam.ParDo(GetCitiesUsersFn())
             # |'Debug' >> beam.Map(debug_function)
             | 'writeUserToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','users'),
                        schema='id:STRING, fullname:STRING, created_at:DATETIME, updated_at:DATETIME, city:STRING, email:STRING, gender:STRING, birthdate:STRING, birthyear:INTEGER, registration_channel:STRING, version:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )
#
# # #35
    with beam.Pipeline(options = options) as p_factquestions:
        (p_factquestions
           | 'ReadFQMob' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
                                # query={'is_closed':True, 'created_at':{'$gt':table_names['factquestions-cr'] - timedelta(hours = 7)}} ,
                                query={'is_closed':True, 'created_at':{'$gt':datetime.now() - timedelta(hours = 7)}} ,
                                fields=['is_has_conclusion', 'no_conclusion', 'first_user_answer_time', 'first_doctor_answer_time', 'meta_question_id', 'user_id', 'topic',
                                'picked_up_by_id', 'created_at', 'updated_at', 'intent_id', 'sub_intent_id', 'time_picked_up', 'journal_id', 'pick_hour', '_type'])
           | 'type' >> beam.ParDo(if_meta_question())
           | 'get_terms' >> beam.ParDo(GetTermsFn())
           # | 'get_conclusionsQ5' >> beam.ParDo(GetConclusionFn())
           | 'get_paymethod' >> beam.ParDo(GetPaymentFn())
           # | 'DebugQ5' >> beam.Map(debug_function)
           | 'writeFQToBQ' >> beam.io.Write(
                        beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','factquestions'),
                        schema='id:STRING, user_id:STRING, topic_id:STRING, created_at:DATETIME, first_user_answer_time:STRING, is_has_conclusion:BOOLEAN, no_conclusion:BOOLEAN, first_doctor_answer_time:STRING, date_picked_up:DATE, updated_at:DATETIME, question_id:STRING, meta_question_id:STRING, intent_id:STRING, sub_intent_id:STRING, time_picked_up:DATETIME, payment_method_id:STRING, picked_up_by_id:STRING, journal_id:STRING',
                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
           )


# if __name__ == '__main__':
#     run()
