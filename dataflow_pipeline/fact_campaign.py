import apache_beam as beam
import uuid
from datetime import datetime, timedelta
from bson.objectid import ObjectId
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from google.oauth2.service_account import Credentials
from mongodbio import ReadFromMongo
from pymongo import MongoClient

connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.169.109:27017/alomobile"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.200:27017/alomobile"

def debug_function(pcollection_as_list):
    print(pcollection_as_list)

class GetTagFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        campaign_query = db.campaigns.find({'_id':element['campaign_id']},{'_id':1,'tags':1})
        for campaign in campaign_query:
            if (campaign.get('tags')): #if campaign tags is exsist
                for campaign_tag in campaign['tags']:
                    term_query = db.terms.find({'name':campaign_tag},{'_id':1})
                    for tag_id in term_query:
                        data.append({'id':element['_id'],
                                    'form_instance_values': element['form_instance_values'],
                                    'tag_id':tag_id['_id'],
                                    'client_id':element['client_id'],
                                    'campaign_id':element['campaign_id'],
                                    'user_id':element['user_id'],
                                    'created_at':element['created_at']+timedelta(hours=7)})
            else:
                data.append ({'id':element['_id'],
                              'form_instance_values': element['form_instance_values'],
                              'tag_id':None,
                              'client_id':element['client_id'],
                              'campaign_id':element['campaign_id'],
                              'user_id':element['user_id'],
                              'created_at':element['created_at']+timedelta(hours=7)})
        return data


class GetQnA(beam.DoFn):
    def process(self, element):
        data = []
        for question in element['form_instance_values']:
            if (question.get('form_value_ids')): #jika pertanyaan memiliki jawaban
                for answer in question['form_value_ids']:
                    if (answer == '5770ef13150cd4744f00000b'):
                        answer = questions['text_value']
                    data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': str(answer),
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
            else: data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': None,
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
        return data

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
#    pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])

    # with beam.Pipeline(options = options) as p_fact_campaign:
    #    (p_fact_campaign
    #         |'ReadFCamp1aMob'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
    # #                                      # query={'created_at':{'$gt':table_names['fact_campaigns-cr'] - timedelta(hours = 7)}},
    #                                      # query={'created_at':{'$gt':datetime(2018,11,5,16,0,30)}},
    #                                      query={'created_at': {'$gt':datetime(2018,11,5,16,0,30), '$lte':datetime(2018,11,17,0,0,0)}},
    #                                      fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
    #         |'GetTagFC' >> beam.ParDo(GetTagFn())
    #         |'GetQnAFC' >> beam.ParDo(GetQnA())
    #         # |'DebugFC' >> beam.Map(debug_function)
    #         | 'writeFCToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaigns'),
    #                   schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #    )

    # with beam.Pipeline(options = options) as p_fact_campaign:
    #    (p_fact_campaign
    #         |'ReadFCamp1bMob'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
    # #                                      # query={'created_at':{'$gt':table_names['fact_campaigns-cr'] - timedelta(hours = 7)}},
    #                                      # query={'created_at':{'$gt':datetime(2018,11,5,16,0,30)}},
                                         # query={'created_at': {'$gt':datetime(2018,11,17,0,0,0), '$lte':datetime(2018,11,30,0,0,0)}},
    #                                      fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
    #         |'GetTagFC' >> beam.ParDo(GetTagFn())
    #         |'GetQnAFC' >> beam.ParDo(GetQnA())
    #         # |'DebugFC' >> beam.Map(debug_function)
    #         | 'writeFCToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaigns'),
    #                   schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #    )

    # with beam.Pipeline(options = options) as p_fact_campaign:
    #    (p_fact_campaign
    #         |'ReadFCamp2aMob'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
    # #                                      # query={'created_at':{'$gt':table_names['fact_campaigns-cr'] - timedelta(hours = 7)}},
    #                                      # query={'created_at':{'$gt':datetime(2018,11,5,16,0,30)}},
                                         # query={'created_at': {'$gt':datetime(2018,12,1,0,0,0), '$lte':datetime(2018, 12, 7,0,0,0)}},
    #                                      fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
    #         |'GetTagFC' >> beam.ParDo(GetTagFn())
    #         |'GetQnAFC' >> beam.ParDo(GetQnA())
    #         # |'DebugFC' >> beam.Map(debug_function)
    #         | 'writeFCToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaigns'),
    #                   schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #    )

    # with beam.Pipeline(options = options) as p_fact_campaign:
    #    (p_fact_campaign
    #         |'ReadFCamp2bMob'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
    # #                                      # query={'created_at':{'$gt':table_names['fact_campaigns-cr'] - timedelta(hours = 7)}},
    #                                      # query={'created_at':{'$gt':datetime(2018,11,5,16,0,30)}},
    #                                      query={'created_at': {'$gt':datetime(2018,12,7,0,0,0), '$lte':datetime(2018, 12, 16,0,0,0)}},
    #                                      fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
    #         |'GetTagFC' >> beam.ParDo(GetTagFn())
    #         |'GetQnAFC' >> beam.ParDo(GetQnA())
    #         # |'DebugFC' >> beam.Map(debug_function)
    #         | 'writeFCToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaigns'),
    #                   schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #    )

    # with beam.Pipeline(options = options) as p_fact_campaign:
    #    (p_fact_campaign
    #         |'ReadFCamp3aMob'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
    # #                                      # query={'created_at':{'$gt':table_names['fact_campaigns-cr'] - timedelta(hours = 7)}},
    #                                      # query={'created_at':{'$gt':datetime(2018,11,5,16,0,30)}},
    #                                      query={'created_at': {'$gt':datetime(2018,12,16,0,0,0), '$lte':datetime(2018, 12, 21,0,0,0)}},
    #                                      fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
    #         |'GetTagFC' >> beam.ParDo(GetTagFn())
    #         |'GetQnAFC' >> beam.ParDo(GetQnA())
    #         # |'DebugFC' >> beam.Map(debug_function)
    #         | 'writeFCToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaigns'),
    #                   schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #    )

    # with beam.Pipeline(options = options) as p_fact_campaign:
    #    (p_fact_campaign
    #         |'ReadFCamp3bMob'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
    # #                                      # query={'created_at':{'$gt':table_names['fact_campaigns-cr'] - timedelta(hours = 7)}},
    #                                      # query={'created_at':{'$gt':datetime(2018,11,5,16,0,30)}},
    #                                      query={'created_at': {'$gt':datetime(2018,12,21,0,0,0)}},
    #                                      fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
    #         |'GetTagFC' >> beam.ParDo(GetTagFn())
    #         |'GetQnAFC' >> beam.ParDo(GetQnA())
    #         # |'DebugFC' >> beam.Map(debug_function)
    #         | 'writeFCToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaigns'),
    #                   schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #   )

#     with beam.Pipeline(options = options) as pipeline_fact_campaign11:
#        (pipeline_fact_campaign11
#         |'ReadFactCamp11'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2018, 11, 1)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #         |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign11'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#        )
#
#     with beam.Pipeline(options = options) as pipeline_fact_campaign10:
#        (pipeline_fact_campaign10
#         |'ReadFactCamp10'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2018, 8, 1), '$lte':datetime(2018, 10, 31)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #         |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign10'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )

    # with beam.Pipeline(options = options) as pipeline_fact_campaign9:
    #        (pipeline_fact_campaign9
    #         |'ReadFactCamp9'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
    #                                          query={'created_at': {'$gte':datetime(2018,05, 1), '$lte':datetime(2018, 07, 31)}},
    #                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
    #         |'GetTag' >> beam.ParDo(GetTagFn())
    #         |'GetQnA' >> beam.ParDo(GetQnA())
    # #         |'Debug' >> beam.Map(debug_function)
    #         | 'writeToBQ' >> beam.io.Write(
    #                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign9'),
    #                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #         )

#     with beam.Pipeline(options = options) as pipeline_fact_campaign8:
#        (pipeline_fact_campaign8
#         |'ReadFactCamp8'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2018,02, 1), '$lte':datetime(2018, 04, 30)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #        |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign8'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )
#
#     with beam.Pipeline(options = options) as pipeline_fact_campaign7:
#        (pipeline_fact_campaign7
#         |'ReadFactCamp7'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2017,11, 1), '$lte':datetime(2018, 01, 31)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #         |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign7'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )

#     with beam.Pipeline(options = options) as pipeline_fact_campaign6:
#        (pipeline_fact_campaign6
#         |'ReadFactCamp6'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2017,8, 1), '$lte':datetime(2017, 10, 31)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #         |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign6'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )
#
#     with beam.Pipeline(options = options) as pipeline_fact_campaign5:
#        (pipeline_fact_campaign5
#         |'ReadFactCamp5'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2017,05, 1), '$lte':datetime(2017, 07, 31)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #        |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign5'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )

#     with beam.Pipeline(options = options) as pipeline_fact_campaign4:
#        (pipeline_fact_campaign4
#         |'ReadFactCamp4'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2017,02, 1), '$lte':datetime(2017, 04, 30)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #         |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign4'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )

#     with beam.Pipeline(options = options) as pipeline_fact_campaign3:
#        (pipeline_fact_campaign3
#         |'ReadFactCamp3'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2016,11, 1), '$lte':datetime(2017, 01, 31)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #        |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign3'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )

#     with beam.Pipeline(options = options) as pipeline_fact_campaign2:
#        (pipeline_fact_campaign2
#         |'ReadFactCamp2'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$gte':datetime(2016, 8, 01), '$lte':datetime(2016, 10, 31)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #         |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign2'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#        )

#     with beam.Pipeline(options = options) as pipeline_fact_campaign1:
#        (pipeline_fact_campaign1
#         |'ReadFactCamp1'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
#                                          query={'created_at': {'$lte':datetime(2016, 07, 31)}},
#                                          fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
#         |'GetTag' >> beam.ParDo(GetTagFn())
#         |'GetQnA' >> beam.ParDo(GetQnA())
# #        |'Debug' >> beam.Map(debug_function)
#         | 'writeToBQ' >> beam.io.Write(
#                       beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign1'),
#                       schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
#                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#          )
