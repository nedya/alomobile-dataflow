import apache_beam as beam
import datetime
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math
import re

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

connection_string = "mongodb://grumpycat:alo.1975.dokter@35.185.189.200:27017/alomobile"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.225.73/alomobile"
tdelta = datetime.timedelta(hours=7)



#dimensi_doctors
class GetCitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        #add birthyear element
        birthyear = element.get('birthday', None)[-4:]
        if (birthyear):
            element['birthyear'] = int(birthyear)
        else: element['birthyear'] = None

        #add city name element
        if (element['city_id']):
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])})
            for city in city_query:
                element['city'] = city['name']
                data.append(element)
        else:
            element['city'] = None
            data.append(element)
        return data

class GetSpecialitiesDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        speciality_query = db.doctor_specialities.find({'_id':ObjectId(element['doctor_speciality_id'])})
        for speciality in speciality_query:
            element['speciality']=speciality['name']
            data.append(element)
        return data

class GetSkuDoctorFn(beam.DoFn):
    def process(self, element):
        data = []
        client = MongoClient(connection_string)
        db = client.alomobile
        if element.get('app_product_id'):
            sku_query = db.app_products.find({'_id':ObjectId(element['app_product_id'])})
            for sku in sku_query:
                element['sku']=sku['sku']
                data.append(element)
        else:
            element['sku']=None
            data.append(element)
        return data

#dimensi_review_doctors
def alphanumeric(x):
    x['review'] = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", x['review'])
    return x

#dimensi_campaigns
class GetclientnameCampFn(beam.DoFn):
    def process (self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        clients = []
        client_query = db.clients.find({'_id':ObjectId(element['client_id'])})
        for client in client_query:
            clients.append({'id':str(element['_id']),
                            'name':str(client['name']+" (" + str(element['start_date']+tdelta) [0:19] + " - "+ str(element['end_date']+tdelta)[0:19] + ")"),
                            'start_date':str(element['start_date']+tdelta)[0:19],
                            'end_date':str(element['end_date']+tdelta)[0:19],
                            'updated_at':str(element['updated_at']+tdelta)[0:19]})
        return clients

#dimensi_doctor_specialities
class UnlistDoctorSpecialitiesIDFn(beam.DoFn):
    def process(self, element):
        speciality_ids = element.get('speciality_ids', [])
        speciality_query = []
        for data in speciality_ids:
            speciality_query.append({'doctor_id':element['_id'],
                                     'speciality_ids':data})
        return speciality_query

class GetDoctorSpecialityIDFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        specialities = db.specialities
        speciality_list = []
        for data in specialities.find({'_id':element['speciality_ids']}):
                speciality_list.append({'id':str(uuid.uuid4()),
                                        'doctor_id':str(element['doctor_id']),
                                        'speciality_id':str(data['_id']),
                                        'is_deleted':False})
        return speciality_list

#dimensi_journal
def paid_timezoneJournals(j):
    if j['paid_time'] is not None:
        j['paid_time'] = str(j['paid_time']+tdelta)[0:19]
    return j

def gross_amountJournals(j):
    if j['gross_amount'] is not None:
        j['gross_amount'] = int(j['gross_amount'])
    return j

#dimensi meta question
def maxrecMetaQ(m):
        if m.get('max_count_recommendation', None) is None:
                m['max_count_recommendation'] = 3
        return m

#dimensi question
def join_listsQ((k, v)):
    return itertools.product(v['number_of_chatbox'], v['questions'])

def mergedictsQ(questions_dict_chatbox):
    (chatbox, questions_dict) = questions_dict_chatbox
    a_dict = questions_dict
    a_dict.update(chatbox)
    return a_dict

class GetPriceQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        prices = []
        if element.get('journal_id', None) is not None:
                price_query = db.journals.find({'_id':element['journal_id']})
                for price in price_query:
                    if price.get('gross_amount', None) is not None:
                        prices.append({'id':str(element['_id']),
                                        'updated_at':str(element['updated_at']+tdelta)[0:19],
                                        'price':int(price.get('gross_amount', None)),
                                        'pickup_fee':4000, 'like_fee':1000,
                                        'title':element['title'],
                                        'content':element['content'],
                                        'is_said_thanks':element['is_said_thanks'],
                                        'is_paid':element.get('is_paid'),
                                        'is_closed':element['is_closed'],
                                        'type':element['_type'],
                                        'sub_intent_id':element.get('sub_intent_id', None),
                                        'intent_id':element.get('intent_id', None),
                                        'is_has_conclusion':element.get('is_has_conclusion', None),
                                        'is_shown':element.get('is_shown', None)})
                    elif price.get('gross_amount', None) is None:
                        prices.append({'id':str(element['_id']),
                                       'updated_at':str(element['updated_at']+tdelta)[0:19],
                                       'price':None, 'pickup_fee':4000,
                                       'like_fee':1000,
                                       'title':element['title'],
                                       'content':element['content'],
                                       'is_said_thanks':element['is_said_thanks'],
                                       'is_paid':None,
                                       'is_closed':element['is_closed'],
                                       'type':element['_type'],
                                       'sub_intent_id':element.get('sub_intent_id', None),
                                       'intent_id':element.get('intent_id', None),
                                       'is_has_conclusion':element.get('is_has_conclusion', None),
                                       'is_shown':element.get('is_shown', None)})
        elif element.get('journal_id', None) is None:
            prices.append({'id':str(element['_id']),
                           'updated_at':str(element['updated_at']+tdelta)[0:19],
                           'price':None,
                           'pickup_fee':4000,
                           'like_fee':1000,
                           'title':element['title'],
                           'content':element['content'],
                           'is_said_thanks':element['is_said_thanks'],
                           'is_paid':None,
                           'is_closed':element['is_closed'],
                           'type':element['_type'],
                           'sub_intent_id':element.get('sub_intent_id', None),
                           'intent_id':element.get('intent_id', None),
                           'is_has_conclusion':element.get('is_has_conclusion', None),
                           'is_shown':element.get('is_shown', None)})
        return prices


def is_chatbotQ(q):
       if q['intent_id'] or q['sub_intent_id'] is not None:
           is_chatbot = {'is_chatbot':True}
       else:
           is_chatbot = {'is_chatbot':False}

       if q['type'] == "Core::PreQuestion":
           autochat_close = {'autochat_close':True}
       else:
           autochat_close = {'autochat_close':False}

       q.update(is_chatbot)
       q.update(autochat_close)
       q.pop('intent_id')
       q.pop('sub_intent_id')
       return q

class GetChatBoxQFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        chatboxes = []
        number_of_chatbox = db.answers.find({'question_id':ObjectId(element['id'])}, no_cursor_timeout=True).count()
        chatboxes.append({'id':str(element['id']),
                          'updated_at':element['updated_at'],
                          'number_of_chatbox':number_of_chatbox,
                          'price':element['price'],
                          'pickup_fee':element['pickup_fee'],
                          'like_fee':element['like_fee'],
                          'title':element['title'],
                          'content':element['content'],
                          'is_said_thanks':element['is_said_thanks'],
                          'is_paid':element['is_paid'],
                          'is_closed':element['is_closed'],
                          'type':element['type'],
                          'is_has_conclusion':element.get('is_has_conclusion', None),
                          'is_shown':element.get('is_shown', None)})
        return chatboxes


#dimensi users
class GetCitiesUserFn(beam.DoFn):
    def process(self, element):
        if (element['birthday']):
            birth_year = int(element['birthday'][-4:])
        else: birth_year = None

        if (element['provider']):
            registration_channel = element['provider']
        else: registration_channel = "email"

        if element.get('city_id', None) is not None:
            client = MongoClient(connection_string)
            db = client.alomobile
            city_query = db.cities.find({'_id':ObjectId(element['city_id'])})
            for city in city_query:
                city_name = city['name']
        else: city_name = None

        data = []
        data.append({'id':str(element['_id']),
                     'fullname':element['firstname'] + " " + element['lastname'],
                     'city':city_name,
                     'email':element.get('email', None),
                     'created_at':str(element['created_at'] + tdelta)[0:19],
                     'gender':element.get('gender',""),
                     'birthdate':element['birthday'],
                     'birthyear': birth_year,
                     'registration_channel':registration_channel,
                     'version':element.get('version',"")})
        return data

#dimensi interests
class GetInterestFn(beam.DoFn):
    def process(self,element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        if element.get('interest_ids'):
            for interest_id in element ['interest_ids']:
                data.append({'user_id':str(element['_id']),
                            'interest_id':str(interest_id),
                            'is_deleted':False})
        else: data.append({'user_id':str(element['_id']),
                           'interest_id':None,
                           'is_deleted':False})
        return data

#dimensi video campaigns tag
class DenormalizeVCTFn(beam.DoFn):
    def process(self,element):
        data = []
        if element.get('tags'):
            for tag in element['tags']:
                data.append({'video_campaign_id':element['_id'], 'tag':tag})
        else: data.append({'video_campaign_id':element['_id'], 'tag':None})
        return data

class GetTagIDVCTFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        tag_list = []
        if element.get('tag'):
            tag_query = db.terms.find({'name':element['tag']})
            for tag in tag_query:
                tag_list.append({'video_campaign_id':str(element['video_campaign_id']),
                                'tag_id':str(tag['_id'])
                                })
        else:
            tag_list.append({'video_campaign_id':str(element['video_campaign_id']),
                                'tag_id':None
                                })

        return tag_list


#dimensi fact campaigns
class GetTagFCFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        campaign_query = db.campaigns.find({'_id':element['campaign_id']},{'_id':1,'tags':1})
        for campaign in campaign_query:
            if (campaign.get('tags')): #if campaign tags is exsist
                for campaign_tag in campaign['tags']:
                    term_query = db.terms.find({'name':campaign_tag},{'_id':1})
                    for tag_id in term_query:
                        data.append({'id':element['_id'],
                                    'form_instance_values': element['form_instance_values'],
                                    'tag_id':tag_id['_id'],
                                    'client_id':element['client_id'],
                                    'campaign_id':element['campaign_id'],
                                    'user_id':element['user_id'],
                                    'created_at':element['created_at']+datetime.timedelta(hours=7)}
                                   )
            else:
                data.append ({'id':element['_id'],
                                'form_instance_values': element['form_instance_values'],
                                'tag_id':None,
                                'client_id':element['client_id'],
                                'campaign_id':element['campaign_id'],
                                'user_id':element['user_id'],
                                'created_at':element['created_at']+datetime.timedelta(hours=7)}
                              )
        return data


class GetQnAFC(beam.DoFn):
    def process(self, element):
        data = []
        for question in element['form_instance_values']:
            if (question.get('form_value_ids')): #jika pertanyaan memiliki jawaban
                for answer in question['form_value_ids']:
                    if (answer == '5770ef13150cd4744f00000b'):
                        answer = questions['text_value']
                    data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': str(answer),
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
            else: data.append({'id':str(uuid.uuid4()),
                                'question_id': str(question['form_id']),
                                'answer_id': None,
                                'tag_id':str(element['tag_id']),
                                'client_id':str(element['client_id']),
                                'campaign_id':str(element['campaign_id']),
                                'user_id':str(element['user_id']),
                                'created_at':str(element['created_at'])[0:19],
                                'year_id':element['created_at'].year,
                                'month_id':element['created_at'].month,
                                'date_id':element['created_at'].day,
                                'hour_id':element['created_at'].hour,
                                'minute_id':element['created_at'].minute})
        return data

#dimensi fact users
def join_infoUC((k,v)):
    return itertools.product(v['user_id'], v['channel_id'])

def merge_dictUC(user_channel):
      (user, channel) = user_channel
      a_dict = user
      a_dict.update(channel)
      return a_dict

#dimensi fact video campaign
class UnlistfvcFn(beam.DoFn):
    def process(self, element):
        tags = element.get('tags', [])
        tag_list = []
        for data in tags:
                tag_list.append({'tags':data,
                                'campaign_id':element['video_ad_campaign_id'],
                                'user_id':element['user_id'],
                                'created_at':element['created_at']})
        return tag_list

class GetTagfvcFn(beam.DoFn):
    def process (self, element):
        client = MongoClient(connection_string)
        db = client.alomobile
        termID_list = []
        terms = db.terms
        for term in terms.find({'name':element['tags']}):
            campaign_query = db.video_ad_campaigns.find({'_id':element['campaign_id']})
            for campaign in campaign_query:
                termID_list.append({'id':str(uuid.uuid4()),
                                     'ads_unit_id':str(campaign['video_ad_id']),
                                     'user_id':str(element['user_id']),
                                     'campaign_id':str(element['campaign_id']),
                                     'tag_id':str(term['_id']),
                                     'created_at':str(element['created_at']+tdelta)[0:19],
                                     'year_id':element['created_at'].year,
                                     'month_id':element['created_at'].month,
                                     'date_id':element['created_at'].day,
                                     'hour_id':element['created_at'].hour,
                                     'minute_id':element['created_at'].minute
                                    })
        return termID_list

#dimensi fact doctors
class DenormalizedFDFn(beam.DoFn):
    def process (self,element):
        data = []
        if element.get("educations"):
            for education in element['educations']:
                data.append((education['name'], {'doctor_id':str(element['_id']), 'year':int(education['year_of_graduation'])}))
        else:
             data.append((None, {'doctor_id':str(element['_id']), 'year':None}))
        return data

def join_infoFD((k,v)):
    doctors = []
    if (v['university']):
        data=itertools.product(v['doctor'], v['university'])
    else:
        data=itertools.product(v['doctor'], ' ')

    for element in data:
        if (element[1] == ' '):university_id = None
        else: university_id = element[1]

        doctors.append({'id': str(uuid.uuid4()),'doctor_id':str(element[0]['doctor_id']),
                        'university_id':university_id,
                        'graduate_year_id':element[0]['year']})
    return doctors

#dimensi user interest
class GetUserInterestFn(beam.DoFn):
    def process(self,element):
        client = MongoClient(connection_string)
        db = client.alomobile
        data = []
        if element.get('interest_ids'):
            for interest_id in element ['interest_ids']:
                data.append({'user_id':str(element['_id']),
                            'interest_id':str(interest_id),
                            'is_deleted':False})
        else: data.append({'user_id':str(element['_id']),
                           'interest_id':None,
                           'is_deleted':False})
        return data

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
#    pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])

    # with beam.Pipeline(options = options) as pipeline_popularities:
    #     (pipeline_popularities
    #              | 'Readpopularities' >> ReadFromMongo(connection_string, 'alomobile', 'magazine_popularities', query={}, fields=['magazine_id', 'total_view'])
    #              | 'strpopularities' >> beam.Map (lambda x:{ 'article_id':str(x['magazine_id']), 'total_view':int(x['total_view'])})
    #              # | 'Debugpopularities' >> beam.Map(debug_function)
    #              | 'writepopularitiesToBQ' >> beam.io.Write(
    #                          beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','magazine_popularities'),
    #                          schema='article_id:STRING, total_view:INTEGER',
    #                          create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                          write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )

    # client = MongoClient(connection_string)
    # db = client.alomobile
    # users = db.users
    # universities=[]
    # for element in users.distinct('educations.name',{'_type':'Core::Doctor','educations':{'$exists':True}}):
    #     universities.append({'id': str(uuid.uuid4()),'university':element})
    #
    #
    # with beam.Pipeline(options = options) as pipeline_universities:
    #     (pipeline_universities
    #         | 'unique_universities' >> beam.Create(universities)
    #         # | 'Debuguniversities' >> beam.Map(debug_function)
    #         | 'writeuniversitiesToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','universities'),
    #                       schema='id:STRING, university:STRING',
    #                       create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                       write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )

    # with beam.Pipeline(options = options) as pipeline_popperday:
    #     (pipeline_popperday
    #              | 'Readpopperday' >> ReadFromMongo(connection_string, 'alomobile', 'magazine_popularity_per_days', query={}, fields=['magazine_id', 'total_view'])
    #              | 'strpopperday' >> beam.Map (lambda x:{ 'article_id':str(x['magazine_id']), 'total_view_per_day':x['total_view']})
    #              # | 'Debugpopperday' >> beam.Map(debug_function)
    #              | 'writepopperdayToBQ' >> beam.io.Write(
    #                          beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','pageviews'),
    #                          schema='article_id:STRING, total_view_per_day:INTEGER',
    #                          create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                          write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )

    # with beam.Pipeline(options = options) as pipeline_article:
    #     (pipeline_article
    #             | 'Readarticle' >> ReadFromMongo(connection_string, 'alomobile', 'magazines',
    #                                   query={},
    #                                   fields=['post_id', 'title', 'slug',
    #                                           'is_deleted', 'created_at',
    #                                           'updated_at'])
    #             | 'strarticle' >> beam.Map (lambda x:{ 'id':str(x['_id']),
    #                                 'post_id':x['post_id'],
    #                                 'title':x['title'],
    #                                 'slug':x.get('slug', None),
    #                                 'is_deleted':x['is_deleted'],
    #                                 'created_at':str(x['created_at']+datetime.timedelta(hours=7))[0:19],
    #                                 'updated_at':str(x['updated_at']+datetime.timedelta(hours=7))[0:19]
    #                                })
    #             # | 'Debugarticle' >> beam.Map(debug_function)
    #             | 'writearticleToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','article_android'),
    #                   schema='id:STRING, post_id:STRING, title:STRING, slug:STRING, created_at:DATETIME, updated_at:DATETIME, is_deleted:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #         )
    #
    # with beam.Pipeline(options = options) as pipeline_video_campaign_tag:
    #     (pipeline_video_campaign_tag
    #             |'ReadVCT'>> ReadFromMongo(connection_string, 'alomobile', 'video_ad_campaigns',
    #                                      query={},
    #                                      fields=['_id', 'tags'])
    #             |'DenormalizeVCT' >> beam.ParDo(DenormalizeVCTFn())
    #             |'GetTagIDVCT'>> beam.ParDo(GetTagIDVCTFn())
    #             # |'DebugVCT' >> beam.Map(debug_function)
    #             | 'writeVCTToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','video_campaigns_tag'),
    #                   schema='video_campaign_id:STRING, tag_id:STRING',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #    )

    # with beam.Pipeline(options = options) as pipeline_user_interests:
    #     (pipeline_user_interests
    #         |'ReadUserInterest' >> ReadFromMongo(connection_string, 'alomobile', 'users',
    #                                     query={"_type":"Core::User"}, fields=['_id', 'interest_ids'])
    #         |'GetUserInterest' >> beam.ParDo(GetUserInterestFn())
    #         # |'DebugUserInterest' >> beam.Map(debug_function)
    #         | 'writeUserInterestToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','user_interests'),
    #                   schema='user_id:STRING, interest_id:STRING, is_deleted:BOOLEAN',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )
    #
    # with beam.Pipeline(options = options) as pipeline_specialities:
    #     (pipeline_specialities
    #         |'Readspecialities' >> ReadFromMongo(connection_string, 'alomobile', 'specialities', query={}, fields=['_id', 'name'])
    #         |'strspecialities' >> beam.Map (lambda x:{'id':str(x['_id']), 'interest':x['name']})
    #         # |'Debugspecialities' >> beam.Map(debug_function)
    #         | 'writespecialitiesToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','specialities'),
    #                   schema='id:STRING, interest:STRING',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #     )


    # with beam.Pipeline(options = options) as pipeline_fact_campaign:
    #    (pipeline_fact_campaign
    #         |'ReadFormInstance'>> ReadFromMongo(connection_string, 'alomobile', 'form_instances',
    #                                      query={},
    #                                      fields=['_id', 'form_instance_values','client_id','campaign_id','user_id','created_at'])
    #         |'GetTagFC' >> beam.ParDo(GetTagFCFn())
    #         |'GetQnAFC' >> beam.ParDo(GetQnAFC())
    #         # |'DebugFC' >> beam.Map(debug_function)
    #         | 'writeFCToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_campaign'),
    #                   schema='id:STRING, user_id:STRING, campaign_id:STRING, tag_id:STRING, client_id:STRING, question_id:STRING, answer_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INtEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #    )
    #
    #
    # with beam.Pipeline(options = options) as pipeline_fact_video_campaign:
    #     (pipeline_fact_video_campaign
    #         |'ReadViewVideo' >> ReadFromMongo(connection_string, 'alomobile', 'view_video_campaigns',
    #                                   query={},
    #                                   fields=(['user_id', 'tags', 'video_ad_campaign_id', 'created_at']))
    #         |'Unlistfvc' >> beam.ParDo(UnlistfvcFn())
    #         |'GetTagfvc' >> beam.ParDo(GetTagfvcFn())
    #         # |'Debugfvc' >> beam.Map(debug_function)
    #         | 'writeTfvcoBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','fact_video_campaign'),
    #                   schema='id:STRING, user_id:STRING, tag_id:STRING, campaign_id:STRING, ads_unit_id:STRING, created_at:DATETIME, year_id:INTEGER, month_id:INTEGER, date_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    #
    #     )
    # with beam.Pipeline(options = options) as pipeline_question:
    #     (pipeline_question
    #         | 'Readquestions' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #             query={'is_closed':True, "$or": [{ '_type': 'Core::Question'}, {'_type': 'Core::PreQuestion'}]},
    #             fields=['title', 'content', 'is_said_thanks', 'is_paid', 'is_closed', '_type', 'intent_id',
    #             'sub_intent_id', 'journal_id', 'is_has_conclusion', 'is_shown', 'updated_at'])
    #         | 'get_price' >> beam.ParDo(GetPriceQFn())
    #         | 'is_chatbot' >> beam.Map(is_chatbotQ)
    #         | 'number_of_chatbox' >> beam.ParDo(GetChatBoxQFn())
    #         # | 'DebugQ' >> beam.Map(debug_function)
    #         | 'writeQToBQ' >> beam.io.Write(
    #                  beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','questions'),
    #                  schema = 'id:STRING, type:STRING, updated_at:DATETIME, is_has_conclusion:BOOLEAN, autochat_close:BOOLEAN, is_paid:BOOLEAN, title:STRING, price:INTEGER, number_of_chatbox:INTEGER, content:STRING, is_said_thanks:BOOLEAN, is_chatbot:BOOLEAN, like_fee:INTEGER, pickup_fee:INTEGER, is_shown:BOOLEAN, is_closed:BOOLEAN',
    #                  create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                  write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #      )
    #
    # with beam.Pipeline(options = options) as pipeline_meta_question:
    #     (pipeline_meta_question
    #         | 'ReadMetaQ' >> ReadFromMongo(connection_string, 'alomobile', 'questions',
    #                                 query={"_type":'Core::MetaQuestion'},
    #                                 fields=['title', 'choice', 'created_at', 'max_count_recommendation'])
    #         |'strMetaQ'>>beam.Map (lambda x:{'id':str(x['_id']),
    #                              'title':x['title'],
    #                              'created_at':str(x['created_at']+datetime.timedelta(hours=7))[0:19],
    #                              'answer_by_user':x['choice'],
    #                              'max_count_recommendation':x['max_count_recommendation']
    #                             })
    #         | 'maxrecMetaQ' >> beam.Map(maxrecMetaQ)
    #         # | 'DebugMetaQ' >> beam.Map(debug_function)
    #         | 'writeMetaQToBQ' >> beam.io.Write(
    #                   beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','metaquestion'),
    #                   schema='id:STRING, title:STRING, created_at:DATETIME, answer_by_user:BOOLEAN, max_count_recommendation:INTEGER',
    #                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #                   write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
    #     )

    with beam.Pipeline(options = options) as pipeline_journals:
        (pipeline_journals
            | 'ReadJournals'>> ReadFromMongo(connection_string, 'alomobile', 'journals',
                                         query={'status':{"$ne":"pending"}},
                                         fields=['status', 'transaction_id','paid_time','doctor_id', 'user_id',
                                                 'is_consumed', 'created_at','updated_at','payment_method_id','payment_gateway_id',
                                                 'payment_provider_id', 'app_product_id', 'gross_amount'])
            | 'strJournals' >> beam.Map (lambda x:{'id':str(x['_id']),
                                       'status':x['status'],
                                       'transaction_id':x['transaction_id'],
                                       'paid_time':x.get('paid_time', None),
                                       'is_consumed':x['is_consumed'],
                                       'created_at':str(x['created_at']+datetime.timedelta(hours=7))[0:19],
                                       'updated_at':str(x['updated_at']+datetime.timedelta(hours=7))[0:19],
                                       'user_id':str(x['user_id']),
                                       'payment_method_id':str(x['payment_method_id']),
                                       'payment_gateway_id':str(x['payment_gateway_id']),
                                       'payment_provider_id':str(x['payment_provider_id']),
                                       'app_product_id':str(x['app_product_id']),
                                       'gross_amount':x.get('gross_amount', None),
                                       'doctor_id':str(x['doctor_id']),
                                       'date_id':(x['updated_at']+datetime.timedelta(hours=7)).day,
                                       'month_id':(x['updated_at']+datetime.timedelta(hours=7)).month,
                                       'year_id':(x['updated_at']+datetime.timedelta(hours=7)).year,
                                       'hour_id':(x['updated_at']+datetime.timedelta(hours=7)).hour,
                                       'minute_id':(x['updated_at']+datetime.timedelta(hours=7)).minute})

            | 'paid_timezoneJournals' >> beam.Map(paid_timezoneJournals)
            | 'gross_amountJournals' >> beam.Map(gross_amountJournals)
            # | 'DebugJournals' >> beam.Map(debug_function)
            | 'writeJournalsToBQ' >> beam.io.Write(
                             beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','journal'),
                             schema='id:STRING, user_id:STRING, status:STRING, created_at:DATETIME, updated_at:DATETIME, date_id:INTEGER, month_id:INTEGER, year_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER, app_product_id:STRING, transaction_id:STRING, is_consumed:BOOL, paid_time:DATETIME, payment_method_id:STRING, payment_gateway_id:STRING, payment_provider_id:STRING, gross_amount:INTEGER, doctor_id:STRING',
                             create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                             write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )


        # with beam.Pipeline(options = options) as pipeline_client:
        #     (pipeline_client
        #          | 'Readclient' >> ReadFromMongo(connection_string, 'alomobile', 'clients', query={}, fields=['name', 'updated_at'])
        #          | 'strClient' >> beam.Map (lambda x:{ 'id':str(x['_id']), 'name':x['name'], 'updated_at':str(x['updated_at']+datetime.timedelta(hours=7))[0:19]})
        #          # | 'DebugClient' >> beam.Map(debug_function)
        #          | 'writeClientToBQ' >> beam.io.Write(
        #                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','clients'),
        #                      schema='id:STRING, name:STRING, updated_at:DATETIME',
        #                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
        #                      write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        #     )
        #
        # with beam.Pipeline(options = options) as pipeline_doctor_specialities:
        #     (pipeline_doctor_specialities
        #         |'Readdoctorspecialities' >> ReadFromMongo(connection_string, 'alomobile', 'users',
        #                                 query={'_type':'Core::Doctor'}, fields=['_id', 'speciality_ids'])
        #         |'UnlistSpecialitiesID'>>beam.ParDo(UnlistDoctorSpecialitiesIDFn())
        #         |'GetSpecialityID'>>beam.ParDo(GetDoctorSpecialityIDFn())
        #         # |'Debugdoctorspecialities' >> beam.Map(debug_function)
        #         | 'writedoctorspecialitiesToBQ' >> beam.io.Write(
        #               beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodoktermobile','doctor_specialities'),
        #               schema='id:STRING, doctor_id:STRING, speciality_id:STRING, is_deleted:BOOLEAN',
        #               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
        #               write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        #     )
